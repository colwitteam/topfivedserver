<?php

namespace App;

use JWTAuth;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Validator;
use \app\Helpers\Utils;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\SoftDeletes;

class Activity extends Model
{

    use SoftDeletes;
    protected $softDelete = true;
    private $rules = [];
    private $errors;
    protected $fillable = ['origin', 'action', 'target', 'affected'];
    protected $hidden = ['created_at', 'updated_at', 'deleted_at'];
    protected $dates = ['deleted_at'];

    public function validate($data)
    {
        $this->rules = [
            'origin' => 'required',
            'action' => 'required',
            'target' => 'required',
            'affected' => 'required',
        ];
        // make a new validator object
        $v = Validator::make($data, $this->rules);

        // check for failure
        if ($v->fails()) {
            // set errors and return false
            $this->errors = $v->errors()->toArray();
            return false;
        }

        // validation pass
        return true;
    }

    public function errors()
    {
        return $this->errors;
    }

    public function track($data)
    {
        if ($this->validate($data)):
            $this->fill($data);
            $this->Save();
        endif;
    }

    public static function preparedActivity($activity)
    {
        $token = JWTAuth::getToken();
        $user = JWTAuth::toUser($token);
        $activity->activity_for_user = false;
        if ($user->id == $activity->origin):
            $activity->activity_for_user = true;
        endif;
        $user_origin = User::find($activity->origin);
        $user_affect = User::find($activity->affected);
        $activity->origin = User::prepareLazy($user_origin);
        $activity->affected = User::prepareLazy($user_affect);
        if ($activity->action == 'comment' || $activity->action == 'mention'):
            $comment = ListComment::find($activity->target);
            if ($comment):
                $newComment = ListComment::replaceUser($comment->comment);
                $mention = Utils::truncate($newComment, 60);
                $activity->comment = $mention;
            endif;
        endif;
        if ($activity->action == 'follow'):
            if ($activity->affected->id == $user->id):
                $activity = UserFollow::setIsFollowing($activity, $user->id, $activity->origin->id);
            endif;

        endif;
        if ($activity->action == 'liked'):
            $listFavorite = ListFavorite::find($activity->target);
            if ($listFavorite):
                $list = UserList::find($listFavorite->list_id);
                if ($list):
                    $listFavorite->list_id = UserList::preparedListLazy($list);
                    $activity->target = $listFavorite;
                endif;
            endif;
        endif;
        if ($activity->action == 'challenge'):
            $challenge = Challenge::find($activity->target);
            if ($challenge):
                $activity->target = Challenge::preparedChallenge($challenge);
            endif;
        endif;
        $activity->time_activity = $activity->created_at->diffForHumans();

        return $activity;
    }

    public static function preparedFollowsActivities($userActivity)
    {

        $user_origin = User::where('id', $userActivity->origin)->first();

        $userActivity->user_origin = User::prepareLazy($user_origin);
        if ($userActivity->action == 'liked'):
            $activities = self::where('origin', $userActivity->id)
                ->where('action', $userActivity->action)
                ->orderBy('created_at', 'DESC')
                ->take(10)
                ->get();
            if (!$activities->isEmpty()):
                foreach ($activities as $act):
                    $listFavorite = ListFavorite::find($act->target);
                    if ($listFavorite):
                        $list = UserList::find($listFavorite->list_id);
                        if ($list):
                            $act->target = UserList::preparedMiniList($list);
                        endif;
                    endif;
                endforeach;
                $userActivity->activities = $activities;
            endif;
        elseif ($userActivity->action == 'comment'):
            $comments = Activity::select('lists.*')->join('lists', 'lists.id', '=', 'activities.target')->where(['action' => 'comment', 'origin' => $userActivity->id])->get();
            $list_comment = [];
            //$userActivity->lists_comment = $comments;
            foreach ($comments as $comment):
                $list_comment[] = UserList::preparedListLazy($comment);
            endforeach;
            $userActivity->list_comment = $list_comment;
        elseif($userActivity->action == 'follow'):
            $user_affected = User::find($userActivity->affected);
            $userActivity->user_affected = User::prepareLazy($user_affected);
        elseif ($userActivity->action == 'challenge'):
                $challenge = Challenge::find($userActivity->target);
                if ($challenge):
                    $userActivity->target = Challenge::preparedChallenge($challenge);
                endif;
        endif;
        $userActivity->last_date = Carbon::parse($userActivity->last_date)->diffForHumans();
        return $userActivity;
    }

}
