<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Award extends Model
{
    public static function preparedAwardList($award){

        $list = UserList::find($award->list_id);
        $award->list = $list;
        $award->award = self::find($award->award_id);
        $award->user = User::find($award->user_id);
        return $award;
    }
}
