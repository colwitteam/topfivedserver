<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use JWTAuth;

class Challenge extends Model
{

    protected $fillable = ['id', 'owner_id', 'image_mini', 'target_id', 'owner_list_id', 'status', 'deadline_date', 
                            'open_to', 'auto_shared', 'facebook', 'twitter', 'my_feed'];
    protected $hidden = ['created_at', 'updated_at'];
    private $rules = [];
    private $errors;

    public function validate($data)
    {

        $this->rules = [
            'owner_id' => 'required',
            'target_id' => 'required',
            'target_list_id' => 'required',
            'owner_list_id' => 'required',
            'deadline_date' => 'required|digits_between:1,14',
            'facebook' => (isset($data['auto_shared']) && $data['auto_shared']==1 &&(!isset($data['twitter']) && !isset($data['my_feed']))) ? 'required' : '',
            'twitter' => (isset($data['auto_shared']) && $data['auto_shared']==1 && (!isset($data['facebook']) && !isset($data['my_feed']))) ? 'required' : '',
            'my_feed' => (isset($data['auto_shared']) && $data['auto_shared']==1 && (!isset($data['twitter']) && !isset($data['facebook']))) ? 'required' : '',
        ];

        // make a new validator object
        $v = Validator::make($data, $this->rules);

        // check for failure
        if ($v->fails()) :
            // set errors and return false
            $this->errors = $v->errors()->toArray();
            return false;
        endif;

        // validation pass
        return true;
    }

    public function errors()
    {
        return $this->errors;
    }

    public function validateResponse($data)
    {

        $required = '';

        if (array_key_exists('status', $data) and !empty($data['status'])):
            $required = $data['status'] == 2 ? '|required' : '';
        endif;

        $this->rules = [
            'status' => 'numeric|required',
            'target_list_id' => 'numeric' . $required
        ];
        // make a new validator object
        $v = Validator::make($data, $this->rules);

        // check for failure
        if ($v->fails()) :
            // set errors and return false
            $this->errors = $v->errors()->toArray();
            return false;
        endif;

        // validation pass
        return true;
    }

    public static function preparedChallenge($challenge)
    {
        $token = JWTAuth::getToken();
        $user = JWTAuth::toUser($token);

        $timezone = null;
        if ($user->timezone):
            $timezone = $user->timezone;
        endif;

        $target_user = User::find($challenge->target_id);
        $owner = User::find($challenge->owner_id);
        $status = ChallengeStatus::find($challenge->status);
        $challenge->status = $status;
        $owner_like = $list_owner = ListFavorite::select(DB::raw('count(list_favorites.list_id) as totalLikes'), "lists.user_id as user_id")
            ->join('lists', 'lists.id', '=', 'list_favorites.list_id')
            ->where(['lists.id' => $challenge->owner_list_id])
            ->where('list_favorites.created_at', '<=', Carbon::parse($challenge->deadline_date))
            ->where('list_favorites.created_at', '>=', Carbon::parse($challenge->start_date))
            ->groupBy('lists.id')->first();
        $target_like = $list_owner = ListFavorite::select(DB::raw('count(list_favorites.list_id) as totalLikes'), "lists.user_id as user_id")
            ->join('lists', 'lists.id', '=', 'list_favorites.list_id')
            ->where(['lists.id' => $challenge->target_list_id])
            ->where('list_favorites.created_at', '<=', Carbon::parse($challenge->deadline_date))
            ->where('list_favorites.created_at', '>=', Carbon::parse($challenge->start_date))
            ->groupBy('lists.id')->first();
        $challenge->likes_competition_you = 0;
        $challenge->likes_competition_competitor = 0;
        if ($owner_like):
            if ($owner_like->user_id == $user->id):
                $challenge->likes_competition_you = "You " . $owner_like->totalLikes;
            else:
                $user_competitor = User::find($owner_like->user_id);

                $challenge->likes_competition_competitor = $user_competitor->username . " " . $owner_like->totalLikes;
            endif;
        endif;
        if ($target_like):
            if ($target_like->user_id == $user->id):
                $challenge->likes_competition_you = "You " . $target_like->totalLikes;
            else:
                $user_competitor = User::find($target_like->user_id);

                $challenge->likes_competition_competitor = $user_competitor->username . " " . $target_like->totalLikes;
            endif;
        endif;     
        $challenge->open_to = ($challenge->open_to == 1 or !isset($challenge->open_to))?'public':'mutual followers';
        $challenge->owner_user = User::prepareLazy($owner);
        $challenge->target_user = User::prepareLazy($target_user);
        $ownerList = UserList::find($challenge->owner_list_id);
        $challenge->owner_list = UserList::preparedListLazy($ownerList);
        if ($challenge->target_list_id):
            $targetList = UserList::find($challenge->target_list_id);
            $challenge->target_list = UserList::preparedListLazy($targetList);
        endif;
        $challenge->auto_shared = $challenge->auto_shared;
        if ($challenge->start_date):
            $dateStart = Carbon::parse($challenge->start_date);
            $dateFinal = Carbon::parse($challenge->deadline_date);
            $now = Carbon::parse(Carbon::now($timezone));
            $challenge->time_left = $dateFinal->diffForHumans($now);
            $challenge->time_period = $dateFinal->diffForHumans($dateStart);
        endif;
        //$challenge->start_date = Carbon::parse($challenge->start_date);
        return $challenge;
    }

    public static function preparedChallengePublic($challenge)
    {
        $target_user = User::find($challenge->target_id);
        $owner = User::find($challenge->owner_id);
        $status = ChallengeStatus::find($challenge->status);
        $challenge->status = $status;
        $owner_like = $list_owner = ListFavorite::select(DB::raw('count(list_favorites.list_id) as totalLikes'), "lists.user_id as user_id")
            ->join('lists', 'lists.id', '=', 'list_favorites.list_id')
            ->where(['lists.id' => $challenge->owner_list_id])
            ->where('list_favorites.created_at', '<=', Carbon::parse($challenge->deadline_date))
            ->where('list_favorites.created_at', '>=', Carbon::parse($challenge->start_date))
            ->groupBy('lists.id')->first();
        $target_like = $list_owner = ListFavorite::select(DB::raw('count(list_favorites.list_id) as totalLikes'), "lists.user_id as user_id")
            ->join('lists', 'lists.id', '=', 'list_favorites.list_id')
            ->where(['lists.id' => $challenge->target_list_id])
            ->where('list_favorites.created_at', '<=', Carbon::parse($challenge->deadline_date))
            ->where('list_favorites.created_at', '>=', Carbon::parse($challenge->start_date))
            ->groupBy('lists.id')->first();
        $challenge->likes_competition_you = 0;
        $challenge->likes_competition_competitor = 0;
        /*if ($owner_like):
            if ($owner_like->user_id == $user->id):
                $challenge->likes_competition_you = "You " . $owner_like->totalLikes;
            else:
                $user_competitor = User::find($owner_like->user_id);

                $challenge->likes_competition_competitor = $user_competitor->username . " " . $owner_like->totalLikes;
            endif;
        endif;
        if ($target_like):
            if ($target_like->user_id == $user->id):
                $challenge->likes_competition_you = "You " . $target_like->totalLikes;
            else:
                $user_competitor = User::find($target_like->user_id);

                $challenge->likes_competition_competitor = $user_competitor->username . " " . $target_like->totalLikes;
            endif;
        endif;     */   
        $challenge->open_to = ($challenge->open_to == 1 or !isset($challenge->open_to))?'public':'mutual followers';
        $challenge->owner_user = User::prepareLazy($owner);
        $challenge->target_user = User::prepareLazy($target_user);
        $ownerList = UserList::find($challenge->owner_list_id);
        $challenge->owner_list = UserList::preparedListLazyPublic($ownerList);
        //$challenge->target_list = 59;
        if ($challenge->target_list_id):

            $targetList = UserList::find($challenge->target_list_id);
            //$challenge->target_list = $targetList;
            $challenge->target_list = UserList::preparedListLazyPublic($targetList);
        endif;
        /*$challenge->auto_shared = $challenge->auto_shared;
        if ($challenge->start_date):
            $dateStart = Carbon::parse($challenge->start_date);
            $dateFinal = Carbon::parse($challenge->deadline_date);
            $now = Carbon::parse(Carbon::now($timezone));
            $challenge->time_left = $dateFinal->diffForHumans($now);
            $challenge->time_period = $dateFinal->diffForHumans($dateStart);
        endif;*/
        //$challenge->start_date = Carbon::parse($challenge->start_date);
        return $challenge;
    }

    public static function prepareUser($challenge)
    {
        $user_owner = User::find($challenge->owner_id);
        $timezone = null;
        if ($user_owner->timezone):
            $timezone = $user_owner->timezone;
        endif;
        $challenge->user_owner = User::prepareLazy($user_owner);
        $user_target = User::find($challenge->target_id);
        $challenge->user_target = User::prepareLazy($user_target);
        $dateFinal = Carbon::parse($challenge->deadline_date);
        $now = Carbon::parse(Carbon::now($timezone));
        $challenge->time_left=$dateFinal->diffForHumans($now);
        $owner_list = UserList::find($challenge->owner_list);
        if($owner_list):
            $challenge->owner_list = UserList::preparedListLazy($owner_list);
        endif;
        if ($challenge->target_list_id):
            $targetList = UserList::find($challenge->target_list_id);
            $challenge->target_list = UserList::preparedListLazy($targetList);
        endif;
        $challenge->open_to = ($challenge->open_to == 1 or !isset($challenge->open_to))?'public':'mutual followers';
        return $challenge;
    }

}
