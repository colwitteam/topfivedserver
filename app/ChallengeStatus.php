<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ChallengeStatus extends Model
{
    protected $table = 'challenge_status';
}
