<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\ListPopularity;
use App\UserList;

class DayListPopulary extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'ListPopularity:popularity';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'cron job detected list more popular';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $list_popularities = UserList::select('lists.id as id', 'list_popularities.id as id_popular', 'list_popularities.quantity as quantity')
            ->join('list_popularities', 'list_popularities.list_id', '=', 'lists.id')
            ->where('lists.is_private', 0)
            ->orderBy('quantity')
            ->get();

        foreach ($list_popularities as $list_popularity):
            $find_popular = ListPopularity::find($list_popularity->id_popular);

            if ($find_popular):
                if ($find_popular->quantity != $list_popularity->quantityPopulary()):
                    $find_popular->quantity = $list_popularity->quantityPopulary();
                    $find_popular->save();
                endif;
            endif;
        endforeach;
    }
}
