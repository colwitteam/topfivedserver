<?php

namespace App\Console\Commands;

use App\UserList;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;
use App\Challenge;
use Illuminate\Support\Facades\DB;
use App\ListFavorite;
use App\User;
use App\Helpers\Utils;

class WinnerChallenges extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'winnerchallenge:challenge';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Winner challenge every days';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        $now = Carbon::now();

        $challenges = Challenge::where('deadline_date', '<=', $now)->where("status", 4)->get();
        foreach ($challenges as $challenge):
            $this->getWinnerChallenge($challenge);
        endforeach;
    }

    public function getWinnerChallenge($challenge)
    {

        $list_owner = ListFavorite::select(DB::raw('count(list_favorites.list_id) as totalLikes'))
            ->join('lists', 'lists.id', '=', 'list_favorites.list_id')
            ->where(['lists.id' => $challenge->owner_list_id])
            ->where('list_favorites.created_at', '<=', Carbon::parse($challenge->deadline_date))
            ->where('list_favorites.created_at', '>=', Carbon::parse($challenge->start_date))
            ->groupBy('lists.id')->first();

        $list_target = ListFavorite::select(DB::raw('count(list_favorites.list_id) as totalLikes'))
            ->join("lists", "lists.id", "=", "list_favorites.list_id")
            ->where(['list_favorites.list_id' => $challenge->target_list_id])
            ->where('list_favorites.created_at', '<=', Carbon::parse($challenge->deadline_date))
            ->where('list_favorites.created_at', '>=', Carbon::parse($challenge->start_date))
            ->groupBy('lists.id')->first();

        $totalOwner = $list_owner ? $list_owner->totalLikes : 0;
        $totalTarget = $list_target ? $list_target->totalLikes : 0;

        if ($totalOwner > $totalTarget):
            $list_winner = User::find($challenge->owner_id);
            $like_winner = $totalOwner;
        else:
            $list_winner = User::find($challenge->target_id);
            $like_winner = $totalTarget;
        endif;

        $challenge->winner_id = $list_winner->id;
        $challenge->winner_likes = $like_winner;
        $challenge->status = 6;
        $save_challenge = $challenge->save();
        if($save_challenge):
            $firebaseNotification = new Utils();
            $firebaseNotification->sendNotificationFirebase($list_winner->token_android,'User winner challenge',$save_challenge,'award_challenge');

        endif;
    }
}
