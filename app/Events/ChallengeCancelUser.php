<?php

namespace App\Events;

use App\User;
use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class ChallengeCancelUser
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $user_disable;
    public $enable_user;
    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(User $user,$enable)
    {
        $this->user_disable = $user;
        $this->enable_user = $enable;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('channel-name');
    }
}
