<?php

namespace App\Events;

use App\User;
use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class EventActivitiesUser
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $user_origin;
    public $option;
    public $activity_id;
    public $active_activities;
    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(User $user,$option,$active,$id=null)
    {
        $this->user_origin = $user;
        $this->option = $option;
        $this->active_activities = $active;
        if($id):
            $this->activity_id = $id;
        endif;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('channel-name');
    }
}
