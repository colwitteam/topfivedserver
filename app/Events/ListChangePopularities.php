<?php

namespace App\Events;

use App\UserList;
use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class ListChangePopularities
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $_list;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(UserList $list)
    {
        $this->_list = $list;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('channel-name');
    }
}
