<?php

namespace App\Events;

use App\ListFavorite;
use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class ListPopularities
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $listFavorite;
    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(ListFavorite $listFavorite)
    {
        $this->listFavorite=$listFavorite;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('channel-name');
    }
}
