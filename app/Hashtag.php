<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Validator;

class Hashtag extends Model
{
    private $rules = [];
    private $errors;
    protected $fillable = ['keyword'];
    protected $hidden = ['created_at', 'updated_at'];

    public function validate($data) {
        $this->rules = [
            'keyword' => 'required'
        ];
        // make a new validator object
        $v = Validator::make($data, $this->rules);

        // check for failure
        if ($v->fails()) {
            // set errors and return false
            $this->errors = $v->errors()->toArray();
            return false;
        }

        // validation pass
        return true;
    }

    public function errors() {
        return $this->errors;
    }

    public static function getHashTag($tags){
        $hashtags=[];
        foreach ($tags as $tag):
            $keywords = Hashtag::find($tag->hashtag_id);
            $hashtags[] = $keywords->keyword;
        endforeach;
        return $hashtags;
    }

    public function scopeFilter($query,$request){
        $hashtags = [];
        $query_sugesst = $this->select('hashtags.*');
        if(is_array($request["name"]) and count($request["name"]) > 0):
            foreach ($request["name"] as $key => $name) {
                $query_sugesst->orWhere('keyword','like',$name.'%');
            }
        endif;
        $query->select('hashtags.*');
        if(is_array($request["name"]) and count($request["name"]) > 0):
            foreach ($request["name"] as $key => $name) {
                $query->orWhere('keyword','like',$name.'%');
            }
        endif;
        if($request['user_id']):
            $query->join('list_hashtags','list_hashtags.hashtag_id','=','hashtags.id');
            $query->join('lists','lists.id','=','list_hashtags.list_id');
            $query->where('lists.user_id',$request['user_id']);
        endif;
        $query->union($query_sugesst);
    }

    public static function preparedHashtag($hashtag){
        $hashtag->id = $hashtag->id;
        $hashtag->keyword = $hashtag->keyword;
        return $hashtag;
    }


}
