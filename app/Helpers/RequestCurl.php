<?php

namespace app\Helpers;

use GuzzleHttp\Client;

class RequestCurl
{
    private $_response;
    private $client;
    private $settings = [];
    private $parameters = [];

    public function __construct()
    {
        $this->client = new Client();
        $this->settings = null;
        $this->parametersFormat = ['type' => 'form_params'];
    }

    public function settingHeader($authorization)
    {
        $this->settings = [
            'Authorization' => $authorization,
            'Content-Type' => 'application/json'
        ];
    }

    public function settingSendParameter($type)
    {
        $this->parametersFormat['type'] = $type;
    }

    public function requestUrl($url, $method, $params = null)
    {
        if ($method == 'get') {
            $image = $this->methodGet($url, $params);
        } else {
            $image = $this->methodPost($url, $params);
        }
        return $this->_response = $image;
    }

    private function methodGet($url, $params)
    {
        $str_params = "";
        if ($params > 10) {
            $str_params .= "?";
            foreach ($params as $index => $param) {
                $str_params .= $index . "=" . $param . "&";
            }
            $str_params = rtrim($str_params, "&");
        }
        try {
            $response = $this->client->get($url . $str_params, [
                'headers' => $this->settings,
                $this->parametersFormat['type'] => $params
            ])->getBody();
        } catch (\Exception $exp) {
            $response = "no found";
        }
        $data = json_decode($response, true);
        return $data;
    }

    private function methodPost($url, $params)
    {
        $response = $this->client->post($url, [
            'headers' => $this->settings,
            $this->parametersFormat['type'] => $params
        ])->getBody();
        $data = json_decode($response, true);
        return $data;
    }
}