<?php


namespace app\Helpers;

use Abraham\TwitterOAuth\TwitterOAuth;
use Facebook;
use Google_Client;
use App\User;
class UserSocials
{
    public function getFriendUserTwitter($token,$secret,$count=null,$limit = null){
        $connection = new TwitterOAuth(config('app.twitter.consumer_key'), config('app.twitter.consumer_secret'), $token, $secret);
        $content = $connection->get("account/verify_credentials");
        if ($connection->getLastHttpCode() != 200):
            return null;
        endif;
        $ids = $connection->get("friends/ids", ['user_id' => $content->id]);

        $users_twiiter = [];

        foreach ($ids->ids as $id):
            $user = User::where(['twitter_id'=>$id])->first();
            if($user):
                $users_twiiter[] =  User::prepareLazy($user);
            endif;
        endforeach;
        return $users_twiiter;
    }
}