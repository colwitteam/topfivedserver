<?php

/**
 * Created by PhpStorm.
 * User: chechorjuela
 * Date: 9/05/17
 * Time: 09:26 AM
 */

namespace app\Helpers;

use App\UserList;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class Utils {

    public static function createToken($length = 8) {
        $characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
        $string = '';
        for ($i = 0; $i < $length; $i++) {
            $string .= $characters[rand(0, strlen($characters) - 1)];
        }
        return $string;
    }

    public static function extractName($name) {
        $string_name = explode(' ', $name);
        return $string_name;
    }

    public static function truncate($text, $size = 0) {
        if (strlen($text) > $size and $size > 0) {
            $parts = preg_split('/([\s\n\r]+)/', $text, null, PREG_SPLIT_DELIM_CAPTURE);
            $parts_count = count($parts);

            $length = 0;
            $last_part = 0;
            for (; $last_part < $parts_count; ++$last_part) {
                $length += strlen($parts[$last_part]);
                if ($length > $size) {
                    break;
                }
            }
            $text = implode(array_slice($parts, 0, $last_part)) . "...";
        }
        return $text;
    }

    public function sendNotificationFirebase($token, $title, $object, $type) {
        $request_curl = new RequestCurl();
        $url_google = env('URL_FIREBASE');
        $request_curl->settingHeader('key = '.env('API_ACCESS_KEY_FIREBASE'));
        $request_curl->settingSendParameter('json');

        $params=[
            'to' => $token,
            'notification' => [
                'title'=>$title,
            ],
            'data' => [
                'type' => $type,
                'object'=>$object
            ]
        ];

        $request_curl->requestUrl($url_google, 'post', $params);
    }

    public function getGeocodeLocation($lat, $long) {
        $data = [];
        $api_key = config('app.google_api.api_key');
        $geocode = file_get_contents('https://maps.googleapis.com/maps/api/geocode/json?latlng=' . $lat . ',' . $long . '&key=' . $api_key);
        $jsonGeocode = json_decode($geocode);

        if ($jsonGeocode->results):
            $address = $jsonGeocode->results[0]->formatted_address;        
            $data['address']= substr($address, 0, strpos($address, ','));
            $data['city'] = $jsonGeocode->results[1]->address_components[1]->long_name;
            $data['country'] = $jsonGeocode->results[1]->address_components[3]->long_name;
        endif;
        
        return $data;
    }

    public function formatChatDate($date,$timezone=null){
        if($timezone==null):
            $timezone = 'UTC';
        endif;
        $date = Carbon::instance($date)->timezone($timezone);

        $different_hours = $date->diffInHours();
        if($different_hours<1):
            $format_date = $date->diffForHumans();

        elseif($different_hours<24):
            $format_date = $date->formatLocalized('%a %H:%M');
        else:
            $different_days = $date->diffInDays();
            if($different_days<31):
                $format_date = $date->formatLocalized('%a %H:%M');
            else:
                $format_date= $date->formatLocalized('%b %a-%d %H:%M');
            endif;
        endif;

        return $format_date;
    }

}
