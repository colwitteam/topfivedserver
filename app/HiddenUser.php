<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class HiddenUser extends Model
{
    protected $fillable = ['user_id','user_hidden_id','social'];
    public $timestamps = true;

}
