<?php

namespace App\Http\Controllers;

use app\Helpers\Utils;
use Illuminate\Http\Request;
use app\Helpers\RequestCurl;
use Session;
use App\User;
use App\Challenge;

class AdminController extends Controller
{

    public function __construct()
    {
        $this->middleware('authUserApi')->except(['verifyUserAdmin']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return redirect()->route('admin.lists');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function verifyUserAdmin(Request $request){

        $user = User::find($request->id);
        $code = 204;
        if($user->role==2):
            $code = 200;
            Session::put('access_token',$request->token);
        else:
            $code = 204;
        endif;
        return response()->json($user,$code);
    }

    public function welcomeAdmin(){

    }

    public function getListAdmin(Request $request){
        $params = [];
        if($request):
            $params['name'] = $request->name;
            $params['order'] = $request->order;
            $params['page'] = $request->page;
            $params['hashtag'] = $request->hashtag;
            $params['top'] = $request->top;
            $params['term'] = $request->term;
        endif;

        $url = env('APP_URL_API')."admin/lists";
        $request_curl = new RequestCurl();
        $request_curl->settingHeader('Bearer '.Session::get('access_token'));
        $lists = $request_curl->requestUrl($url, 'get',$params);

        return view('admin.lists',['lists'=>$lists,'request'=>$params]);
    }

    public function getUsersAdmin(Request $request){
        $params = [];
        if($request):

            $params['page'] = $request->page;

            $params['username'] = $request->username;    
            $params['firstname'] = $request->firstname;
            $params['lastname'] = $request->lastname;
            $params['email'] = $request->email;

        endif;
        $url = env('APP_URL_API')."admin/users";
        $request_curl = new RequestCurl();
        $request_curl->settingHeader('Bearer '.Session::get('access_token'));
        $users = $request_curl->requestUrl($url, 'get',$params);
        
        return view('admin.users',['users'=>$users['data'], 'request' => $params]);
    }

    public function getReportsList(Request $request){
        if($request):
            $params['name'] = $request->name;
            $params['order'] = $request->order;
            $params['page'] = $request->page;
            $params['hashtag'] = $request->hashtag;
            $params['top'] = $request->top;
            $params['term'] = $request->term;
        endif;
        $url = env('APP_URL_API')."admin/reports/lists";
        $request_curl = new RequestCurl();
        $request_curl->settingHeader('Bearer '.Session::get('access_token'));
        $lists = $request_curl->requestUrl($url, 'get',$params);

        return view('admin.reports.lists',['lists'=>$lists['data'],'request'=>$request]);
    }

    public function getReportsUsers(Request $request){
        if($request):
            $params['order'] = $request->order;
            $params['page'] = $request->page;
            $params['username'] = $request->username;    
            $params['firstname'] = $request->firstname;
            $params['lastname'] = $request->lastname;
            $params['email'] = $request->email;            
        endif;

        $url = env('APP_URL_API')."admin/reports/users";

        $request_curl = new RequestCurl();
        $request_curl->settingHeader('Bearer '.Session::get('access_token'));
        $users = $request_curl->requestUrl($url, 'get',$params);

        return view('admin.reports.users',['users'=>$users['data'],'request'=>$request]);
    }
    public function logout(){

        $request_url = new RequestCurl();
        $request_url->settingHeader('Bearer '.Session::get('access_token'));
        $request = $request_url->requestUrl(env('APP_URL_API').'logout','get');
        Session::forget('access_token');
        return redirect("admin/login");;
    }
}
