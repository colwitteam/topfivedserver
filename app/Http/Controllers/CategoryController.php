<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ListCategory;
use App\UserList;
use App\Category;
use App\Http\Controllers\ListController;

class CategoryController extends Controller
{

    private $_limit = 10;
    private $_page = 1;

    private function _setPagination(Request $request) {
        !!$request->get('quantity') and ($this->_limit = $request->get('quantity'));
        !!$request->get('page') and ($this->_page = $request->get('page'));
    }

    public function __construct() {
        $this->_limit = config('app.pagination.quantity');
    }

    function show($term) {
        $response = ['status' => 'ok', 'data' => []];
        $code = 200;

        if (empty($term)):
            $response['data'] = Category::all();
        else:
            if (is_numeric($term)):
                $response['data'] = Category::find($term);
            elseif (is_string($term)):
                $response['data'] = Category::where('name','like',"%{$term}%")->get();
                $response['status'] = $term;
            endif;
        endif;

        return response()->json($response, $code);
    }

    function all(Request $request) {
        $this->_setPagination($request);
        $response = ['status' => 'ok', 'data' => [], 'page' => $this->_page, 'quantity' => $this->_limit];
        $code = 200;
        $response['data'] = Category::select('categories.*')->paginate($this->_limit);

        return response()->json($response, $code);
    }

    function showLists(Request $request, $term) {
        $this->_setPagination($request);
        $response = ['status' => 'ok', 'data' => [], 'page' => $this->_page, 'quantity' => $this->_limit];
        $code = 200;

        if (!empty($term)):
            if (is_numeric($term)):
                $response['data'] = UserList::join('list_categories', 'lists.id', '=', 'list_categories.list_id')
                    ->select('lists.*')
                    ->where(['list_categories.category_id'=>$term])
                    ->paginate($this->_limit);
            elseif (is_string($term)):
                $response['data'] = UserList::join('list_categories', 'lists.id', '=', 'list_categories.list_id')
                    ->join('categories', 'categories.id', '=', 'list_categories.category_id')
                    ->select('lists.*')
                    ->where('categories.name','like',"%{$term}%")
                    ->paginate($this->_limit);
            endif;
            if (!empty($response['data'])):
                ListController::_setLikes($response['data']);
                ListController::_setLikeCounter($response['data']);
                ListController::_setMedia($response['data']);
                ListController::_setTops($response['data']);
                ListController::_setCategories($response['data']);
                ListController::_setLocations($response['data']);
            endif;
        else:
            $response = ['status' => 'fail', 'data' => []];
            $code = 400;
        endif;

        return response()->json($response, $code);
    }

    function create(Request $request) {
        $response = ['status' => 'fail', 'data' => []];
        $code = 400;
        $data = $request->all();
        $category = new Category();

        if ($category->validate($data)):
            $data['name'] = htmlentities($data['name'], ENT_QUOTES, 'utf-8');
            $cat = $category->create($data);

            if (empty($cat)):
                $code = 500;
                $response['data'] = $cat->errors();
            else:
                $code = 201;
                $response['status'] = 'created';
                $response['data'] = $cat;
            endif;
        else:
            $response['data'] = $category->errors();
        endif;

        return response()->json($response, $code);
    }

    function update(Request $request, $id) {
        $response = ['status' => 'fail', 'data' => []];
        $code = 400;
        $data = $request->all();
        $category = Category::where(['id',$id])->first();

        if ($category->validate($data)):
            $category->name = htmlentities($data['name'], ENT_QUOTES, 'utf-8');

            if ($category->save()):
                $code = 200;
                $response['status'] = 'updated';
                $response['data'] = $category;
            else:
                $code = 500;
                $response['data'] = $category->errors();
            endif;
        else:
            $response['data'] = $category->errors();
        endif;

        return response()->json($response, $code);
    }

    function delete($id) {
        $response = ['status' => 'fail', 'data' => []];
        $code = 400;
        ListCategory::where(['category_id',$id])->delete();
        $category = Category::where(['id',$id])->first();

        if ($category->delete()):
            $code = 200;
            $response['status'] = 'deleted';
        else:
            $code = 500;
            $response['data'] = $category->errors();
        endif;

        return response()->json($response, $code);
    }
}
