<?php

namespace App\Http\Controllers;

use App\Activity;
use App\HiddenUser;
use Carbon\Carbon;
use Illuminate\Http\Request;
use JWTAuth;
use App\Challenge;
use App\User;
use app\Helpers\Utils;
use App\UserList;
use App\ChallengeStatus;
use App\ListTop;
use App\Top;
use App\UserFollow;
use Illuminate\Support\Facades\DB;

class ChallengeController extends Controller
{

    private $_limit = 10;
    private $_page = 1;

    public function __construct()
    {
        $this->middleware('checkHiddenUser')->only(['store', 'replyChallenge', 'startChallenge', 'show', 'showByUser']);
    }

    private function _setPagination(Request $request)
    {
        !!$request->get('quantity') and ($this->_limit = $request->get('quantity'));
        !!$request->get('page') and ($this->_page = $request->get('page'));
    }

    public function _preparedChallenge($challenges)
    {
        foreach ($challenges as &$challenge):
            $challenge_serialize = Challenge::preparedChallenge($challenge);
            $challenge = $challenge_serialize;
        endforeach;
    }

    public function _preparedRequestChallange($request_challenges)
    {
        foreach ($request_challenges as &$request_challenge):
            $user_serialize = Challenge::prepareUser($request_challenge);
            $request_challenge = $user_serialize;
        endforeach;
    }

    public function index(Request $request)
    {

        $response = ['status' => 'ok', 'data' => []];
        $code = 200;
        $token = JWTAuth::getToken();
        $user = JWTAuth::toUser($token);

        $this->_setPagination($request);

        $request_challenges = Challenge::select("id as id_challenge", "owner_id as owner_id", "target_id as target_id", "owner_list_id as owner_list", "open_to as open_to", "deadline_date as deadline_date", "target_list_id as target_list_id")
            ->where(["target_id" => $user->id, "status" => 1])->whereNotIn('owner_id', HiddenUser::select('hidden_users.user_id as user_id')
                ->where('hidden_users.user_hidden_id', '=', $user->id)
                ->get())
            ->orderByDesc('created_at')
            ->skip(0)->take(3)->get();

        if ($request_challenges):
            $this->_preparedRequestChallange($request_challenges);
            $response["data"]["request_challanges"] = $request_challenges;
        else:
            $response["data"]["request_challanges"] = '';
        endif;

        $request_challenges_user = Challenge::select("id as id_challenge", "owner_id as owner_id", "target_id as target_id", "deadline_date as deadline_date")
            ->where(["owner_id" => $user->id, "status" => 2])
            ->whereNotIn('owner_id', HiddenUser::select('hidden_users.user_id as user_id')
                ->where('hidden_users.user_hidden_id', '=', $user->id)
                ->get())
            ->orderByDesc('created_at')
            ->skip(0)->take(3)->get();
        if ($request_challenges_user):
            $this->_preparedRequestChallange($request_challenges_user);
            $response["data"]["request_accept"] = $request_challenges_user;
        else:
            $response["data"]["request_accept"] = '';
        endif;

        $waiting_challenges_user = Challenge::select("id as id_challenge", "owner_id as owner_id", "target_id as target_id", "deadline_date as deadline_date")
        ->where(["target_id" => $user->id, "status" => 2])->whereNotIn('owner_id', HiddenUser::select('hidden_users.user_id as user_id')
            ->where('hidden_users.user_hidden_id', '=', $user->id)
            ->get())
        ->orderByDesc('created_at')
        ->skip(0)->take(3)->get();
        if ($waiting_challenges_user):
            $this->_preparedRequestChallange($waiting_challenges_user);
            $response["data"]["waiting_challenge"] = $waiting_challenges_user;
        endif;

        $now = Carbon::now();
        $challenges1 = Challenge::
        where('owner_id','=',$user->id)
        ->where('status','=',4)
        ->where('deadline_date','>=',$now->toDateTimeString())
        ->whereNotIn('owner_id', HiddenUser::select('hidden_users.user_id as user_id')
            ->where('hidden_users.user_hidden_id', '=', $user->id)
            ->get())
        ->orderByDesc('created_at')
        ->get();

        $challenges2 = Challenge::
        where('target_id','=',$user->id)
        ->where('status','=',4)
        ->where('deadline_date','>=',$now->toDateTimeString())
        ->whereNotIn('owner_id', HiddenUser::select('hidden_users.user_id as user_id')
            ->where('hidden_users.user_hidden_id', '=', $user->id)
            ->get())
        ->orderByDesc('created_at')
        ->get();
        

        $challenges = $challenges1->merge($challenges2);


        if ($challenges):
            $code = 200;
            if (count($challenges) > 0):
                $this->_preparedChallenge($challenges);
                $response['data']['challenges'] = $challenges;
            endif;
        endif;


        return response()->json($response, $code);
    }

    public function challengeRequest(Request $request)
    {

        $response = ['status' => 'ok', 'data' => []];
        $code = 200;
        $token = JWTAuth::getToken();
        $user = JWTAuth::toUser($token);
        $this->_setPagination($request);

        $challenges = Challenge::where(function ($where) use ($user) {
            $where->where('owner_id', $user->id);
        })->where(function ($where) {
            $where->where('status', 2);
            $where->orWhere('status', 3);
        })->where(function ($where) use ($user) {
            $where->whereNotIn('target_id', HiddenUser::select('hidden_users.user_id as user_id')
                ->where('hidden_users.user_hidden_id', '=', $user->id)
                ->get());
        })->orderByDesc('created_at')
            ->simplePaginate($this->_limit);

        if ($challenges):
            $this->_preparedChallenge($challenges);
            $response['data'] = $challenges;
        endif;

        return response()->json($response, $code);
    }

    public function store(Request $request)
    {

        $response = ['status' => 'fail', 'data' => []];
        $code = 200;
        $token = JWTAuth::getToken();
        $user = JWTAuth::toUser($token);
        $data = $request->all();
        $challenge = new Challenge;
        $data['owner_id'] = $user->id;
        $data['status'] = 1;
        $timezone = null;
        if ($user->timezone):
            $timezone = $user->timezone;
        endif;


        if ($challenge->validate($data)):
            $data['deadline_date'] = Carbon::now($timezone)->addDay($data['deadline_date']);
            $challenge->fill($data);
            $targetUser = User::find($challenge->target_id);
            if ($targetUser):
                if ($targetUser->id == $user->id):
                    //Throws error regarding the user is the same logged in, you cannot floowo yourself
                    $response ['data'] = 'User to attempt to challenge is the same logged in';
                    $code = 200;
                    return response()->json($response, $code);
                endif;
                $ownerList = UserList::where('id', $challenge->owner_list_id)
                    ->where('user_id', $challenge->owner_id)->where('enable', '=', 1)->first();
                if ($ownerList):
                    if ($ownerList->is_private != 1):
                        $target_list_id = $data['target_list_id'];
                        unset($data['target_list_id']);
                        $challenge = $challenge->create($data);
                        $challenge = Challenge::preparedChallenge($challenge);
                        if ($targetUser->token_android):
                            $firebaseNotification = new Utils();
                            $firebaseNotification->sendNotificationFirebase($targetUser->token_android, $user->username, $challenge, 'challenge_created');
                        endif;

                        $targetList = UserList::where('id', $target_list_id)
                            ->where('user_id', $targetUser->id)->where('enable', 1)->first();
                        if ($targetList):
                            if ($targetList->is_private != 1):
                                $cha = Challenge::where('id', $challenge->id)->first();
                                $cha->target_list_id = $targetList->id;
                                $cha->save();
                                //$challenge = Challenge::preparedChallenge($challenge);
                                $list = UserList::where('id', '=', $targetList->id)->where('enable', '=', 1)->first();
                                $list->tops = self::searchTops($list);
                                $challenge->target_list= $list;
                            else:
                                $response = ['message' => 'The list you attempt to challenge, is private'];
                                $response['data'] = $targetList;
                                $code = 200;
                                return response()->json($response, $code);
                            endif;
                        else:
                            $response['data'] = 'The list you attempt to challenge, is not valid';
                            $code = 200;
                            return response()->json($response, $code);
                            endif;

                        $this->addImageMini($challenge);
                        $response = ['status' => 'created', 'data' => $challenge];
                        $code = 201;
                    else:
                        $response['status'] = 'ok';
                        $response['message'] = 'The list you attempt to challenge, is private';
                        $response['data'] = $ownerList;
                        $code = 200;
                    endif;
                else:
                    //Throws error regarding the user is trying to set a not exists list
                    $response ['data'] = 'The list you attempt to challenge, is not valid';
                    $code = 200;
                endif;
            else:
                //Throws error regarding the user is trying to set a not exists user
                $response['data'] = 'User to attempt to challenge does not exists';
                $code = 200;
            endif;
        else:
            $response['data'] = $challenge->errors();
        endif;

        return response()->json($response, $code);
    }


    public function _prepareLists($lists)
    {
        foreach ($lists as &$list):
            $list = UserList::prepareListPublic($list);
        endforeach;
    }

    public static function searchTops($list)
    {

        $listTops = ListTop::select('tops.id', 'list_tops.order', 'tops.name')
            ->join('tops', 'tops.id', '=', 'list_tops.top_id')
            ->where(['list_id' => $list->id])
            ->orderBy('order', 'asc')
            ->get();
        $searchTop = Top::getTops($listTops); //send array

        return $searchTop;
    }

    function addImageMini($challenge){

        $im = imagecreatefromjpeg('images/DesingChallenge.jpg');
        //$im1 = imagecreatefromjpeg('images/DesingChallenge2.jpg');
        // get the image size
        $w = imagesx($im);
        $h = imagesy($im);

        // place some text (top, left)
        //imagettftext($im, 60, 0, 300, 100, 100, 'OpenSans-Regular','Hello');
        imagettftext($im, 36, 0, 460, 280, 0xffffff, 'open-sans/OpenSans-Regular.ttf', 'Best NFL Quaterback');
        imagettftext($im, 20, 0, 300, 400, 0xffffff, 'open-sans/OpenSans-Regular.ttf', 'Ranking by');
        imagettftext($im, 36, 0, 300, 445, 0xffffff, 'open-sans/OpenSans-Regular.ttf', $challenge['owner_user']['firstname']);
        imagettftext($im, 20, 0, 300, 480, 0xffffff, 'open-sans/OpenSans-Regular.ttf', $challenge['owner_user']['username']);
        $posHeight = 580;
        
        for ($i = 1; $i <= sizeof($challenge['owner_list']['tops']); $i++):
            imagettftext($im, 36, 0, 300, $posHeight, 0xffffff, 'open-sans/OpenSans-Regular.ttf', $i.". ".$challenge['owner_list']['tops'][$i-1]['name']);
            $posHeight +=50;
        endfor;

        imagettftext($im, 20, 0, 1000, 400, 0xffffff, 'open-sans/OpenSans-Regular.ttf', 'Ranking by');
        imagettftext($im, 36, 0, 1000, 445, 0xffffff, 'open-sans/OpenSans-Regular.ttf', $challenge['target_user']['firstname']);
        imagettftext($im, 20, 0, 1000, 480, 0xffffff, 'open-sans/OpenSans-Regular.ttf', $challenge['target_user']['username']);
        $posHeight = 580;
        
        for ($i = 1; $i <= sizeof($challenge['target_list']['tops']); $i++):
            imagettftext($im, 36, 0, 1000, $posHeight, 0xffffff, 'open-sans/OpenSans-Regular.ttf', $i.". ".$challenge['target_list']['tops'][$i-1]['name']);
            $posHeight +=50;
        endfor;

        $nameImage = "";
        $idChallenge= (string) $challenge['id'];
        $nameImage = "image_mini/challenge".$idChallenge.".jpg";
        
        imageJpeg($im, $nameImage, 85);
        $namenewImage = "http://top5dapp.com/public/". $nameImage;
        Challenge::find($challenge['id'])->update(['image_mini' => $namenewImage]);
        return $nameImage;        
    }

    public function replyChallenge(Request $request, $id)
    {
        $response = ["status" => "ok", "data" => []];
        $code = 200;
        $token = JWTAuth::getToken();
        $user = JWTAuth::toUser($token);
        $data = $request->all();
        $challenge = Challenge::where('id', $id)
            ->where('target_id', $user->id)->where('status', 1)->first();
        if ($challenge):
            if ($challenge->status == 1):
                if ($challenge->validateResponse($data)):
                    $status = $data['status'];
                    if ($status == 2 || $status == 3):
                        $owner_user = User::find($challenge->owner_id);
                        $status_notification = $status == 2 ? 'Accept challenge' : 'Rejected challenge';
                        if ($owner_user->token_android):
                            $firebaseNotification = new Utils();
                            $firebaseNotification->sendNotificationFirebase($owner_user->token_android, $status_notification, $challenge, 'challenge_accept');
                        endif;
                        $challenge->status = $status;
                        $challenge->save();
                        $challenge = Challenge::preparedChallenge($challenge);
                        $response["status"] = "ok";
                        $response["data"] = $challenge;
                        $code = 200;
                    else:
                        $response['data'] = 'The reply you attempt to set, is not valid';
                        $code = 200;
                    endif;
                else:
                    $response['data'] = $challenge->errors();
                endif;
            else:
                $response['data'] = 'The challenge has already been replied';
                $code = 200;
            endif;
        else:
            $response["data"] = 'The challenge you attempt reply, is not valid';
            $code = 200;
        endif;

        return response()->json($response, $code);
    }

    public function startChallenge(Request $request, $idChallenge)
    {
        $response = ['status' => 'fail', 'data' => []];
        $code = 400;
        $token = JWTAuth::getToken();
        $user = JWTAuth::toUser($token);
        $timezone = null;
        if ($user->timezone):
            $timezone = $user->timezone;
        endif;
        $data = $request->all();

        $challenge = Challenge::where('id', $idChallenge)
            ->where('owner_id', $user->id)
            ->where(function ($where) {
                $where->where('status', 1);
                $where->orWhere('status', 2);
            })
            ->first();

        if ($challenge):
            if ($challenge->status == 2 || $challenge->status == 1):
                if ($challenge->validateResponse($data)):
                    $status = $data['status'];
                    if ($status == 4 || $status == 5):
                        if ($status == 4):
                            $challenge->start_date = Carbon::now($timezone);
                            $action = new Activity();
                            $action->track([
                                'origin' => $user->id,
                                'target' => $challenge->id,
                                'action' => 'challenge',
                                'affected' => $challenge->target_id
                            ]);
                        endif;
                    else:
                        $response['data'] = 'The approve/disapprove you attempt to set, is not valid';
                        $code = 200;
                        return response()->json($response, $code);
                    endif;
                    //push notification
                    $target_user = User::find($challenge->target_id);
                    if ($target_user->token_android):
                        $firebaseNotification = new Utils();
                        $firebaseNotification->sendNotificationFirebase($target_user->token_android, "Start challenge", $challenge, 'challenge_confirm');
                    endif;

                    $challenge->status = $status;
                    $challenge->save();
                    $challenge = Challenge::preparedChallenge($challenge);
                    $response["status"] = "ok";
                    $response["data"] = $challenge;
                    $code = 200;
                else:
                    $response['data'] = $challenge->errors();
                endif;
            else:
                $response['data'] = 'The status of challenge you attempt approve/disapprove, is not valid';
                $code = 404;
            endif;
        else:
            $response["data"] = 'The challenge you attempt approve/disapprove, is not valid';
            $code = 404;
        endif;

        return response()->json($response, $code);
    }

    public function acceptsChallenge(Request $request)
    {

        $response = ['status' => 'fail', 'data' => []];
        $code = 400;
        $token = JWTAuth::getToken();
        $user = JWTAuth::toUser($token);
        $this->_setPagination($request);
        $request_challenges_user = Challenge::select("id as id_challenge", "owner_id as owner_id", "target_id as target_id")
            ->where(["owner_id" => $user->id, "status" => 4])->paginate($this->_limit);
        if ($request_challenges_user):
            $this->_preparedRequestChallange($request_challenges_user);
            $response["data"] = $request_challenges_user;
            $code = 200;
        endif;

        return response()->json($response, $code);
    }

    public function pendingForApproval(Request $request)
    {

        $response = ['status' => 'fail', 'data' => []];
        $code = 400;
        $token = JWTAuth::getToken();
        $user = JWTAuth::toUser($token);
        $this->_setPagination($request);
        $request_challenges = Challenge::select("id as id_challenge", "owner_id as owner_id", "target_id as target_id")
            ->where(["target_id" => $user->id, "status" => 1])->where(function ($where) use ($user) {
                $where->whereNotIn('owner_id', HiddenUser::select('hidden_users.user_id as user_id')
                    ->where('hidden_users.user_hidden_id', '=', $user->id)
                    ->get());
            })->orderByDesc('created_at')->paginate($this->_limit);

        if ($request_challenges):
            $response['status'] = 'ok';
            $this->_preparedRequestChallange($request_challenges);
            $response["data"] = $request_challenges;
            $code = 200;
        endif;

        return response()->json($response, $code);
    }

    public function createdByMe(Request $request)
    {
        $token = JWTAuth::getToken();
        $user = JWTAuth::toUser($token);
        $this->_setPagination($request);
        $response = ["status" => "fail", "data" => []];
        $challenges = Challenge::where(['owner_id' => $user->id, "status" => 1])->where(function ($where) use ($user) {
            $where->whereNotIn('target_id', HiddenUser::select('hidden_users.user_id as user_id')
                ->where('hidden_users.user_hidden_id', '=', $user->id)
                ->get());
        })->paginate($this->_limit);
        if ($challenges):
            $code = 200;
            $response["status"] = "ok";
            if (count($challenges) > 0):
                $response["status"] = 'ok';
                $this->_preparedChallenge($challenges);
                $response['data'] = $challenges;
            endif;
        endif;
        return response()->json($response, $code);
    }

    public function show($id, Request $request)
    {

        $token = JWTAuth::getToken();
        $user = JWTAuth::toUser($token);

        $response = ['status' => 'ok', 'data' => []];
        $code = 200;

        $challenge = Challenge::find($id);

        if ($challenge):
            if (self::validateChallenge($challenge, $user->id)):
                $challenge = Challenge::preparedChallenge($challenge);
                $response['data'] = $challenge;
            endif;
        else:
            $response['status'] = 'ok';
            $code = 200;
        endif;

        return response()->json($response, $code);
    }

    private static function validateChallenge($challenge, $idUser)
    {

        if ($challenge->owner_id == $idUser):
            return true;
        elseif ($challenge->open_to == 1 || $challenge->open_to ==null):
            return true;
        else:
            $follow = UserFollow::select('user_follows.id')
                ->where(function ($where) use ($idUser, $challenge) {
                    $where->where('user_id', $idUser);
                    $where->where('follows_to', $challenge->owner_id);
                })->orWhere(function ($where) use ($idUser, $challenge) {
                    $where->where('user_id', $idUser);
                    $where->where('follows_to', $challenge->target_id);
                })->get();

            if (count($follow) > 1):
                return true;
            endif;
        endif;

        return false;
    }

    public function showByUser($id, Request $request)
    {
        $response = ['status' => 'fail', 'data' => []];
        $this->_setPagination($request);
        $code = 200;

        $token = JWTAuth::getToken();
        $user = JWTAuth::toUser($token);

        $challengeMutual = Challenge::select('challenges.*')
            ->join(DB::raw('(select user_follows.* from user_follows '
                . 'where user_follows.user_id =' . $user->id . ') as user_follow_owner '), function ($join) {
                $join->on('user_follow_owner.follows_to', '=', 'challenges.owner_id');
            })->join(DB::raw('(select user_follows.* from user_follows '
                . 'where user_follows.user_id =' . $user->id . ') as user_follow_target '), function ($join) {
                $join->on('user_follow_target.follows_to', '=', 'challenges.target_id');
            })->Where(function ($where) {
                $where->where('open_to', 2);
            })->Where(function ($where) use ($id) {
                $where->where('owner_id', $id)
                    ->orWhere('target_id', $id);
            })->where('status',4);

        $challenges = Challenge::select('challenges.*')
            ->Where(function ($where) {
                $where->where('open_to', 1);
            })->Where(function ($where) use ($id) {
                $where->where('owner_id', $id)
                    ->orWhere('target_id', $id);
            })->where('status',4)->union($challengeMutual)->simplePaginate($this->_limit);


        /* $challenges_by_user = Challenge::orWhere(function ($where) use ($id) {
          $where->where('owner_id', $id)
          ->orWhere('target_id', $id);
          })->Where(function ($where) {
          $where->where('status', 4)
          ->orWhere('status', 6);
          })->orderBy('created_at', 'DESC')->paginate($this->_limit); */

        if ($challenges):
            $this->_preparedChallenge($challenges);
            $response["status"] = 'ok';
            $response["data"] = $challenges;
        endif;

        return response()->json($response, $code);
    }

}
