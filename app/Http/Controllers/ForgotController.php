<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\PasswordReset;
use App\Helpers\Utils;
use Mail;

class ForgotController extends Controller
{
    //
    public function resetForgot($token)
    {

    }

    public function forgotPassword(Request $request)
    {

        $response = ['status' => 'fail', 'data' => []];
        $code = 200;
        $email = User::whereEmail($request->email)->first();

        if ($email) {
            $token_exist = PasswordReset::where("email", $email->email)->where("status", 1)->first();
            $utls = new Utils();
            if (!$token_exist) {

                $token = $token = $utls->createToken(50);

                $token_user = new PasswordReset();
                while ($token_user->where("token", $token)->first()):
                    $token = $utls->createToken(50);
                endwhile;

                $user_token = new PasswordReset();
                $user_token->token = $token;
                $user_token->email = $request->email;
                $user_token->save();

                if ($user_token) {
                    $data = ['email' => $request->email, 'username' => $email->username, 'subject' => 'Reset password', 'link' => 'wwww.dev.com'];
                    $this->sendMessageLink($data);
                }
            }else{
                $data = ['email' => $request->email, 'username' => $email->username, 'subject' => 'Reset password', 'link' => 'wwww.dev.com'];
                $this->sendMessageLink($data);
            }
            $response = ['status' => 'ok', 'data' => ["status" => "ok","message"=>"Send"]];
            $code = 200;


        }else{
            $response = ['status' => 'ok', 'data' => ["status" => "false","message"=>"This email does not exist"]];
        }

        return response()->json($response, $code);

    }

    private function sendMessageLink($data)
    {

        Mail::send("templates.forgotpassword", compact("data"), function ($message) use ($data) {
            $message->from('not-repeat@gmail.com');
            $message->to($data['email']);
            $message->subject($data['subject']);
        });
    }
}
