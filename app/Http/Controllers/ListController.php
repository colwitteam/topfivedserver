<?php

namespace App\Http\Controllers;

use App\Events\ChallengeCancelUser;
use app\Helpers\RequestCurl;
use app\Helpers\Utils;
use App\HiddenUser;
use App\ListPopularity;
use App\Report;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
Use App\Events\ListChangePopularities;
use JWTAuth;
use App\Category;
use App\Location;
use App\ListCategory;
use App\ListComment;
use App\ListFavorite;
use App\ListHashtag;
use App\ListImage;
use App\ListLocation;
use App\ListTop;
use App\ListVideo;
use App\User;
use App\UserList;
use App\Hashtag;
use App\Top;
use App\Challenge;
use App\Activity;
use Carbon\Carbon;
use File;
use App\UserFollow;

class ListController extends Controller
{

    private $_limit = 10;
    private $_page = 1;

    private function _setPagination(Request $request)
    {
        !!$request->get('quantity') and ($this->_limit = $request->get('quantity'));
        !!$request->get('page') and ($this->_page = $request->get('page'));
    }

    public function __construct()
    {

        $this->_limit = config('app.pagination.quantity');
        $this->middleware('checkHiddenUser')->only(['getList', 'getAllByUser', 'setLike', 'commentList',
            'getListComments', 'getLocationsList', 'getUsersLikesByList']);
    }

    public function _prepareLists($lists)
    {
        foreach ($lists as &$list):
            $list = UserList::prepareList($list);
        endforeach;
    }

    private function _prepareTreding($tredings)
    {
        foreach ($tredings as &$treding):
            $topic = ListHashtag::preparedHashtagTop($treding);
        endforeach;
    }

    public function _serializeComments($comments)
    {

        foreach ($comments as &$comment):
            $comment_serialize = ListComment::prepareComment($comment);
            $comment = $comment_serialize;
        endforeach;
    }

    public function _preparedUserLazy($users)
    {
        foreach ($users as &$user):
            $user = User::prepareLazy($user);
        endforeach;
    }

    public function _preparedUserLazyReferences($users)
    {
        {
            foreach ($users as &$user):
                $user = User::prepareLazyReferences($user);
            endforeach;
        }
    }

    private function _validateCategoryLocation($value, $kind)
    {

        $result = ['passed' => true, 'message' => ''];

        if ($result['passed'] and !is_array($value)):
            $result['message'] = "{$kind} must be array.";
            $result['passed'] = false;
        endif;

        if ($result['passed']):
            $ids = [];
            switch ($kind):
                case 'Locations':
                    $ids = Location::getCurrentLocsId();
                    break;
                case 'Categories':
                    $ids = Category::getCurrentCatsId();
                    break;
            endswitch;

            if (sizeof(array_diff($value, $ids)) > 0):
                $result['message'] = "Unknown {$kind} given.";
                $result['passed'] = false;
            endif;
        endif;

        return $result;
    }

    private function _uploadMedia($b64, $type = 'image', $path)
    {
        $comp = explode(';', $b64);
        $ext = explode('/', $comp[0])[1];
        $img = explode(',', $comp[1])[1];
        $media = base64_decode($img);
        $ext === 'jpeg' and ($ext = 'jpg');
        $filename = $path . md5(str_random(8)) . ".{$ext}";

        return [$filename, $media];
    }

    function getMyLists(Request $request)
    {
        $this->_setPagination($request);
        $response = ['status' => 'ok', 'data' => []];
        $code = 200;

        $token = JWTAuth::getToken();
        $user = JWTAuth::toUser($token);

        $queries = ["query" => $request->get("query"), "id_user" => $user->id, /* "category" => $request->get("category"), */
            "top" => $request->get("top"),  "location" => $request->get("location"),
            "hashtag" => $request->get("hashtag"),'order'=>$request->get('order')];

        $lists = UserList::filter($queries)->paginate($this->_limit);
        if ($lists):
            foreach ($lists as $list) {
                if ($list->user_id != $user->id and $list->is_private == 1):
                    $list = new \stdClass();
                endif;
                $this->_prepareLists([$list]);
            }
            $response['data'] = $lists;
        endif;
        return response()->json($response, $code);
    }

    function getAllByUser($user, Request $request)
    {
        $this->_setPagination($request);
        $response = ['status' => 'ok', 'data' => []];
        $code = 200;
        $queries = ["query" => $request->get("query"), "id_user" => $user, "category" => $request->get("category"),
            "top" => $request->get("top"), "location" => $request->get("location"),'order'=>$request->get('order'), "hashtag" => $request->get("hashtag")];

        $lists = UserList::filter($queries)->paginate($this->_limit);
        if (empty($lists)):
            $code = 200;
        else:
            foreach ($lists as $list) {
                if ($list->user_id != $user and $list->is_private == 1):
                    $list = new \stdClass();
                endif;
                $this->_prepareLists([$list]);
            }
        endif;
        $response['data'] = $lists;
        return response()->json($response, $code);
    }

    /**
     * @todo Validates what happened with the privates lists
     * @param unknown $list
     * @return \Illuminate\Http\JsonResponse
     */
    function getList($list)
    {

        $response = ['status' => 'fail', 'data' => []];
        $code = 200;
        $list = UserList::where('id', '=', $list)->where('enable', '=', 1)->first();

        $token = JWTAuth::getToken();
        $user = JWTAuth::toUser($token);
        if ($list):
            if (empty($list->id)):
                $code = 200;
            else:
                $response["status"] = "ok";
                if ($list->user_id == $user->id && $list->is_private == 1):
                    $this->_prepareLists([$list]);

                    $response['data'] = $list;
                elseif ($list->is_private == 0):
                    $this->_prepareLists([$list]);

                    $response['data'] = $list;
                endif;
            endif;

        else:
            $code = 200;
            $response["status"] = "not found list";
        endif;
        return response()->json($response, $code);
    }

    function getAll(Request $request)
    {

        $this->_setPagination($request);

        $response = ['status' => 'ok', 'data' => []];
        $code = 200;
        $queries = ["query" => $request->get("query"), "category" => $request->get("category"),
            "top" => $request->get("top"), "location" => $request->get("location"),
            "order"=>$request->get('order'),
            "hashtag" => $request->get("hashtag"), 'id_user' => $request->get('id_user')];

        $lists = UserList::filter($queries)->paginate($this->_limit);

        if (empty($lists)):
            $code = 200;
        else:
            $this->_prepareLists($lists);
            $response['data'] = $lists;
        endif;

        return response()->json($response, $code);
    }

    function create(Request $request)
    {
        $response = ['status' => 'fail', 'data' => []];
        $code = 400;
        $token = JWTAuth::getToken();
        $user = JWTAuth::toUser($token);
        $data = $request->all();
        $list = new UserList();
        $data['user_id'] = $user->id;

        if ($list->validate($data)):
            $dataMedia = null;
            $dataTop = json_decode($data['top'], true);
            //$dataCatLoc = json_decode($data['categoryLocation']);
            if (isset($data['media'])) :
                $dataMedia = json_decode($data['media']);
            endif;

            $data = $list->setLocations($user, $data);

            /* if (empty($dataCatLoc->categories) and empty($dataCatLoc->locations)):
              $response['data'] = 'Locations and Categories should not be empty at same time.';
              return response()->json($response, $code);
              endif; */

            /* if (!empty($dataCatLoc->categories)):
              $catLocValid = $this->_validateCategoryLocation($dataCatLoc->categories, 'Categories');

              if ($catLocValid['passed'] === false):
              $response['data'] = $catLocValid['message'];
              return response()->json($response, $code);
              endif;
              endif; */

            /* if (!empty($dataCatLoc->locations)):
              $catLocValid = $this->_f;;validateCategoryLocation($dataCatLoc->locations, 'Locations');

              if ($catLocValid['passed'] === false):
              $response['data'] = $catLocValid['message'];
              return response()->json($response, $code);
              endif;
              endif; */

            if (!empty($dataTop) and sizeof($dataTop) <= 10):

                //dd($data["comments"]);
                $newList = $list->create($data);
              /*  if(isset($data["comments"])):
                    $comment = ListComment::create([
                        'list_id'=>$newList->id,
                        'user_id'=>$user->id,
                        'comment'=>$data['comments']
                    ]);
                endif;*/

                if (!empty($newList)):
                    /* if (!empty($dataCatLoc->categories)):
                      foreach ($dataCatLoc->categories as $cat):
                      $catList = ListCategory::create(['list_id' => $newList->id, 'category_id' => $cat]);
                      endforeach;
                      endif;

                      if (!empty($dataCatLoc->locations)):
                      foreach ($dataCatLoc->locations as $location):
                      $catList = ListLocation::create(['list_id' => $newList->id, 'location_id' => $location]);
                      endforeach;
                      endif; */

                    $tops = [];
                    //validation for any previous top list is not neeeded.
                    for ($i = 1; $i <= sizeof($dataTop); $i++):
                        $tops[] = $dataTop["top$i"];
                    endfor;

                    ListTop::saveTops($newList->id, $tops);
                    if(isset($data['hashtags'])){
                        ListHashtag::saveTags($newList->id, explode(' ', $data['hashtags']));
                    }
                    
                    if (!empty($dataMedia)):
                        foreach ($dataMedia as $mmedia):

                            $dir = $mmedia->type === 'image' ? 'list_pics/' : 'list_videos/';
                            $media = $this->_uploadMedia($mmedia->content, $mmedia->type, $dir);

                            $data['media'] = url('/') . '/' . $media[0];

                            $obj = $mmedia->type === 'image' ? new ListImage() : new ListVideo();
                            $obj->{$mmedia->type} = $data['media'];
                            $obj->list_id = $newList->id;
                            $obj->order = empty($data['order']) ? 0 : 0 + $data['order'];

                            if ($obj->save()):
                                if (!File::exists(public_path('list_videos/'))) {
                                    File::makeDirectory(public_path('list_videos/', 0775, true));
                                }
                                file_put_contents(public_path() . '/' . $media[0], $media[1]);
                                /* $response['data'] = $pic; */
                                /* $response['status'] = 'Saved'; */
                                $code = 200;
                            else:
                                $response['data'] = $obj->errors();
                            endif;
                        endforeach;
                    endif;

                    $this->_prepareLists([$newList]);
                    /* $newList->hashtags = $data['hashtags']; */

                    $code = 201;
                    //$image_mini =  $this->addImageMini($newList);
                    $response['image_mini'] = $this->addImageMini($newList);
                    $response['status'] = 'created';
                    $response['data'] = $newList;
                    
                    
                else:
                    $response['data'] = $list->errors();
                endif;
            else:
                $response['status'] = 'not_enough_params';
            endif;
        else:
            $response['data'] = $list->errors();
        endif;
        return response()->json($response, $code);
    }

    function addImageMini($newList){

        $im = imagecreatefromjpeg('images/DesingList.jpg');
        //$im1 = imagecreatefromjpeg('images/DesingList2.jpg');
        // get the image size
        $w = imagesx($im);
        $h = imagesy($im);

        // place some text (top, left)
        //imagettftext($im, 60, 0, 300, 100, 100, 'OpenSans-Regular','Hello');
        imagettftext($im, 32, 0, 190, 290, 0xffffff, 'open-sans/OpenSans-Regular.ttf', 'My ranking for:');
        imagettftext($im, 32, 0, 190, 350, 0xffffff, 'open-sans/OpenSans-Regular.ttf', $newList['name']);
        $posHeight = 470;
        for ($i = 1; $i <= sizeof($newList['tops']); $i++):
            //$tops[] = $dataTop["top$i"];
            imagettftext($im, 32, 0, 190, $posHeight, 0xffffff, 'open-sans/OpenSans-Regular.ttf', $newList['tops'][$i-1]['name']);            
            $posHeight +=60;
        endfor;
        
        $nameImage = "";
        $idList= (string) $newList['id'];
        $nameImage = "image/".$idList.".jpg";
        //imageJpeg($im, $nameImage, 85);
        
        imageJpeg($im, $nameImage, 85);
        $namenewImage = "http://top5dapp.com/public/". $nameImage;
        UserList::find($newList['id'])->update(['image_mini' => $namenewImage]);
        return $nameImage;

        //imageJpeg($im, $nameImage, 85);
        //$list = new UserList();
        //UserList::find($newList['id'])->update(['image_mini' => $nameImage]);
    }

    function updateByUser(Request $request, $list)
    {

        $response = ['status' => 'fail', 'data' => []];
        $code = 400;

        $token = JWTAuth::getToken();
        $user = JWTAuth::toUser($token);

        $data = $request->all();
        //Must be the owner of the list
        $list = UserList::where(['user_id' => $user->id, 'id' => $list, 'enable' => 1])->first();

        if (empty($list)):
            $response['status'] = 'List_not_found';
            $code = 404;
        else:
            $data = $list->_cleanupData($data);
            if ($list->validateUpdate($data)):
                $list_private = $list->is_private;
                //ensures always to be the exact same user
                $data['user_id'] = $user->id;
                //$dataCatLoc = new \stdClass();
                if (!empty($data['latitude']) and !empty($data['longitude'])):
                    $data = $list->setLocation($data);
                endif;
                /* if (!empty($data['categoryLocation'])):
                  $dataCatLoc = json_decode($data['categoryLocation']);
                  if (!empty($dataCatLoc->categories)):
                  $catLocValid = $this->_validateCategoryLocation($dataCatLoc->categories, 'Categories');

                  if ($catLocValid['passed'] === false):
                  $response['data'] = $catLocValid['message'];
                  return response()->json($response, $code);
                  endif;
                  endif;
                  if (!empty($dataCatLoc->locations)):
                  $catLocValid = $this->_validateCategoryLocation($dataCatLoc->locations, 'Locations');

                  if ($catLocValid['passed'] === false):
                  $response['data'] = $catLocValid['message'];
                  return response()->json($response, $code);
                  endif;
                  endif;
                  endif; */

                $list->fill($data);
                if ($list->save()):
                    if ($list_private != $request->is_private):
                        event(new ListChangePopularities($list));
                    endif;
                    if (!empty($data['top'])):
                        $dataTop = json_decode($data['top'], true);
                        if (!empty($dataTop) and sizeof($dataTop) <= 10):
                            $currentLocs = ListLocation::where('list_id', $list->id)->delete();

                            $tops = [];
                            //validation for any previous top list is not neeeded.
                            for ($i = 1; $i <= sizeof($dataTop); $i++):
                                $tops[] = $dataTop["top$i"];
                            endfor;
                            ListTop::saveTops($list->id, $tops);
                        endif;
                    endif;

                    /* if (!empty($dataCatLoc->categories)):
                      ListCategory::where('list_id', $list->id)->delete();
                      foreach ($dataCatLoc->categories as $cat):
                      ListCategory::create(['list_id' => $list->id, 'category_id' => $cat]);
                      endforeach;
                      endif;
                      if (!empty($dataCatLoc->locations)):
                      ListLocation::where('list_id', $list->id)->delete();
                      foreach ($dataCatLoc->locations as $location):
                      ListLocation::create(['list_id' => $list->id, 'location_id' => $location]);
                      endforeach;
                      endif; */

                    empty($data['hashtags']) or ListHashtag::saveTags($list->id, explode(' ', $data['hashtags']));

                    $this->_prepareLists([$list]);
                    $response['status'] = 'ok';
                    $response['data'] = $list;
                    $code = 201;

                else:
                    $response['data'] = $list->errors();
                endif;

            else:
                $response['data'] = $list->errors();
            endif;

        endif;

        return response()->json($response, $code);
    }

    function deleteByUser($list)
    {
        $response = ['status' => 'fail', 'data' => []];
        $code = 400;

        $token = JWTAuth::getToken();
        $user = JWTAuth::toUser($token);
        $timezone = null;
        if ($user->timezone):
            $timezone = $user->timezone;
        endif;
        $now = Carbon::now($timezone);

        $list = UserList::where(['user_id' => $user->id, 'id' => $list])->first();
        if (empty($list->id)):
            $response['status'] = 'List_not_found';
            $code = 404;
        else:
            $challenge = Challenge::where('owner_list_id', $list->id)->orWhere("target_list_id", $list->id)
                ->where('deadline_date', '<', $now)->whereIn('status', ['1', '3', '5', '6'])->first();

            if (!$challenge):
                /* ListTop::where(['list_id' => $list->id])->delete();
                  ListImage::where(['list_id' => $list->id])->delete();
                  ListFavorite::where(['list_id' => $list->id])->delete();
                  ListCategory::where(['list_id' => $list->id])->delete();
                  ListLocation::where(['list_id' => $list->id])->delete(); */
                $list_populary = ListPopularity::where('list_id', $list->id)->first();
                if ($list_populary):
                    $list_populary->delete();
                endif;
                $list->delete();
                if ($list->trashed()):
                    $list->putListPopular();
                    //Get all list_tops in the deleted list
                    $response['status'] = 'deleted';
                    $code = 200;
                else:
                    $response['data'] = $list->errors();
                endif;
            else:
                $challenge->status = 5;
                $challenge->save();
                $response['status'] = 'fail';
                $response['data'] = 'List no delete, belogs to a challenge.';
                $code = 200;
            endif;
        endif;

        return response()->json($response, $code);
    }

    // Favorites
    function getMyLikes(Request $request)
    {
        $this->_setPagination($request);
        $response = ['status' => 'ok', 'data' => []];
        $code = 200;

        $token = JWTAuth::getToken();
        $user = JWTAuth::toUser($token);

        $lists = UserList::select('lists.*')
            ->join('list_favorites', 'lists.id', '=', 'list_favorites.list_id')
            ->join('users', 'users.id', '=', 'lists.user_id')
            ->where(['list_favorites.user_id' => $user->id])
            ->whereNotIn('lists.user_id', HiddenUser::select('user_id')->where('user_hidden_id', '=', $user->id)->get())
            ->where('lists.is_private', 0)
            ->where('lists.enable', '=', 1)
            ->where('users.banned', '=', 0)
            ->paginate($this->_limit);

        if (empty($lists)):
            $code = 200;
        else:
            $this->_prepareLists($lists);
            $response['data'] = $lists;
        endif;

        return response()->json($response, $code);
    }

    function setLike(Request $request)
    {
        $response = ['status' => 'fail', 'data' => []];
        $code = 400;

        $token = JWTAuth::getToken();
        $user = JWTAuth::toUser($token);

        $data = $request->all();
        $like = new ListFavorite();
        $data['user_id'] = $user->id;
        
        if ($like->validate($data)):        
            $list = UserList::select('lists.id as id', 'lists.name as name', 'users.id as user_id', 'users.token_android as token')
                ->join('users', 'users.id', '=', 'lists.user_id')
                ->where('lists.id', '=', $data['list_id'])->first();
                            
            if ($list):

                $likes = ListFavorite::where(['user_id' => $user->id, 'list_id' => $data['list_id']])->first();            
                if ($likes):
                    $response['data'] = $likes;
                    $response['status'] = 'ok';
                    $code = 200;
                else:
                    if ($list->token):
                        $firebaseNotification = new Utils();
                        $firebaseNotification->sendNotificationFirebase($list->token, 'Like list', $list, 'like_list');
                    endif;                
                    $like->fill($data);
                    
                    if ($like->save()):

                        $response['data'] = $like;
                        $response['status'] = 'ok';
                        $code = 200;
                        $action = new Activity();

                        $action->track([
                            'origin' => $user->id,
                            'target' => $like->id,
                            'action' => 'liked',
                            'affected' => $list->user_id
                        ]);


                    else:
                        $response['data'] = $like->errors();
                    endif;
                endif;
            else:
                $response['data'] = 'The list you attempt to like, is not valid';
                $code = 404;
            endif;
        else:
            $response['data'] = $like->errors();
        endif;

        return response()->json($response, $code);
    }

    function removeLikes($list)
    {
        $response = ['status' => 'unliked', 'data' => []];
        $code = 200;

        $token = JWTAuth::getToken();
        $user = JWTAuth::toUser($token);

        $list = ListFavorite::where(['user_id' => $user->id, 'list_id' => $list])->first();
        if (!empty($list->id)):
            if (!$list->delete()):
                $response['data'] = $list->errors();
                $response['status'] = 'fail';
                $code = 200;
            endif;
        endif;

        return response()->json($response, $code);
    }

    /**
     *
     * @param Request $request
     * @param unknown $list
     * @return \Illuminate\Http\JsonResponse
     */
    function uploadListPic(Request $request, $list)
    {
        $response = ['status' => 'fail', 'data' => []];
        $code = 400;

        $data = $request->all();
        $images = json_decode($data["pic"]);

        if (empty($images)):
            return response()->json($response, $code);
        endif;

        $token = JWTAuth::getToken();
        $user = JWTAuth::toUser($token);

        $list = UserList::where(['user_id' => $user->id, 'id' => $list])->first();
        $order = ListImage::where('list_id', $list->id)->max('order');

        foreach ($images as $image):

            if (!empty($list->id)):
                $order = $order + 1;
                $media = $this->_uploadMedia($image, 'image', 'list_pics/');

                if (!File::exists(public_path('list_pics/'))):
                    File::makeDirectory('list_pics/', 0775, true);
                endif;

                $url_image = url('/') . '/' . $media[0];

                $pic = new ListImage();
                $pic->image = $url_image;
                $pic->list_id = $list->id;
                $pic->order = $order;

                if ($pic->save()):
                    file_put_contents(public_path() . '/' . $media[0], $media[1]);
                    $response['data'][] = $pic;
                    $response['status'] = 'Saved';
                    $code = 200;
                else:
                    $response['data'] = $pic->errors();
                endif;
            endif;
        endforeach;
        return response()->json($response, $code);
    }

    /**
     *
     * @param unknown $pic
     * @return \Illuminate\Http\JsonResponse
     */
    function deleteImage($pic)
    {
        $response = ['status' => 'fail', 'data' => []];
        $code = 400;

        $token = JWTAuth::getToken();
        $user = JWTAuth::toUser($token);

        $pic = ListImage::find($pic);

        if (!empty($pic->id)):
            //validates if is the list owner
            $list = UserList::where(['user_id' => $user->id, 'id' => $pic->list_id])->first();
            if (!empty($list->id)):
                if ($pic->delete()):
                    $path = str_replace(url('/'), '', $pic->image);
                    file_exists(public_path() . $path) and unlink(public_path() . $path);
                    $response['status'] = 'deleted';
                    $code = 200;
                else:
                    $response['data'] = $pic->errors();
                endif;
            endif;
        endif;

        return response()->json($response, $code);
    }

    function commentList(Request $request)
    {
        $response = ['status' => 'fail', 'data' => []];
        $code = 200;
        $data = $request->all();

        $token = JWTAuth::getToken();
        $user = JWTAuth::toUser($token);

        $data['user_id'] = $user->id;
        $listComment = new ListComment();

        if ($listComment->validate($data)):
            $list = UserList::where('id', '=', $data['list_id'])->first();
            if ($list):

                $list_comment = $listComment->create($data);

                $this->setHashtags($list_comment, $list);
                if ($list_comment):
                    $this->_serializeComments([$list_comment]);
                    $response['data'] = $list_comment;
                    $response['status'] = 'ok';
                    $code = 200;
                    $action = new Activity();
                    $action->track([
                        'origin' => $user->id,
                        'target' => $listComment->id,
                        'action' => 'comment',
                        'affected' => $list->user_id
                    ]);
                    $user_affect = User::find($list->user_id);
                    $firebaseNotification = new Utils();

                    $firebaseNotification->sendNotificationFirebase($user_affect->token_android, 'Comment list', $list, 'comment_list');
                    $mentions = $listComment->extractUsers($listComment->comment);
                    if (!empty($mentions)):
                        foreach ($mentions as $uid):
                            $action = new Activity();
                            $action->track([
                                'origin' => $user->id,
                                'target' => $listComment->id,
                                'action' => 'mention',
                                'affected' => $uid
                            ]);
                            $user_mension = User::find($uid);
                            $firebaseNotification->sendNotificationFirebase($user_mension->token_android, 'Mentions list', $list, 'mention_comment');

                        endforeach;
                    endif;
                else:
                    $response['data'] = $listComment->errors();
                endif;
            else:
                $response['data'] = 'The list you attempt to comment, is not valid';
                $code = 200;
            endif;
        else:
            $response['data'] = $listComment->errors();
        endif;

        return response()->json($response, $code);
    }

    public function setHashtags($comment, $list)
    {

        $comment_list = new ListComment();

        $hashtags = $comment_list->extractHashtags($comment->comment);

        foreach ($hashtags as $hashtag):
            $name_hashtag = Hashtag::where('keyword', $hashtag)->first();
            if (!$name_hashtag):
                $name_hashtag = Hashtag::create(['keyword' => $hashtag]);
            endif;
            //    $comment_hashtag = ListHashtag::where(['hasgtag_id'=>$name_hashtag->id,'list_id'=>$list])->first();
            $list_hashtag = new ListHashtag();
            $list_hashtag->list_id = $list->id;
            $list_hashtag->hashtag_id = $name_hashtag->id;
            $list_hashtag->comment_id = $comment->id;
            $list_hashtag->save();

        endforeach;;
    }

    function getListComments(Request $request, $id)
    {
        if (empty($id)):
            return response()->json(['status' => 'fail', 'data' => 'List id is mandatory'], 400);
        endif;
        $token = JWTAuth::getToken();
        $user = JWTAuth::toUser($token);
        $this->_setPagination($request);
        $response = ['status' => 'ok', 'data' => []];
        $code = 200;

        $comments = ListComment::join('users', 'users.id', '=', 'list_comments.user_id')
            ->select('list_comments.*', 'users.username')
            ->where(['list_comments.list_id' => $id])
            ->whereNotIn('users.id', HiddenUser::select('user_id')->where('user_hidden_id', '=', $user->id)->get())
            ->orderByDesc('created_at')
            ->paginate($this->_limit);

        if ($comments->isEmpty()):
            $code = 200;
            $response['status'] = 'Comment_not_found';
        else:
            $this->_serializeComments($comments);

            $response['data'] = $comments;
        endif;

        return response()->json($response, $code);
    }

    function deleteListComments(Request $request, $id)
    {
        if (empty($id)):
            return response()->json(['status' => 'fail', 'data' => 'List id is mandatory'], 200);
        endif;

        $this->_setPagination($request);
        $response = ['status' => 'ok', 'data' => []];
        $code = 200;

        $token = JWTAuth::getToken();
        $user = JWTAuth::toUser($token);

        $comment = ListComment::where(['user_id' => $user->id, 'id' => $id])->first();
        if (empty($comment)):
            $response['status'] = 'Comment_not_found';
            $code = 200;
        elseif ($comment->delete()):
            $list_comment = ListHashtag::where('comment_id', $comment->id)->delete();
            $response['status'] = 'deleted';
            $code = 200;
        else:
            $response['data'] = $comment->errors();
        endif;

        return response()->json($response, $code);
    }

    function uploadVideo(Request $request)
    {
        $response = ['status' => 'fail', 'data' => []];
        $code = 400;
        $data = $request->all();
        $videos = json_decode($data["video"]);

        if (empty($videos)):
            return response()->json($response, $code);
        endif;

        $token = JWTAuth::getToken();
        $user = JWTAuth::toUser($token);

        $list = UserList::where(['user_id' => $user->id, 'id' => $data['list_id']])->first();
        $order = ListImage::where('list_id', $list->id)->max('order');

        foreach ($videos as $vid):
            if (!empty($list->id)):
                $order = $order + 1;
                $media = $this->_uploadMedia($vid, 'video', 'list_videos/');
                if (!File::exists(public_path('list_pics/'))):
                    File::makeDirectory('list_videos/', 0775, true);
                endif;
                $url_video = url('/') . '/' . $media[0];

                $video = new ListVideo();
                $video->video = $url_video;
                $video->list_id = $list->id;
                $video->order = $order;

                if ($video->save()):
                    file_put_contents(public_path() . '/' . $media[0], $media[1]);
                    $response['data'][] = $video;
                    $response['status'] = 'Saved';
                    $code = 200;
                else:
                    $response['data'] = $video->errors();
                endif;
            endif;
        endforeach;
        return response()->json($response, $code);
    }

    function deleteVideo($id)
    {
        $response = ['status' => 'fail', 'data' => []];
        $code = 400;

        $token = JWTAuth::getToken();
        $user = JWTAuth::toUser($token);

        $vid = ListVideo::find($id);

        if (!empty($vid->id)):
            //validates if is the list owner
            $list = UserList::where(['user_id' => $user->id, 'id' => $vid->list_id])->first();
            if (!empty($list->id)):
                if ($vid->delete()):
                    $path = str_replace(url('/'), '', $vid->video);
                    file_exists(public_path() . $path) and unlink(public_path() . $path);
                    $response['status'] = 'deleted';
                    $code = 200;
                else:
                    $response['data'] = $vid->errors();
                endif;
            endif;
        endif;

        return response()->json($response, $code);
    }

    function getTrendingTopics(Request $request)
    {
        $token = JWTAuth::getToken();
        $user = JWTAuth::toUser($token);
        $timezone = null;
        if ($user->timezone):
            $timezone = $user->timezone;
        endif;
        $this->_setPagination($request);
        $response = ['status' => 'ok', 'data' => []];
        $code = 200;
        $date = Carbon::now($timezone)->subDays(1)->toDateTimeString();
        /*
                $tops_populars = Top::select(DB::raw('tops.id as id'),DB::raw('tops.name as keyword'),DB::raw('if(tops.id>0,5,3) as target'),DB::raw('count(list_tops.top_id) as trending'))
                    ->join('list_tops','list_tops.top_id','=','tops.id')
                    ->join('lists','lists.id','=','list_tops.list_id')
                    ->join('list_popularities','list_popularities.list_id','=','lists.id')
                    ->whereNotIn('lists.user_id',
                        HiddenUser::select('hidden_users.user_id as user_id')
                            ->where('hidden_users.user_hidden_id', '=', $user->id)->get()
                    )
                    ->where('lists.enable',1)
                    ->skip(0)
                    ->take(10)
                    ->groupBy('tops.id');*/

        $lists_theme_treding = UserList::select('lists.id as id', 'lists.name as keyword', DB::raw('if(lists.id>0,5,3) as target'),
            DB::raw('COUNT( lists.name ) AS trending'))
            ->groupBy('lists.name')
            ->orderBy('trending', 'DESC')
            ->skip(0)->take(10);

        $trending = ListHashtag::select(DB::raw('list_hashtags.hashtag_id as id'), DB::raw('hashtags.keyword as keyword'), DB::raw('if(hashtags.id>0,3,5) as target'), DB::raw('count(list_hashtags.hashtag_id) as trending'))
            ->join('hashtags', 'hashtags.id', '=', 'list_hashtags.hashtag_id')
            ->unionAll($lists_theme_treding)
            ->where('list_hashtags.created_at', '>', $date)
            ->groupBy('list_hashtags.hashtag_id')
            ->orderBy('trending', 'DESC')
            ->simplePaginate($this->_limit);

        if ($trending->isEmpty()):
            $code = 200;
            $response['status'] = 'ok';
        else:
            $this->_prepareTreding($trending);
            $response['data'] = $trending;
        endif;

        return response()->json($response, $code);
    }

    function getLocationsList(Request $request, $list, $milles)
    {

        $response = ['status' => 'ok', 'data' => []];
        $code = 200;

        $token = JWTAuth::getToken();
        $user = JWTAuth::toUser($token);
        $this->_setPagination($request);

        $list = UserList::find($list);

        if ($list):
            $lat = ($list->latitude) ? $list->latitude : $user->latitude;
            $long = ($list->longitude) ? $list->longitude : $user->longitude;
            $api_key = config('app.google_api.api_key'); //'AIzaSyDUWtCjAIZy1qDzlsKmmDBS7NyIuYElLgU';            
            $geocode = file_get_contents('https://maps.googleapis.com/maps/api/geocode/json?latlng=' . $lat . ',' . $long . '&key=' . $api_key);
            $jsonGeocode = json_decode($geocode);

            if ($jsonGeocode->results):
                $data = ['Address' => $jsonGeocode->results[0]->formatted_address,
                    'City' => $jsonGeocode->results[1]->address_components[1]->long_name,
                    'State/Prov' => $jsonGeocode->results[1]->address_components[2]->long_name,
                    'Country' => $jsonGeocode->results[1]->address_components[3]->long_name];

                $userLikes = ListFavorite::getUsersLikes($list);
                $radius = ($milles * (1.60934));

                $nearbyUsers = User::select('users.id', 'users.firstname', 'users.lastname', 'users.username', 'users.private', DB::raw('ACOS( SIN( RADIANS( `latitude` ) ) '
                    . '* SIN( RADIANS( ' . $lat . ') )'
                    . ' + COS( RADIANS( `latitude` ) ) * COS( RADIANS( ' . $lat . ' ) ) '
                    . '* COS( RADIANS( `longitude` ) - RADIANS( ' . $long . ' ) ) ) '
                    . '* 6380 AS distance'))
                    ->where(DB::raw('ACOS( SIN( RADIANS( `latitude` ) ) * SIN( RADIANS( ' . $lat . ') )'
                        . ' + COS( RADIANS( `latitude` ) )* COS( RADIANS( ' . $lat . ' ) ) '
                        . '* COS( RADIANS( `longitude` ) - RADIANS( ' . $long . ' ) ) ) '
                        . '* 6380 '), "<", $radius)
                    ->whereIn('users.id', $userLikes)
                    ->whereNotIn('users.id',
                        HiddenUser::select('user_id')->where('user_hidden_id', '=', $user->id)->get())
                    ->where('users.banned', '=', 0)
                    ->orderBy('distance', 'DESC')
                    ->paginate($this->_limit);

                foreach ($nearbyUsers as &$user):
                    $user->user = User::prepareLazy($user);
                endforeach;

                $response['status'] = 'ok';
                $response['data']['location'] = $data;
                $response['data']['users'] = $nearbyUsers;
            else:
                $response['data'] = $jsonGeocode;
            endif;
        else:
            $code = 200;
        endif;

        return response()->json($response, $code);
    }

    public function getUsersLikesByList($id, Request $request)
    {
        $this->_setPagination($request);
        $response = ['status' => 'fail', 'data' => []];
        $code = 200;
        $token = JWTAuth::getToken();
        $user = JWTAuth::toUser($token);
        $users = User::select('users.id')
            ->join('list_favorites', 'list_favorites.user_id', '=', 'users.id')
            ->join('lists', 'lists.id', '=', 'list_favorites.list_id')
            ->where('list_favorites.list_id', $id)
            ->whereNotIn('users.id', HiddenUser::select('user_id')->where('user_hidden_id', '=', $user->id)->get())
            ->where('lists.enable', '=', 1)
            ->where('lists.deleted_at', null)
            ->paginate($this->_limit);

        if ($users):
            $code = 200;
            $response['status'] = 'ok';
            $this->_preparedUserLazyReferences($users);
            foreach ($users as $follower):
                $follower = UserFollow::setIsFollowing($follower, $user->id, $follower->id);
            endforeach;
            $response['data'] = $users;
        endif;
        return response()->json($response, $code);
    }

    //Admin
    public function getListAdmin(Request $request)
    {
        $response = ['status' => 'fail', 'data' => []];
        $code = 200;
        $this->_setPagination($request);
        $queries = ["name" => $request->get("name"),
            "top" => $request->get("top"),
            "hashtag" => $request->get("hashtag"),
            "term" => $request->get("term")];

        $lists = UserList::filterAdmin($queries)->paginate($this->_limit);
        if ($lists):
            $response['status'] = 'ok';
            $this->_prepareLists($lists);
            $response['data'] = $lists;
        endif;
        return response()->json($lists, $code);
    }

    //Admin
    public function getListByIdAdmin($id)
    {
        $response = ['status' => 'fail', 'data' => []];
        $code = 200;

        $list = UserList::find($id);
        if ($list):
            $this->_prepareLists([$list]);
            $response['status'] = 'ok';
            $response['data'] = $list;
        endif;

        return response()->json($response, $code);
    }

    //Admin
    public function statusListEnabled($id, Request $request)
    {

        $token = JWTAuth::getToken();
        $user = JWTAuth::toUser($token);
        $response = ['status' => 'fail', 'data' => []];
        $code = 200;
        if ($user->role == 2):
            $list = UserList::find($id);

            if ($list):
                $enable = true;
                if ($request->status == 2):
                    $enable = false;
                endif;
                $list->enable = $request->status;
                $list->save();
                event(new ListChangePopularities($list));
                $this->_prepareLists([$list]);
                $response['status'] = 'ok';
                $response['data'] = $list;
            endif;
            $code = 200;
        else:
            $response['status'] = 'fail';
            $response['data'] = ['message' => 'User not admin'];
        endif;

        return response()->json($response, $code);
    }

    public function getListByHashtag(Request $request, $hashtag)
    {

        $this->_setPagination($request);
        $response = ['status' => 'fail', 'data' => []];
        $code = 200;
        $hashtag = str_replace('#', '', $hashtag);
        $queries = ['hashtag' => $hashtag];
        $lists = UserList::filter($queries)->paginate($this->_limit);

        $this->_prepareLists($lists);

        if (!$lists->isEmpty()):
            $response['status'] = ['ok'];
            $response['data'] = $lists;
        else:
            $code = 200;
        endif;

        return response()->json($response, $code);
    }

    public function getReportsList(Request $request)
    {
        $response = ['status' => 'fail', 'data' => []];
        $lists = Report::select('lists.*', DB::raw('(SELECT count(*) FROM lists li inner join reports on li.id = reports.affect_id  WHERE li.id = rep.affect_id and reports.target = 1) as total_list'))
            ->join('lists', 'lists.id', '=', 'rep.affect_id')
            ->join('users', 'users.id', '=', 'lists.user_id');
        $lists->from(DB::raw('reports rep'));
        if ($request->name):
            $lists->where('lists.name', 'like', '' . $request->name . '%');
        endif;
        if ($request->term):
            $term = $request->term;
            $lists->where(function ($where) use ($term) {
                $where->where('users.firstname', 'like', $term . '%');
                $where->orWhere('users.lastname', 'like', $term . '%');
                $where->orWhere('users.username', 'like', $term . '%');
            });
        endif;
        if ($request->hashtag):
            $hashtag = $request->hashtag;
            $lists->leftJoin('list_hashtags', 'list_hashtags.list_id', '=', 'lists.id');
            $lists->leftJoin('hashtags', 'hashtags.id', '=', 'list_hashtags.hashtag_id');
            $lists->where('hashtags.keyword', 'like', $hashtag . '%');
        endif;
        if ($request->top):
            $top = $request->top;
            $lists->leftJoin("list_tops", "list_tops.list_id", "=", "lists.id");
            $lists->leftJoin("tops", "tops.id", "=", "list_tops.top_id");
            $lists->where("tops.name", "like", "{$top}%");
        endif;
        $order = $request->order == 0 ? 1 : 2;
        $lists->where('lists.enable', $order);
        $lists->where('lists.deleted_at', null);
        $lists->groupBy('lists.id');
        $lists->orderByDesc('rep.created_at');
        $lists = $lists->where('target', 1)->paginate($this->_limit);
        //return response()->json($lists->where('rep.target',1)->toSql());
        if (!$lists->isEmpty()) {
            $response['status'] = 'ok';
            $response['data'] = $lists;
        }
        $code = 200;
        return response()->json($response, $code);
    }

    public function getListsTheme($theme,Request $request){
        $response=['status'=>'fail','data'=>[]];
        $code=200;
        $this->_setPagination($request);
        $lists = UserList::themes($theme)->paginate($this->_limit);
        if($lists):
            $response['status']='ok';
            $response['data'] = $lists;
        endif;
        return response()->json($response,$code);
    }

}
