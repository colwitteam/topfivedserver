<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\UserList;
use App\Challenge;



class ListPublicController extends Controller
{
    public function listShow($list)
    {
        $response = ['status' => 'fail', 'data' => []];
        $code = 200;
        $list = UserList::where('id', '=', $list)->where('enable', '=', 1)->first();
        
        if ($list):
            if (empty($list->id)):
                $code = 200;
            else:
                $response["status"] = "ok";
                $this->_prepareLists([$list]);
                $list = UserList::prepareListPublic($list);
                $response['data'] = $list;
            endif;

        else:
            $code = 200;
            $response["status"] = "Not Found List";
        endif;
        $im = imagecreatefromjpeg('images/DesingList.jpg');
        //$im1 = imagecreatefromjpeg('images/DesingList2.jpg');
        // get the image size
        $w = imagesx($im);
        $h = imagesy($im);

        // place some text (top, left)
        //imagettftext($im, 60, 0, 300, 100, 100, 'OpenSans-Regular','Hello');
        imagettftext($im, 32, 0, 190, 290, 0xffffff, 'open-sans/OpenSans-Regular.ttf', 'My ranking for:');
        imagettftext($im, 32, 0, 190, 340, 0xffffff, 'open-sans/OpenSans-Regular.ttf', 'hello');
        $posHeight = 420;
        for ($i = 1; $i <= 5; $i++):
            //$tops[] = $dataTop["top$i"];
            imagettftext($im, 32, 0, 190, $posHeight, 0xffffff, 'open-sans/OpenSans-Regular.ttf', $i.". ".'helloe');            
            $posHeight +=50;
        endfor;
        imageJpeg($im, "images/DesingList2.jpg", 85);
        //$nameImage = "";
        //imagejpeg($im);
        //$nameImage = "image/DesingList2.jpg";
        return view('image');
        //return $nameImage;
        //return response()->json($response, $code);
        //return view('appSocial',['date'=>$response,'code'=>$code]);
    }

    function addImageMini($newList){

        $im = imagecreatefromjpeg('images/DesingList.jpg');
        //$im1 = imagecreatefromjpeg('images/DesingList2.jpg');
        // get the image size
        $w = imagesx($im);
        $h = imagesy($im);

        // place some text (top, left)
        //imagettftext($im, 60, 0, 300, 100, 100, 'OpenSans-Regular','Hello');
        imagettftext($im, 32, 0, 190, 290, 0xffffff, 'open-sans/OpenSans-Regular.ttf', 'My ranking for:');
        imagettftext($im, 32, 0, 190, 350, 0xffffff, 'open-sans/OpenSans-Regular.ttf', $newList['name']);
        $posHeight = 470;
        for ($i = 1; $i <= sizeof($newList['tops']); $i++):
            //$tops[] = $dataTop["top$i"];
            imagettftext($im, 32, 0, 190, $posHeight, 0xffffff, 'open-sans/OpenSans-Regular.ttf',  "1.".$newList['tops'][$i-1]['name']);            
            $posHeight +=60;
        endfor;
        $nameImage = "";
        $idList= (string) $newList['id'];
        $nameImage = "image/".$idList.".jpg";
        return $nameImage;

        //imageJpeg($im, $nameImage, 85);
        //$list = new UserList();
        //UserList::find($newList['id'])->update(['image_mini' => $nameImage]);
    }

    public function _prepareLists($lists)
    {
        foreach ($lists as &$list):
            $list = UserList::prepareListPublic($list);
        endforeach;
    }

    public function challengeShow($id)
    {    	
        $response = ['status' => 'ok', 'data' => []];
        $code = 200;

        $challenge = Challenge::find($id);

        if ($challenge):        
            $challenge = Challenge::preparedChallengePublic($challenge);
            $response['data'] = $challenge;        
        endif;
        //var_dump($challenge);

        //return response()->json($response, $code);
        //return view('appSocialChallenge', ['date'=>$response,'code'=>$code]);
        $im = imagecreatefromjpeg('images/DesingChallenge.jpg');
        //$im1 = imagecreatefromjpeg('images/DesingChallenge2.jpg');
        // get the image size
        $w = imagesx($im);
        $h = imagesy($im);

        // place some text (top, left)
        //imagettftext($im, 60, 0, 300, 100, 100, 'OpenSans-Regular','Hello');
        imagettftext($im, 36, 0, 460, 280, 0xffffff, 'open-sans/OpenSans-Regular.ttf', 'Best NFL Quaterback');
        imagettftext($im, 20, 0, 300, 400, 0xffffff, 'open-sans/OpenSans-Regular.ttf', 'My ranking for:');
        imagettftext($im, 36, 0, 300, 445, 0xffffff, 'open-sans/OpenSans-Regular.ttf', 'hello');
        imagettftext($im, 20, 0, 300, 480, 0xffffff, 'open-sans/OpenSans-Regular.ttf', 'hello');
        $posHeight = 580;
        for ($i = 1; $i <= 5; $i++):
            //$tops[] = $dataTop["top$i"];
            imagettftext($im, 36, 0, 300, $posHeight, 0xffffff, 'open-sans/OpenSans-Regular.ttf', $i.". ".'helloe');            
            $posHeight +=50;
        endfor;
        imagettftext($im, 20, 0, 1000, 400, 0xffffff, 'open-sans/OpenSans-Regular.ttf', 'My ranking for:');
        imagettftext($im, 36, 0, 1000, 445, 0xffffff, 'open-sans/OpenSans-Regular.ttf', 'hello');
        imagettftext($im, 20, 0, 1000, 480, 0xffffff, 'open-sans/OpenSans-Regular.ttf', 'hello');
        $posHeight = 580;
        for ($i = 1; $i <= 5; $i++):
            //$tops[] = $dataTop["top$i"];
            imagettftext($im, 36, 0, 1000, $posHeight, 0xffffff, 'open-sans/OpenSans-Regular.ttf', $i.". ".'helloe');            
            $posHeight +=50;
        endfor;
        imageJpeg($im, "images/DesingChallenge2.jpg", 85);
        //$nameImage = "";
        //imagejpeg($im);
        //$nameImage = "image/DesingList2.jpg";
        return view('image');
        //return $nameImage;
        //return response()->json($response, $code);
        //return view('appSocial',['date'=>$response,'code'=>$code]);
    }    
}
