<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\ListLocation;
use App\UserList;
use App\Location;
use App\Http\Controllers\ListController;

class LocationController extends Controller
{
    
    private $_limit = 10;
    private $_page = 1;

    private function _setPagination(Request $request) {
        !!$request->get('quantity') and ($this->_limit = $request->get('quantity'));
        !!$request->get('page') and ($this->_page = $request->get('page'));
    }

    public function __construct() {
        $this->_limit = config('app.pagination.quantity');
    }
    
    function show($term, Request $request) {
        $response = ['status' => 'fail', 'data' => []];
        $code = 200;

        if (empty($term)):
            $response['data'] = Location::all();
            $response['status'] = 'Ok';
        else:
            if (is_numeric($term)):
                $response['data'] = Location::find($term);
            elseif (is_string($term)):
                $params = array();
                parse_str($request->term, $params);
                $strSearch = null;
                 if(is_array($params) and count($params) > 0){
                    $strSearch = Location::select('locations.*');
                    foreach ($params as $key => $param) {
                        $strSearch->orWhere('name', 'like', "%{$param}%");
                    }
                    $response['status'] = 'Ok';
                    $response['data'] = $strSearch->get();
                }
            endif;
        endif;

        return response()->json($response, $code);
    }

    function all(Request $request) {
        $this->_setPagination($request);
        $response = ['status' => 'ok', 'data' => [], 'page' => $this->_page, 'quantity' => $this->_limit];
        $code = 200;        
        $response['data'] = Location::select('locations.*')->paginate($this->_limit);

        return response()->json($response, $code);
    }

    function showLists(Request $request, $term) {
        $this->_setPagination($request);
        $response = ['status' => 'ok', 'data' => [], 'page' => $this->_page, 'quantity' => $this->_limit];
        $code = 200;

        if (!empty($term)):
            if (is_numeric($term)):
                $response['data'] = UserList::join('list_locations', 'lists.id', '=', 'list_locations.list_id')                    
                    ->select('lists.*')
                    ->where(['list_locations.location_id'=>$term])
                    ->paginate($this->_limit);
            elseif (is_string($term)):
                $response['data'] = UserList::join('list_locations', 'lists.id', '=', 'list_locations.list_id')
                    ->join('locations', 'locations.id', '=', 'list_locations.location_id')
                    ->select('lists.*')
                    ->where('locations.name','like',"%{$term}%")
                    ->paginate($this->_limit);
            endif;
            if (!empty($response['data'])):
               // ListController::_setLikes($response['data']);
               // ListController::_setLikeCounter($response['data']);
              //  ListController::_setImages($response['data']);
               // ListController::_setTops($response['data']);
               // ListController::_setCategories($response['data']);
               // ListController::_setLocations($response['data']);
            endif;
        else:
            $response = ['status' => 'fail', 'data' => []];
            $code = 400;
        endif;

        return response()->json($response, $code);
    }

    function create(Request $request) {
        $response = ['status' => 'fail', 'data' => []];
        $code = 400;
        $data = $request->all();
        $location = new Location();

        if ($location->validate($data)):
            $data['name'] = htmlentities($data['name'], ENT_QUOTES, 'utf-8');
            $cat = $location->create($data);

            if (empty($cat)):
                $code = 500;
                $response['data'] = $cat->errors();
            else:
                $code = 201;
                $response['status'] = 'created';
                $response['data'] = $cat;
            endif;
        else:
            $response['data'] = $location->errors();
        endif;

        return response()->json($response, $code);
    }

    function update(Request $request, $id) {
        $response = ['status' => 'fail', 'data' => []];
        $code = 400;
        $data = $request->all();
        $location = Location::where(['id',$id])->first();

        if ($location->validate($data)):
            $location->name = htmlentities($data['name'], ENT_QUOTES, 'utf-8');

            if ($location->save()):
                $code = 200;
                $response['status'] = 'updated';
                $response['data'] = $location;
            else:
                $code = 500;
                $response['data'] = $location->errors();
            endif;
        else:
            $response['data'] = $location->errors();
        endif;

        return response()->json($response, $code);
    }

    function delete($id) {
        $response = ['status' => 'fail', 'data' => []];
        $code = 400;
        ListLocation::where(['location_id',$id])->delete();
        $location = Location::where(['id',$id])->first();

        if ($location->delete()):
            $code = 200;
            $response['status'] = 'deleted';
        else:
            $code = 500;
            $response['data'] = $location->errors();
        endif;

        return response()->json($response, $code);
    }
}
