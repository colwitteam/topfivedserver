<?php

namespace App\Http\Controllers;

use App\Helpers\Util;
use app\Helpers\Utils;
use App\User;
use Illuminate\Http\Request;
use App\Message;

use JWTAuth;

class MessageController extends Controller
{
    private $_limit = 10;

    private function _setPagination(Request $request)
    {
        !!$request->get('quantity') and ($this->_limit = $request->get('quantity'));
        !!$request->get('page') and ($this->_page = $request->get('page'));
    }

    public function __construct()
    {
        $this->middleware('checkHiddenUser')->only(['store']);
    }

    private function _preparedChat($chats)
    {
        foreach ($chats as &$chat):
            $chat = Message::prepareChat($chat);
        endforeach;
    }

    private function _preparedChatGroup($chats)
    {
        foreach ($chats as &$chat):
            $chat = Message::prepareChatGroup($chat);
        endforeach;
    }

    private function _preparedMessage($messages)
    {
        foreach ($messages as &$message):
            $message = Message::prepareMessage($message);
        endforeach;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function _responseMessage($response)
    {

        $data = ['status' => 'fail', 'data' => []];
        $code = 200;
        if ($response != null):
            $data['status'] = 'ok';
            $data['data'] = $response;
            $code = 200;
        endif;

        return response()->json($data, $code);
    }

    public function index(Request $request)
    {
        $token = JWTAuth::getToken();
        $user = JWTAuth::toUser($token);
        $response = null;

        $this->_setPagination($request);

        $message = Message::message($user)->paginate($this->_limit);


        if ($message):
            $this->_preparedChatGroup($message);
            $response = $message;
        endif;
        return $this->_responseMessage($response);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $token = JWTAuth::getToken();
        $user = JWTAuth::toUser($token);
        $response = null;

        $data = $request->all();
        $data['user_transmitter'] = $user->id;
        $message = new Message();

        if ($message->validate($data)):
            $user_receiver = User::where('id',$request->user_receiver)->where('banned',0)->first();
            if($user_receiver):
                $new_message = $message->create($data);

                if ($new_message):
                    $this->_preparedMessage([$new_message]);
                    $response = $new_message;
                    $notifications = new Utils();

                    if ($user_receiver->token):
                        $notifications->sendNotificationFirebase($user_receiver->token, 'Message', $new_message, 'message');
                    endif;
                endif;
            endif;
        else:
            $response = $message->errors();
        endif;

        return $this->_responseMessage($response);
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id, Request $request)
    {
        $token = JWTAuth::getToken();
        $user = JWTAuth::toUser($token);
        $response = null;
        $this->_setPagination($request);
        $chat = Message::where(['user_receiver' => $user->id, 'user_transmitter' => $id])
            ->orWhere(['user_receiver' => $id, 'user_transmitter' => $user->id])
            ->orderBy('created_at', 'DESC')
            ->paginate($this->_limit);
        if ($chat):
            $this->_preparedChat($chat);
            $response = $chat;
        endif;
        return $this->_responseMessage($response);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }


}
