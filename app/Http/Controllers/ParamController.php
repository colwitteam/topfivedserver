<?php

namespace App\Http\Controllers;

use Response;
use App\Param;

class ParamController extends Controller
{
    public function index() {
        $params = Param::get();
        return response()->json(['status'=>'ok','data'=>$params],200);
    }
}
