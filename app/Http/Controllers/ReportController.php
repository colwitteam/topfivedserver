<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use JWTAuth;
use App\Report;
use App\UserList;
use App\User;
use Illuminate\Support\Facades\DB;

class ReportController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {

        $response = ['status' => 'fail', 'data' => []];
        $code = 200;

        $token = JWTAuth::getToken();
        $user = JWTAuth::toUser($token);
        $data = $request->all();
        $data['origin_id'] = $user->id;

        $report = new Report();

        if ($report->validate($data)):
            if ($data['target'] == 1 || $data['target'] == 2):
                if ($data['target'] == 1):
                    $affect = UserList::find($data['affect_id']);
                    if ($affect):
                        $affect = UserList::preparedListLazy($affect);
                    else:
                        $response['data'] = 'The list you attempt to report, is not valid';
                        return response()->json($response, $code);
                    endif;
                else:
                    $affect = User::find($data['affect_id']);
                    if ($affect):
                        $affect = User::prepareLazy($affect);
                    else:
                        $response['data'] = 'The user you attempt to report, is not valid';
                        return response()->json($response, $code);
                    endif;
                endif;
                $newReport = $report->create($data);
                if ($newReport):
                    $newReport = Report::preparedReport($newReport);
                    $newReport->affect_id = $affect;
                    $response = ['status' => 'ok', 'data' => $newReport];
                    $code = 200;
                endif;
            else:
                $response['data'] = 'The type of report you attempt to set, is not valid';
                $code = 200;
            endif;
        else:
            $response['data'] = $report->errors();
        endif;

        return response()->json($response, $code);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id,$type) {
        $code = 200;
        $type_report = 1;

        $selects = ['user_origin.id as user_origin_id','user_origin.firstname as user_origin_firstname',
            'user_origin.lastname as user_origin_lastname','user_origin.username as user_origin_username',
            'user_origin.email as user_origin_email', 'reports.id as report_id','reports.comment as comment'];
        $reports = Report::query();

        if($type=='user'):
            $type_report = 2;

            array_push($selects,'user_affect.id as user_affect_id','user_affect.firstname as user_affect_firstname',
                'user_affect.lastname as user_affect_lastname','user_affect.username as user_affect_username',
                'user_affect.email as user_affect_email');
        endif;

        $reports->select($selects);
        $response = ['status'=>'ok','data'=>[]];


        if($type_report==2):
            $reports->join(DB::raw('users as user_affect'),'user_affect.id','=','reports.affect_id');
       endif;
        $reports->join(DB::raw('users as user_origin'),'user_origin.id','=','reports.origin_id');

        $reports->where('affect_id',$id)->where('target',$type_report)->orderByDesc('reports.created_at');

        $reports = $reports->get();
        $response['data'] = $reports;
        return response()->json($response, $code);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id,$type) {
        $type_target = 1;
        if($type=='user'):
            $type_target = 2;
        endif;
        $reports = Report::where('affect_id',$id)->where('target',$type_target)->get();
        foreach ($reports as $report):
            $report->review = 1;
            $report->save();
        endforeach;
        return response()->json('change reports');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        //
    }

}
