<?php

namespace App\Http\Controllers;

use App\HiddenUser;
use App\UserList;
use Illuminate\Http\Request;
use App\ListSave;
use JWTAuth;
use Illuminate\Support\Facades\DB;

class SavelistController extends Controller
{
    private $_user;
    private $_limit = 10;
    private $_page = 1;

    public function __construct()
    {
        $this->middleware('checkHiddenUser')->only(['store', 'show']);
    }

    private function _setPagination(Request $request)
    {
        !!$request->get('quantity') and ($this->_limit = $request->get('quantity'));
        !!$request->get('page') and ($this->_page = $request->get('page'));
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $this->_setPagination($request);
        $response = ['status' => 'fail', 'data' => []];
        $code = 400;
        $token = JWTAuth::getToken();
        $this->_user = JWTAuth::toUser($token);

        $user_public = ListSave::select('list_saves.*')
            ->join('lists', 'lists.id', '=', 'list_saves.list_id')
            ->join('users', 'users.id', '=', 'lists.user_id')
            ->join(DB::raw('(select users.id as user_id from users where users.private = 0) as user_public'), function ($join) {
                $join->on('user_public.user_id', '=', 'lists.user_id');
            })->whereNotIn('lists.user_id', HiddenUser::select('hidden_users.user_hidden_id as users_hiddens')
                ->where('hidden_users.user_hidden_id', '=', $this->_user->id)
                //->union($users_disabled)
                ->get()
            )
            ->where('users.banned', 0)
            ->where('lists.enable', '=', 1)
            ->where('list_saves.user_id', $this->_user->id);


        $lists = ListSave::select('list_saves.*')
            ->join('lists', 'lists.id', '=', 'list_saves.list_id')
            ->join('users', 'users.id', '=', 'lists.user_id')
            ->join(DB::raw('(SELECT users.id AS id_user FROM users INNER JOIN user_follows ON user_follows.follows_to=users.id ' .
                'WHERE users.private = 1 AND user_follows.follows_to IN (SELECT user_follows.follows_to FROM user_follows WHERE ' .
                'user_follows.user_id=' . $this->_user->id . ')) AS user_private'), function ($join) {
                $join->on('user_private.id_user', '=', 'lists.user_id');
            })
            ->union($user_public)
            ->where('users.banned', 0)
            ->where('list_saves.user_id', $this->_user->id)
            ->where('lists.enable', '=', 1)
            ->whereNotIn('lists.user_id', HiddenUser::select('hidden_users.user_id as users_hiddens')
                ->where('hidden_users.user_hidden_id', '=', $this->_user->id)
                //->union($users_disabled)
                ->get()
            )
            ->where('lists.deleted_at', null)
            ->orderByDesc('created_at')
            ->simplePaginate($this->_limit);

        if (count($lists) > 0):
            $response['status'] = 'ok';
            $code = 200;
            foreach ($lists as $key => $list_saved):
                $lists_save[$key]['list_saved'] = $list_saved;
                $list = UserList::find($list_saved->list_id);
                if ($list):
                    $lists_save[$key]['list_saved']['list'] = UserList::prepareList($list);
                endif;
            endforeach;
            $response['data'] = $lists;
        else:
            $code = 200;
        endif;
        return response()->json($response, $code);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $token = JWTAuth::getToken();
        $this->_user = JWTAuth::toUser($token);
        $response = ['status' => 'fail', 'data' => []];
        $code = 400;
        $data = $request->all();
        $data['user_id'] = $this->_user->id;
        if (UserList::find($data['list_id'])):
            $code = 200;
            $list_user = ListSave::where(["user_id" => $this->_user->id, "list_id" => $data["list_id"]])->first();
            $response['data'] = "Already Saved";

            if (!$list_user):
                $listSave = new ListSave();
                $listSave->fill($data);
                if ($listSave->save()):
                    $code = 201;
                    $response['status'] = 'ok';
                    $response['data'] = $listSave;
                endif;
            endif;
        endif;
        return response()->json($response, $code);
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $response = ['status' => 'fail', 'data' => []];
        $code = 400;
        $token = JWTAuth::getToken();
        $this->_user = JWTAuth::toUser($token);
        $list_save = ListSave::where(['list_id' => $id, 'user_id' => $this->_user->id])->first();
        if ($list_save):
            $delete_list = $list_save->delete();
            if ($delete_list):
                $response["status"] = 'ok';
                $response["data"] = "remove list";
                $code = 200;
            endif;
        else:
            $code = 200;
            $response["data"] = "your not have list";
        endif;
        return response()->json($response, $code);
    }
}
