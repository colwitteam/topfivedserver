<?php

namespace App\Http\Controllers;

use App\HiddenUser;
use App\UserList;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Search;
use App\User;
use App\UserFollow;
use App\ListPopularity;
use JWTAuth;

class SearchController extends Controller {

    private $_user;
    private $_limit = 10;
    private $_page = '';

    private function _setPagination(Request $request) {
        !!$request->get('quantity') and ( $this->_limit = $request->get('quantity'));
        !!$request->get('page') and ( $this->_page = $request->get('page'));
    }

    public function _prepareUser($search_users) {
        foreach ($search_users as &$store):
            $user = User::find($store->user_id);
            $user_prepare = User::prepareLazy($user);
            $store['user_search'] = $user_prepare;
        endforeach;
    }

    public function _preparePopularUsersPopular($users) {
        foreach ($users as &$user):
            $user_find = User::find($user->id);
            $user->user = User::prepareLazy($user_find);
        endforeach;
    }

    public function _prepareListRecent($list_resents) {
        foreach ($list_resents as &$list_resent):
            $list_resent = UserList::preparedListLazy($list_resent);
            $user_list = User::find($list_resent->user_id);
            $list_resent->user = User::prepareLazy($user_list);
            $list_resent->recent = true;
        endforeach;
    }

    public function _preparePopularLists($popularLists) {
        foreach ($popularLists as &$popularList):
            $list = UserList::find($popularList->list_id);
            if ($list):
                $popularList->list = UserList::preparedListLazy($list);
            endif;
        endforeach;
    }

    public function _preparePopularUsers($users) {
        foreach ($users as &$user):
            $user->user = User::prepareLazy($user);
        endforeach;
    }

    private function _prepareSearches($searches) {
        foreach ($searches as &$search):
            $search = Search::prepareSearch($search);
        endforeach;
    }

    public function index(Request $request) {
        $token = JWTAuth::getToken();
        $this->_user = JWTAuth::toUser($token);
        $user = $this->_user;
        $this->_setPagination($request);
        $response = ['status' => 'fail', 'data' => []];
        $code = 400;

        $followings=$user->getMyFollowingsActive();

        $searches = Search::moreSearched(null, $followings, $this->_user->id)->simplePaginate($this->_limit);


        if ($searches->isEmpty()):
            $code = 200;
        else:
            $this->_prepareSearches($searches);
            $response = ['status' => 'ok', 'data' => $searches];
            $code = 200;
        endif;

        return response()->json($response, $code);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        $token = JWTAuth::getToken();
        $this->_user = JWTAuth::toUser($token);

        $response = ['status' => 'ok', 'data' => []];
        $code = 200;
        $data = $request->all();
        $store = new Search();
        $data['user_id'] = $this->_user->id;
        if ($store->validate($data)):

            if (array_key_exists('searched_item_id', $data)):
                $search = Search::where(['user_id' => $data['user_id']])
                                ->where(['target' => $data['target']])
                                ->where(['searched_item_id' => $data['searched_item_id']])->first();
                if ($search):
                    $this->_prepareUser([$search]);
                    $this->_prepareSearches([$search]);
                    $response["data"] = $search;
                    return response()->json($response, $code);
                endif;
            endif;

            $store->fill($data);
            $save_store = $store->save();
            if ($save_store):
                $this->_prepareUser([$store]);
                $this->_prepareSearches([$store]);
                $response["data"] = $store;
            endif;
        else:
            $response["data"] = $store->errors();
        endif;
        return response()->json($response, $code);
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        //
    }

    public function destroy($id) {
        
    }

    public function getRecentlySearchedLists(Request $request) {

        $code = 200;
        $response = ['status' => 'ok', 'data' => []];
        $id_resent = [];
        $token = JWTAuth::getToken();
        $this->_user = JWTAuth::toUser($token);
        $user = $this->_user;
        $followings=$user->getMyFollowingsActive();


        /* Recents by user */
        $mySearchs = Search::where('user_id', $user->id)
            ->where('target', 1)
            ->get();

        foreach ($mySearchs as $search):
            $id_lists = UserList::where('name', 'like', $search->term)->whereNotIn('user_id', HiddenUser::select('user_id')->where('user_hidden_id', '=', $this->_user->id)->get())
                            ->where('is_private', '=', 0)
                            ->where('enable', '=', 1)
                            ->skip(0)->take(10)->get();

            if ($id_lists):
                foreach ($id_lists as $id_list):
                    $id_resent[] = $id_list->id;
                endforeach;
            endif;
        endforeach;

        if (count($id_resent) > 0):
            $code = 200;
            $response["status"] = "ok";
            $list_resents = UserList::whereIn('id', $id_resent)->skip(0)->take(10)->get();
            if ($list_resents):
                $this->_prepareListRecent($list_resents);
            endif;
            $response['data']['recent'] = $list_resents;
        endif;

        return response()->json($response, $code);
    }

    public function getMoreSearchedLists(Request $request) {
        $this->_setPagination($request);
        $token = JWTAuth::getToken();
        $this->_user = JWTAuth::toUser($token);

        $response = ['status' => 'ok', 'data' => []];
        $code = 200;

        $followings = $this->_user->getMyFollowingsActive();
        $followings[] = $this->_user->id;

        $searches = Search::moreSearched($target = 1, $followings, $this->_user->id)
                ->simplePaginate($this->_limit);

        if (!$searches->isEmpty()):
            $this->_prepareSearches($searches);
            $response['data'] = $searches;
        endif;

        return response()->json($response, $code);
    }

    public function getMoreSearchedUsers(Request $request) {

        $this->_setPagination($request);
        $token = JWTAuth::getToken();
        $this->_user = JWTAuth::toUser($token);

        $response = ['status' => 'ok', 'data' => []];
        $code = 200;

        $followings = UserFollow::getFollowings($this->_user);
        $followings[] = $this->_user->id;

        $searches = Search::moreSearched($target = 2, $followings, $this->_user->id)->simplePaginate($this->_limit);

        if (!$searches->isEmpty()):
            $this->_prepareSearches($searches);
            $response['data'] = $searches;
        endif;

        return response()->json($response, $code);
    }

    public function getMoreSearchedTopics(Request $request) {

        $this->_setPagination($request);
        $token = JWTAuth::getToken();
        $this->_user = JWTAuth::toUser($token);

        $response = ['status' => 'ok', 'data' => []];
        $code = 200;

        $followings = UserFollow::getFollowings($this->_user);


        $searches = Search::moreSearched($target = 3, $followings, $this->_user->id)->simplePaginate($this->_limit);

        if (!$searches->isEmpty()):
            $this->_prepareSearches($searches);
            $response['data'] = $searches;
        endif;

        return response()->json($response, $code);
    }

    public function getPopularLists(Request $request) {

        $this->_setPagination($request);
        $token = JWTAuth::getToken();
        $this->_user = JWTAuth::toUser($token);

        $response = ['status' => 'fail', 'data' => []];
        $code = 200;

        $popularLists = ListPopularity::popularLists()->paginate($this->_limit);

        if ($popularLists->isEmpty()):
            $code = 200;
            $response['info'] = $popularLists;
        else:
            $response['status']='ok';
            $this->_preparePopularLists($popularLists);
            $response['data'] = $popularLists;
        endif;

        return response()->json($response, $code);
    }

    public function getPopularUsers(Request $request) {

        $this->_setPagination($request);
        $token = JWTAuth::getToken();
        $this->_user = JWTAuth::toUser($token);

        $response = ['status' => 'fail', 'data' => []];
        $code = 200;

        $popularUsers = User::select('users.id', DB::raw('max(list_popularities.quantity) as quantity'))
                ->join('lists', 'lists.user_id', '=', 'users.id')
                ->join('list_popularities', 'list_popularities.list_id', '=', 'lists.id')
                ->whereNotIn('users.id', HiddenUser::select('hidden_users.user_id as users_hiddens')
                    ->where('hidden_users.user_hidden_id', '=', $this->_user->id)
                    //->union($users_disabled)
                    ->get()
                )
                ->where('users.id','!=',$this->_user->id)
                ->where('banned', '=', 0)
                ->groupBy('users.id')
                ->orderBy('quantity', 'DESC')
                ->paginate($this->_limit);

        if ($popularUsers->isEmpty()):
            $code = 200;
            $response['info'] = $popularUsers;
        else:
            $response['status'] = 'ok';
            $popularUsers->user = $this->_preparePopularUsersPopular($popularUsers);
            $response['data'] = $popularUsers;
        endif;

        return response()->json($response, $code);
    }

    public function getPopularLocations(Request $request){
        $response=['status'=>'fail','data'=>[]];
        $code = 200;

       // $this->_setPagination($request);
        $locations= UserList::query();
        $locations->select('city','country',DB::raw('count(lists.city) as totale_city'));
        if($request->city):
            $locations->where('city','like',"{$request->city}%");
        endif;
        if($request->country):
            $locations->where('country','like',"{$request->country}%");
        endif;
        $locations->where('city','!=','')
            ->orderByDesc('totale_city')->groupBy('city');
        $locations = $locations->paginate($this->_limit);

        if($locations):
            $response['data']=$locations;
            $response['status']='ok';
        endif;

        return response()->json($response,$code);
    }

    public function getSearchedTops(Request $request){

        $this->_setPagination($request);
        $token = JWTAuth::getToken();
        $this->_user = JWTAuth::toUser($token);

        $response = ['status' => 'ok', 'data' => []];
        $code = 200;

        $followings = UserFollow::getFollowings($this->_user);


        $searches = Search::moreSearched($target = 5, $followings, $this->_user->id)->simplePaginate($this->_limit);

        if (!$searches->isEmpty()):
            $this->_prepareSearches($searches);
            $response['data'] = $searches;
        endif;

        return response()->json($response, $code);
    }

}
