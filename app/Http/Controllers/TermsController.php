<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Term;

class TermsController extends Controller
{

    public function store(Request $request){

    }

    public function show($terms){
        $response = ['status' => 'fail', 'data' => ['success'=>'fail']];
        $terms = Term::Where('kind',$terms)->first();
        if($terms){
            $response['status'] = 'ok';
            $response['data'] = ['success'=>'ok','data'=>$terms];
        }
        return response()->json($response);
    }
}
