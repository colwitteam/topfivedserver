<?php

namespace App\Http\Controllers;

use App\Award;
use App\Challenge;
use App\Events\ChallengeCancelUser;
use App\Events\EventActivitiesUser;
use App\Hashtag;
use App\Report;
use App\UserAward;
use App\UserList;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;
use Abraham\TwitterOAuth\TwitterOAuth;
use Response;
use App\Activity;
use App\User;
use App\ListHashtag;
use App\UserFollow;
use App\Helpers\Utils;
use App\HiddenUser;
use Session;
use Illuminate\Support\Facades\Validator;

/**
 * @apiDefine UserOkResponse Ok response for user endpoint.
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *       "status": "ok",
 *       "data": [
 *        {
 *              "id": 1,
 *              "firstname": "John",
 *              "lastname": "Doe",
 *              "username": "JohnDoe",
 *              "email": "joh@doe.com"
 *              "profile_pic": "url/to/pic.jpeg"
 *          }
 *       ]
 *     }
 */

/**
 * @todo finish documentation
 * @author rantes
 */
class UserController extends Controller
{

    /**
     * @api {get} /user/:id Fetch user data
     * @apiName GetUser
     * @apiGroup User
     *
     * @apiParam {Number} id User id to retrieve
     *
     * @apiSuccess {Number} id                        Reg Id.
     * @apiSuccess {String}    firstname                User firstname.
     * @apiSuccess {String}    lastname                User lastname.
     * @apiSuccess {String}    username                User username.
     * @apiSuccess {String}    email                User email.
     * @apiSuccess {String}    profile_pic                URI to profile pic stored.
     *
     * @apiUse UserOkResponse
     * @apiUse InternalError
     * @apiUse NotAcceptable
     * @apiUse Forbidden
     *
     * @apiDescription
     * Gets the user reg with providen id
     */
    private $_limit = 10;
    private $_page = '';

    private function _setPagination(Request $request)
    {
        !!$request->get('quantity') and ($this->_limit = $request->get('quantity'));
        !!$request->get('page') and ($this->_page = $request->get('page'));
    }

    public function __construct()
    {
        $this->_limit = config('app.pagination.quantity');
        $this->middleware('checkHiddenUser')->only(['store', 'show', 'followUser', 'getFeed', 'getFollowersById', 'getUserFollowingsById']);
    }

    public function _preparedUser($users, $addList = false)
    {
        foreach ($users as &$user):
            $user = User::preparedUser($user,$addList);
        endforeach;
    }

    public function _preparedUserLazy($users)
    {
        foreach ($users as &$user):
            $user = User::prepareLazy($user);
        endforeach;
    }

    public function _returnImageUser($users)
    {
        foreach ($users as &$user):
            $prepared_user = User::returnImageUser($user);
            $user = $prepared_user;
        endforeach;
    }

    public function _prepareListUser($lists_user)
    {
        foreach ($lists_user as &$list):
            $prepared_list = UserList::preparedListLazy($list);
            $list = $prepared_list;
        endforeach;
    }
    
    private function _prepareHashtag($hashtags)
    {
        foreach ($hashtags as &$hashtag):
            $hashtag = ListHashtag::preparedHashtagTop($hashtag);
        endforeach;
    }

    public function _preparedMyActivity($activities)
    {
        $token = JWTAuth::getToken();
        $user = JWTAuth::toUser($token);
        $timezone = null;
        if ($user->timezone):
            $timezone = $user->timezone;
        endif;
        foreach ($activities as &$activity):

            $prepared_activity = Activity::preparedActivity($activity);
            $activity = $prepared_activity;
            switch ($activity->action):
                case 'liked':
                    $text_like = " liked";
                    $activity_user = Activity::select(DB::raw('count(activities.target) as quantity_users'), DB::raw('max(activities.created_at) as time_last'))
                        ->where(["target" => $activity->target, 'action' => 'liked'])
                        ->orderBy('time_last', "DESC")
                        ->first();
                    $now = Carbon::now($timezone);
                    $date_last = Carbon::parse($activity_user->time_last);
                    $activity->time_activity = $date_last->diffForHumans($now);
                    if ($activity_user->quantity_users > 1):
                        $text_like = ' and ' . ($activity_user->quantity_users - 1) . $text_like;
                    endif;
                    $activity->users_liked = $activity->origin->username . $text_like;
                    break;
                case 'comment':
                    $activity_user = Activity::select(DB::raw('count(activities.target) as quantity_users'), DB::raw('max(activities.created_at) as time_last'))
                        ->where(["target" => $activity->target, 'action' => 'liked'])
                        ->orderBy('time_last', "DESC")
                        ->first();
                    $list = UserList::find($activity->target);
                    if($list):
                        $activity->list = UserList::preparedListLazy($list);
                    endif;
                    $now = Carbon::now($timezone);
                    $date_last = Carbon::parse($activity_user->time_last);
                    $activity->time_activity = $date_last->diffForHumans($now);
                    break;
            endswitch;
        endforeach;
    }

    public function _prepareFollowsActivities($activities)
    {
        foreach ($activities as &$activity):
            $activity = Activity::preparedFollowsActivities($activity);
        endforeach;
    }

    /**
     * Get method will use thi when an id is providen
     * @param numeric or string $term
     */
    public function show($term, Request $request)
    {

        $token = JWTAuth::getToken();
        $user = JWTAuth::toUser($token);
        $response = ['status' => 'fail', 'data' => ''];
        $code = 200;
        $this->_setPagination($request);
        if (is_numeric($term)):
            $addList = true;
            $user_search = User::where('id', '=', $term)->whereNotIn('id', HiddenUser::select('hidden_users.user_id')
                ->where('hidden_users.user_hidden_id', $user->id)->get())->where('banned', '=', 0)->first();

        elseif (is_string($term)):
            $addList = false; //to avoid lists in returned object
            $params = array();
            parse_str($request->id, $params);
            $user_search = User::select('users.*');
            if(is_array($params) and count($params) > 0){
                foreach ($params as $key => $param) {
                    $user_search->orWhere('firstname', 'like', "%{$param}%");
                    $user_search->orWhere('lastname', 'like', "%{$param}%");
                    $user_search->orWhere('username', 'like', "%{$param}%");
                }
                $user_search->where('users.id', '!=', $user->id)
                ->whereNotIn('users.id', HiddenUser::select('hidden_users.user_id')
                ->where('hidden_users.user_hidden_id', $user->id)->get())
                ->where('banned', '=', 0);
                $user_search = $user_search->simplepaginate($this->_limit);
                 if (count($user_search) == 1)  $user_search =  $user_search[0];
            } 
        endif;

        if ((empty($user_search) or sizeof($user_search) === 0)):
            $code = 200;
            $response['data'] = [];
        else:

            if (count($user_search) > 1):
                $this->_preparedUser($user_search,$addList);
            else:
                $this->_preparedUser([$user_search],$addList);
            endif;
            $response['status'] = 'ok';
            $response['data'] = $user_search;
        endif;

        return response()->json($response, $code);
    }

    /**
     * Get method will use thi when an id is providen
     * @param numeric or string $username
     */
    public function getByUsername($username, Request $request)
    {

        $token = JWTAuth::getToken();
        $user = JWTAuth::toUser($token);
        $response = ['status' => 'fail', 'data' => ''];
        $code = 200;
        
        $user_search = User::select('users.*')
        ->where('banned', '=', 0)
        ->where('username', '=', $username);
        $user_search = $user_search->first();
                 
        if ((empty($user_search) or sizeof($user_search) === 0)):
            $code = 200;
            $response['data'] = [];
        else:
            $this->_preparedUser([$user_search],false);
            $response['status'] = 'ok';
            $response['data'] = $user_search;
        endif;

        return response()->json($response, $code);
    }

    public function profileGet()
    {
        $response = ['status' => 'fail', 'data' => []];
        $code = 200;
        $token = JWTAuth::getToken();
        $user = JWTAuth::toUser($token);
        if (empty($user)):
            $code = 200;
        else:
            $response = ['status' => 'ok', 'data' => $user];
        endif;

        return response()->json($response, $code);
    }

    /**
     * @api {put} /user/:id Update user data
     * @apiName PutUser
     * @apiGroup User
     *
     * @apiParam {Number} id User id to retrieve
     * @apiParam {User} user User data to storage [firstname, lastname, email, birthdate, sex, username, password(not encrypted), profile_pic(optional string)]
     *
     * @apiSuccess {Number} id                        Reg Id.
     * @apiSuccess {String}    firstname                User firstname.
     * @apiSuccess {String}    lastname                User lastname.
     * @apiSuccess {String}    username                User username.
     * @apiSuccess {String}    email                User email.
     *
     * @apiUse UserOkResponse
     * @apiUse InternalError
     * @apiUse NotAcceptable
     * @apiUse Forbidden
     *
     * @apiDescription
     * Updates data of an user. This endpoint does not handle the profle pic upload.
     */
    public function profileUpdate(Request $request)
    {
        $response = ['status' => 'fail', 'data' => []];
        $code = 200;

        $path = '';
        $data = $request->all();

        $token = JWTAuth::getToken();
        $user = JWTAuth::toUser($token);

        if (!empty($user)):
            $data['username'] = '';
            $data['email'] = '';
            $data = $user->_cleanupData($data);
            if ($user->validateUpdate($data)):
                if (!empty($data['profile_pic'])):
                    //checks if is there a pic already
                    if (!empty($user->profile_pic)):
                        $path = str_replace(url('/'), '', $user->profile_pic);
                        file_exists(public_path() . $path) and unlink(public_path() . $path);
                    endif;
                    $comp = explode(';', $data['profile_pic']);
                    $ext = explode('/', $comp[0])[1];
                    $img = explode(',', $comp[1])[1];
                    $image = base64_decode($img);
                    $ext === 'jpeg' and ($ext = 'jpg');
                    $filename = '/profile_pics/' . md5(str_random(8)) . ".{$ext}";
                    file_put_contents(public_path() . '/' . $filename, $image);
                    $data['profile_pic'] = url('/') . $filename;
                endif;

                if (!empty($data['password'])):
                    $data['password'] = bcrypt($data['password']);
                endif;

                if (!empty($data['latitude']) and !empty($data['longitude'])):
                    $data = $user->setLocation($data);
                endif;

                $user->fill($data);

                if ($user->save()):

                    $response = ['status' => 'ok', 'data' => $user];
                    $code = 200;
                else:
                    $response['data'] = $user->errors();
                endif;
            else:
                $response['data'] = $user->errors();
            endif;
        else:
            $response = ['status' => 'fail', 'data' => []];
            $code = 200;
        endif;

        return response()->json($response, $code);
    }

    /**
     * @api {put} /user/update Update user data
     * @apiName Put
     * @apiGroup UserImageUsername
     *
     * @apiSuccess {Number} id                        Reg Id.
     * @apiSuccess {String}    firstname                User firstname.
     * @apiSuccess {String}    lastname                User lastname.
     * @apiSuccess {String}    username                User username.
     * @apiSuccess {String}    email                User email.
     *
     * @apiUse UserOkResponse
     * @apiUse InternalError
     * @apiUse NotAcceptable
     * @apiUse Forbidden
     *
     * @apiDescription
     * Updates data of an user. This endpoint does not handle the profle pic upload.
     */
    public function updateImageUsername(Request $request)
    {
        $response = ['status' => 'fail', 'data' => []];
        $code = 200;

        $path = '';
        $data = $request->all();

        $token = JWTAuth::getToken();
        $user = JWTAuth::toUser($token);

        if (!empty($user)):
            
            if (!empty($data['profile_pic'])):
                //checks if is there a pic already
                if (!empty($user->profile_pic)):
                    $path = str_replace(url('/'), '', $user->profile_pic);
                    file_exists(public_path() . $path) and unlink(public_path() . $path);
                endif;
                $comp = explode(';', $data['profile_pic']);
                $ext = explode('/', $comp[0])[1];
                $img = explode(',', $comp[1])[1];
                $image = base64_decode($img);
                $ext === 'jpeg' and ($ext = 'jpg');
                $filename = '/profile_pics/' . md5(str_random(8)) . ".{$ext}";
                file_put_contents(public_path() . '/' . $filename, $image);
                $data['profile_pic'] = url('/') . $filename;
            endif;
    

            if($user->validateUsername($data)){
                if (!empty($data['username'])):
                    $data['username'] = $data['username'];
                endif;
            }else{
                $response['data'] = $user->errors();
            }

            if(!$response['data']):
                $user->fill($data);
                if ($user->save()):
                    $response = ['status' => 'ok', 'data' => $user];
                    $code = 200;
                else:
                    $response['data'] = $user->errors();
                endif;
            endif;
        else:
            $response = ['status' => 'fail', 'data' => []];
            $code = 200;
        endif;

        return response()->json($response, $code);
    }

    public function followUser(Request $request)
    {
        $response = ['status' => 'ok', 'data' => []];
        $code = 201;

        $token = JWTAuth::getToken();
        $user = JWTAuth::toUser($token);
        $data = $request->all();

        $toFollow = User::select('users.id')
            ->where(['id' => $data['userId']])
            ->where(['banned' => 0])
            ->first();

        if ($toFollow):
            if ($toFollow->id == $user->id):
                //Throws error regarding the user is the same logged in, you cannot floowo yourself
                $response = ['status' => 'fail', 'data' => 'User to attempt to follow is the same loggedin'];
                return response()->json($response, $code);
            endif;
            $response['data'] = UserFollow::where(['user_id' => $user->id, 'follows_to' => $toFollow->id])->first();

            if (empty($response['data']->id)):
                $response['data'] = UserFollow::create(['user_id' => $user->id, 'follows_to' => $toFollow->id]);
                $activity = new Activity();
                $activity->track([
                    'origin' => $user->id,
                    'target' => $response['data']->id,
                    'affected' => $toFollow->id,
                    'action' => 'follow'
                ]);
                $following_user = User::find($toFollow->id);
                if ($following_user->token_android):
                    $firebaseNotification = new Utils();
                    $firebaseNotification->sendNotificationFirebase($following_user->token_android, 'User follow', $user, 'following');
                endif;
            endif;
        else:
            //Throws error regarding the user is trying to set a not exists user
            $response = ['status' => 'fail', 'data' => 'User to attempt to follow does not valid'];
            $code = 404;
        endif;

        return response()->json($response, $code);
    }

    public function unfollowUser(Request $request)
    {
        $response = ['status' => 'fail', 'data' => []];
        $code = 200;

        $token = JWTAuth::getToken();
        $user = JWTAuth::toUser($token);
        $data = $request->all();

        $toUnFollow = User::where(['id' => $data['userId']])->select('users.id')->first();

        if ($toUnFollow):
            if ($toUnFollow->id == $user->id):
                //Throws error regarding the user is the same logged in, you cannot unfollow yourself
                $response = ['status' => 'fail', 'data' => 'User to attempt to unfollow is the same loggedin'];
                return response()->json($response, $code);
            endif;
            if (UserFollow::where(['user_id' => $user->id, 'follows_to' => $toUnFollow->id])->delete()):
                $response['status'] = 'ok';
                //$activity = new Activity();
                /*  $activity->track([
                      'origin' => $user->id,
                      'target' => $toUnFollow->id,
                      'affected' => $toUnFollow->id,
                      'action' => 'unfollow'
                  ]);*/
            endif;
        else:
            //Throws error regarding the user is trying to set a not exists user
            $response = ['status' => 'fail', 'data' => 'User to attempt to unFollow does not exists'];
            $code = 200;
        endif;

        return response()->json($response, $code);
    }

    public function getFollows(Request $request)
    {

        $this->_setPagination($request);
        $response = ['status' => 'ok', 'data' => []];
        $code = 200;

        $token = JWTAuth::getToken();
        $user = JWTAuth::toUser($token);

        $random = '';
        !!$request->get('random') and ($random = $request->get('random'));
        $flag = $random === 'true' ? true : false;

        $users = User::getUserFollows($user->id, $flag, $request->term)->paginate($this->_limit);

        if (!$users->isEmpty()):
            foreach ($users as $follow):
                $follow->profile_pic = User::getImageProfile($follow);
            endforeach;
            $response['data'] = $users;
        else:
            $code = 200;
        endif;

        return response()->json($response, $code);
    }

    public function getFollowers(Request $request)
    {

        $this->_setPagination($request);
        $response = ['status' => 'ok', 'data' => []];
        $code = 200;

        $token = JWTAuth::getToken();
        $user = JWTAuth::toUser($token);

        $random = '';
        !!$request->get('random') and ($random = $request->get('random'));
        $flag = $random === 'true' ? true : false;

        $followers = User::getUserFollowers($user->id, $flag, $request->term)->paginate($this->_limit);

        if (!$followers->isEmpty()):
            foreach ($followers as $follower):
                $follower = UserFollow::setIsFollowing($follower, $user->id, $follower->id);
                $follower->profile_pic = User::getImageProfile($follower);
            endforeach;
            $response['data'] = $followers;
        else:
            $code = 200;
        endif;

        return response()->json($response, $code);
    }

    /**
     * @api {post} /signup User register
     * @apiName PostSignup
     * @apiGroup User
     *
     * @apiParam {json} user User data to storage {firstname, lastname, email, birthdate, sex, username, password(not encrypted), profile_pic(optional base64 string)}
     *
     * @apiSuccess {string} token       Token for further interactions.
     *
     * @apiSuccessExample Success-Response:
     *     HTTP/1.1 201 OK
     *     {
     *       "status": "ok",
     *       "data": [
     *        {
     *              "token": "sagsd1svsvsd.-msdfshfsfkasjdfhds"
     *          }
     *       ]
     *     }
     *
     * @apiUse InternalError
     * @apiUse NotAcceptable
     *
     * @apiDescription
     * Creates a new entry in the users table.
     */
    public function signup(Request $request)
    {
        $response = ['status' => 'fail', 'data' => []];
        $code = 400;
        $data = $request->all();
        $u = new User();
        if ($u->validate($data)):
            $data['password'] = bcrypt($data['password']);
            if (!empty($data['birthdate'])):
                $data['birthdate'] += 0;
            endif;
            if (!empty($data['profile_pic'])):
                $comp = explode(';', $data['profile_pic']);
                if (count($comp) > 1):
                    $ext = explode('/', $comp[0])[1];
                    $img = explode(',', $comp[1])[1];
                    $image = base64_decode($img);
                    $ext === 'jpeg' and ($ext = 'jpg');
                    $filename = 'profile_pics/' . md5(str_random(8)) . ".{$ext}";
                    $data['profile_pic'] = url('/') . '/' . $filename;
                    file_put_contents(public_path() . '/' . $filename, $image);
                else:
                    $response = ['status' => 'fail', 'data' => ["message" => "not base 64"]];
                endif;
            endif;

            if (!empty($data['latitude']) and !empty($data['longitude'])):
                $data = $u->setLocation($data);
            endif;
            $user = User::create($data);
            $token = JWTAuth::fromUser($user);
            if ($user):
                $response = ['status' => 'ok', 'data' => ["token" => $token, 'user_id' => $user->id, 'profile_pic' => $user->getImageProfile($user)]];
                $code = 201;
            endif;
        else:
            $response = ['status' => 'fail', 'data' => $u->errors()];
        endif;

        return response()->json($response, $code);
    }

    /**
     * @api {post} /signin User login
     * @apiName PostSignin
     * @apiGroup User
     *
     * @apiParam {json} credentials User data to login, email|username and password
     *
     * @apiSuccess {strin} token Session token.
     *
     * @apiSuccessExample Success-Response:
     *     HTTP/1.1 201 OK
     *     {
     *       "status": "ok",
     *       "data": [
     *        {
     *              "token": "q23iuriurb4virh4kqrhv.34thrjkh"
     *          }
     *       ]
     *     }
     *
     * @apiUse InternalError
     * @apiUse NotAcceptable
     *
     * @apiDescription
     * Creates a new entry in the users table. This endpoint does not handle the profle pic upload.
     */
    public function signin(Request $request)
    {

        $response = ['status' => 'fail', 'data' => ['success' => 'fail']];
        $code = 200;
        $data = $request->all();

        if ((empty($data['email']) or empty($data['username'])) and empty($data['password'])):
            return response()->json($response, $code);
        endif;

        $login = $data['email'];
        
        $user = User::where(['email' => "$login"])->first();
        if ($user):
            if ($user->banned == 0):
                if (empty($user)):
                    return response()->json($response, 200);
                endif;

                if (!Hash::check($data['password'], $user->password)):
                    return response()->json($response, 200);
                endif;
                if (isset($request->token_android) && $request->token_android != $user->token_android):
                    $user->token_android = $request->token_android;
                    $user->save();
                endif;
                $user->profile_pic = $user->getImageProfile($user);

                try {
                    $token = JWTAuth::fromUser($user);
                    $response['token'] = $token;

                    if (!$token):
                        return response()->json($response, 200);
                    endif;
                } catch (JWTException $e) {
                    return response()->json($response, 200);
                }
                $response = ['status' => 'ok', 'data' => ['token' => $token, 'success' => 'ok', 'profile_pic' => $user->profile_pic, 'user_id' => $user->id]];
                $code = 200;
            else:
                $response['status'] = 'fail';
                $response['data'] = ['message' => 'cannot login with this email'];
            endif;
        else:
            // JWTAuth::invalidate(JWTAuth::getToken());
            $response['status'] = 'fail';
            $response['data'] = '';
        endif;

        return response()->json($response, $code);
    }

    public function _prepareLists($lists)
    {
        foreach ($lists as &$list):
            $list = UserList::prepareList($list);
        endforeach;
    }

    public function fbLogin(Request $request)
    {
        $response = ['status' => 'fail', 'data' => []];
        $code = 400;
        $data = $request->all();

        $fb = new \Facebook\Facebook([
            'app_id' => config('app.facebook.app_id'),
            'app_secret' => config('app.facebook.app_secret'),
            'default_graph_version' => config('app.facebook.default_graph_version'),
            'enable_beta_mode' => config('app.facebook.enable_beta_mode'),
        ]);

        try {
            $fbres = $fb->get('/me?fields=id,first_name,last_name,birthday,email', $data['fbtoken']);
        } catch (\Facebook\Exceptions\FacebookResponseException $e) {
            $response['data'] = $e->getMessage();
            return response()->json($response, $code);
        } catch (\Facebook\Exceptions\FacebookSDKException $e) {
            $response['data'] = $e->getMessage();
            return response()->json($response, $code);
        }

        $graphNode = $fbres->getGraphNode();
        $user = User::where(['email' => $graphNode->getField('email')])->first();
        if (empty($user->id) || (!empty($user->id) && $user->banned == 0)):
            $response['is_login'] = true;
            if (empty($user->id)):
                $user = new User();
                $user->firstname = $graphNode->getField('first_name');
                $user->lastname = $graphNode->getField('last_name');
                $user->username = $user->email = $graphNode->getField('email');
                if (!empty($graphNode->getField('birthday'))):
                    $user->birthdate = $graphNode->getField('birthday')->format('U');
                endif;
                $user->timezone = isset($data['timezone']) ? $data['timezone'] : '';
                $user->token_facebook = $data['fbtoken'];
                $user->fb_id = $graphNode->getField('id');

                $user->save();
                $response['is_login'] = false;
            endif;
            if (isset($request->token_android) && $request->token_android != $user->token_android):
                $user->token_android = $request->token_android;
            endif;
            $user->profile_pic = $user->getImageProfile($user);
            try {
                $user->token_facebook = $user->extendsTokenFacebook($user->token_facebook);
                $user->save();
                $token = JWTAuth::fromUser($user);
                $response['token'] = $token;
                if (!$token):
                    $response['data'] = 'user_not_authorized';
                    return response()->json($response, 200);
                endif;
            } catch (JWTException $e) {
                return response()->json($response, 200);
            }

            if($response['is_login'] == true){
                $response = ['status' => 'ok', 'data' => $user, 'token' => $token, 'is_login' => true];
            }else{
                $response = ['status' => 'ok', 'data' => $user, 'token' => $token, 'is_login' => false];
            }
            
            $code = 201;

            return response()->json($response, $code);
        else:
            $response['status'] = 'fail';
            $response['data'] = ['message' => 'cannot login with this email'];
            return response()->json($response, 200);
        endif;
    }

    public function getFbFriends(Request $request, $fbToken)
    {
        $this->_setPagination($request);
        $response = ['status' => 'fail', 'data' => [], 'page' => $this->_page, 'quantity' => $this->_limit];
        $code = 400;

        $fb = new \Facebook\Facebook([
            'app_id' => config('app.facebook.app_id'),
            'app_secret' => config('app.facebook.app_secret'),
            'default_graph_version' => config('app.facebook.default_graph_version'),
            'enable_beta_mode' => config('app.facebook.enable_beta_mode'),
        ]);
        try {
            $req = $fb->get("/me/friends?fields=id,name&limit={$this->_limit}&offset={$this->_page}", $fbToken);
        } catch (\Facebook\Exceptions\FacebookResponseException $e) {
            $response['data'] = $e->getMessage();
            return response()->json($response, $code);
        } catch (\Facebook\Exceptions\FacebookSDKException $e) {
            $response['data'] = $e->getMessage();
            return response()->json($response, $code);
        }

        $graph = $req->getGraphEdge();
        foreach ($graph as $status):
            $data = $status->asArray();
            $data['isRegistered'] = !empty(User::select('id')->where(['fb_id' => $data['id']])->get()->toArray());
            $response['data'][] = $data;
        endforeach;

        $response['status'] = 'ok';
        $code = 200;

        return response()->json($response, $code);
    }

    public function twitterLogin(Request $request)
    {
        $response = ['status' => 'fail', 'data' => []];
        $code = 400;
        $data = $request->all();

        $connection = new TwitterOAuth(config('app.twitter.consumer_key'), config('app.twitter.consumer_secret'), $data['token'], $data['secret']);

        $content = $connection->get("account/verify_credentials", ['include_email' => 'true', 'skip_status' => true, 'include_entities' => false]);

        if ($connection->getLastHttpCode() != 200):
            $response['data'] = 'user_not_authorized';
            return response()->json($response, 200);
        endif;

        $user = User::where('email', 'like', $content->email)->first();

        if (empty($user)):
            $utils = new Utils();
            $extract_name = $utils->extractName($content->name);
            $user = new User();
            $user->firstname = $extract_name[0];
            $user->lastname = $extract_name[count($extract_name) - 1];
            $user->username = $user->email = $content->email;
            $user->token_twitter = $data['token'];
            $user->secrect_twitter = $data['secret'];
            $user->birthdate = 0;
            $user->twitter_id = $content->id;
            $user->timezone = isset($data['timezone']) ? $data['timezone'] : '';
            $user->save();
        else:
            if ($user->twitter_id == null) {
                $user = User::find($user->id);
                $user->twitter_id = $content->id;
                $user->token_twitter = $data['token'];
                $user->secrect_twitter = $data['secret'];
                $user->save();
            }
        endif;
        if (isset($request->token_android) && $request->token_android != $user->token_android):
            $user->token_android = $request->token_android;
        endif;
        $user->profile_pic = $user->getImageProfile($user);

        try {
            $token = JWTAuth::fromUser($user);
            $response['token'] = $token;
            if (!$token):
                return response()->json($response, 200);
            endif;
        } catch (JWTException $e) {
            return response()->json($response, 200);
        }

        $response = ['status' => 'ok', 'data' => $user, 'token' => compact('token')];
        $code = 201;

        return response()->json($response, $code);
    }

    public function twFollowers(Request $request, $token, $secret)
    {

        $this->_setPagination($request);
        $response = ['status' => 'fail', 'data' => []];
        $code = 400;
        $data = $request->all();
        $connection = new TwitterOAuth(config('app.twitter.consumer_key'), config('app.twitter.consumer_secret'), $token, $secret);
        $content = $connection->get("account/verify_credentials");

        if ($connection->getLastHttpCode() != 200):
            $response['data'] = 'user_not_authorized';
            return response()->json($response, 200);
        endif;

        $ids = $connection->get("friends/ids", ['user_id' => $content->id, 'count' => $this->_limit, 'cursor' => $this->_page]);

        foreach ($ids->ids as $id):
            $response['data'][] = ['id' => $id, 'isRegistered' => !empty(User::where(['twitter_id' => $data['id']])->get()->id)];
        endforeach;

        $response['nextPageCursor'] = $ids->next_cursor;
        $response['prevPageCursor'] = $ids->previous_cursor;

        $response['status'] = 'ok';
        $code = 200;

        return response()->json($response, $code);
    }
    public function googleLogin(Request $request)
    {
        echo $request['password'];
        $response = ['status' => 'fail', 'data' => []];
        $code = 400;
        $data = $request->all();
        $client = new \Google_Client(['client_id' => config('461549643609-rbetgd7fb74du4cf25fupgnlfl3tlkn6.apps.googleusercontent.com')]);
        $client->setClientSecret(config('J_A2yEtb2Jp8WFXPlFt9jf30'));

        //$client = new \Google_Client(['client_id' => config('977545800690-cc9rtnjghjtv0mckeukueusf70ii1vp4.apps.googleusercontent.com')]);
        //$client->setAuthConfig(config('app.google.auth_file'));
        //$client->setClientId(config('977545800690-cc9rtnjghjtv0mckeukueusf70ii1vp4.apps.googleusercontent.com'));
        //$client->setClientSecret(config('yZevnpD4GS2WcfJ_2gljosxp'));
        /*$client->setScopes(array(
            'https://www.googleapis.com/auth/plus.login', 
            'https://www.googleapis.com/auth/plus.me', 
            'https://www.googleapis.com/auth/userinfo.email',
             'https://www.googleapis.com/auth/userinfo.profile'));  
        $user_token = $client->verifyIdToken($data['gCode']);
        */
        /*if (empty($user_token)):
            $response['data'] = 'user_not_authorized';
            return response()->json($response, 200);
        endif;*/
        
        $user = User::where(['email' => $data['email']])->where(['google_id' => $data['google_id']])->first();
        if (empty($user->id) || (!empty($user->id) && $user->banned == 0)):
            $response['is_login'] = true;
            if (!$user):
                $user = new User();
                $user->firstname = $data['name'];
                //$user->lastname = $user_token['family_name'];
                $user->username = $user->email = $data['email'];
                $user->birthdate = 0;
                $user->google_id = $data['google_id'];
                $user->timezone = isset($data['timezone']) ? $data['timezone'] : '';
                $user->save();
                $response['is_login'] = false;
            else:
                if ($user->google_id == null):
                    $user = User::find($user->id);
                    $user->google_id = $user_token['sub'];
                    $user->save();
                endif;
            endif;

            $user->profile_pic = $data['photo'];
            //$imagen = file_get_contents($data['photo']);

            //$im = imagecreatefromjpeg($data['photo']);
            //$nameImage = "image/".$idList.".jpg";
            //$urlImage= 'http://top5dapp.com/public/profile_pics/google'.$data['google_id'].'.jpg';
            //file_put_contents($urlImage, $imagen);
            //$user->profile_pic = $urlImage;


            //$user->profile_pic = $user->getImageProfile($user);
            if (isset($request->token_android) && $request->token_android != $user->token_android):
                $user->token_android = $request->token_android;
            endif;
            try {
                $token = JWTAuth::fromUser($user);
                $response['token'] = $token;
                if (!$token):
                    return response()->json($response, 200);
                endif;
            } catch (JWTException $e) {
                return response()->json($response, 500);
            }
            if($response['is_login'] == true){
                $response = ['status' => 'ok', 'data' => $user, 'is_login' => true, 'token' => compact('token')];
            }else{
                $response = ['status' => 'ok', 'data' => $user, 'is_login' => false, 'token' => compact('token')];
            }
            $code = 201;

            return response()->json($response, $code);
        else:
            $response['status'] = 'fail';
            $response['data'] = ['message' => 'cannot login with this email'];
            return response()->json($response, 200);
        endif;
    }


    public function getGoogleFriends(Request $request)
    {
        $response = ['status' => 'fail', 'data' => []];
        $code = 400;
        //$data = $request->all();
        $client = new \Google_Client(['client_id' => config('461549643609-rbetgd7fb74du4cf25fupgnlfl3tlkn6.apps.googleusercontent.com')]);

       // $client = new \Google_Client(['client_id' => config('977545800690-cc9rtnjghjtv0mckeukueusf70ii1vp4.apps.googleusercontent.com')]);
        //$client->setAuthConfig(config('app.google.auth_file'));
        //$client->setClientId(config('977545800690-cc9rtnjghjtv0mckeukueusf70ii1vp4.apps.googleusercontent.com'));
        $client->setClientSecret(config('J_A2yEtb2Jp8WFXPlFt9jf30'));

        //$client->useApplicationDefaultCredentials();
        $client->setScopes(array(
            'https://www.google.com/m8/feeds/contacts/default/full'));
        $client->setAccessType("offline");
        // $client->setIncludeGrantedScopes(true);
        //$client->fetchAccessTokenWithAuthCode($request->googlecode);
        //$user_token = $client->verifyIdToken($data['gCode']);
        //$clien = $client->getAccessToken();


        $curl = curl_init('https://www.google.com/m8/feeds/contacts/default/full?alt=json&max-results=5000&access_token='.$request->googlecode);
        curl_setopt($curl,CURLOPT_RETURNTRANSFER,true);
        curl_setopt($curl,CURLOPT_SSL_VERIFYPEER,false);
        curl_setopt($curl,CURLOPT_TIMEOUT,100);
        $contact_json = curl_exec($curl);
        curl_close($curl);
        //$xml = simplexml_load_string($contact_json);
        //$json = json_encode($xml);
        //$array = json_decode($json,true);
        //$contact_json = simplexml_load_string($contact_xml);
        //return $contact_json;
        $objectcontacts = json_decode($contact_json);
        if (empty($objectcontacts)):
            $response['data'] = 'please refresh google token';
            return response()->json($response, 401);
        endif;


        $contacts = $objectcontacts->feed->entry;
        $users = array();
        
        foreach($contacts as $contact){
            $user = array();
            foreach($contact as $key => $ct){
                switch ($key) {
                    case 'id':
                        foreach($ct as $urlid){
                            $id = explode("base/", $urlid);
                            $user['id'] = $id[1];
                        }
                        break;
                    case 'title':
                        foreach($ct as $k =>$name){
                            switch ($k) {
                                case '$t':
                                    $user['name'] = $name;
                                break;
                            }
                        }
                        break;
                    case 'gd$email':
                        $user['mail'] = $ct[0]->address;
                        break;
                }
            }
            if(isset($user['mail'])){
                $isregisted = User::where(['email' => $user['mail']])->first();
            }
            
            if($isregisted){
                $user['isRegistered'] = true;
                array_push($users, (object) $user);
            }else{
                $user['isRegistered'] = false;
            }

        }
        if(isset($users)) {
            $code = 200;
        }
        

        if (empty($users)):
            $response['data'] = 'user_not_authorized';
            return response()->json($response, 200);
        endif;

        /*$people = new \Google_Service_People($client);

        $params = ['resourceName' => 'people/me', 'request.includeField' => 'person.names,person.email_addresses', 'pageSize' => $this->_limit];
        empty($this->_page) or ($params['pageToken'] = $this->_page);

        $friends = $people->people->connections->listConnections($params);

        foreach ($friends['connections'] as $contact):
            $contact['isRegistered'] = !empty(User::where(['email' => $contact['emailAddresses'][0]['value']])->get()->id);

            $response['data'][] = $contact;
        endforeach;
        $response['nextPage'] = $friends['nextPageToken'];*/

        return response()->json($users, $code);
    }

    public function getNotifications(Request $request)
    {
        $this->_setPagination($request);
        $response = ['status' => 'ok', 'data' => [], 'quantity' => $this->_limit, 'page' => $this->_page];
        $code = 200;

        $token = JWTAuth::getToken();
        $user = JWTAuth::toUser($token);

        $notifications = Activity::where(['affected' => $user->id])->whereNotIn('origin', HiddenUser::select('user_id')
            ->where('user_hidden_id', $user->id)->get()
        )->paginate($this->_limit);

        if ($notifications->isEmpty()):
            $code = 200;
        else:
            $this->_preparedMyActivity($notifications);
            $response['data'] = $notifications;
        endif;

        return response()->json($response, $code);
    }

    public function getFeed(Request $request, $uid)
    {
        $token = JWTAuth::getToken();
        $user = JWTAuth::toUser($token);
        $this->_setPagination($request);
        $response = ['status' => 'ok', 'data' => []];
        $code = 200;
        $followingList = [];

        $followingList [] = $uid;

        $followings = UserFollow::where(['user_id' => $uid])->get();

        if (!empty($followings)):
            foreach ($followings as $follow):
                $followingList [] = $follow->follows_to;
            endforeach;
        endif;

        $activity = Activity::whereIn('origin', $followingList)
            ->whereNotIn('origin', HiddenUser::select('user_id')
                ->where('user_hidden_id', $user->id)->get()
            )->whereNotIn('target', HiddenUser::select('user_id')
                ->where('user_hidden_id', $user->id)->get()
            )->orderBy('created_at', 'desc')->paginate($this->_limit);

        if ($activity->isEmpty()):
            $code = 200;
        else:
            $this->_preparedMyActivity($activity);
            $response['data'] = $activity;
        endif;

        return response()->json($response, $code);
    }

    public function getMyActivities(Request $request)
    {
        $this->_setPagination($request);
        $response = ['status' => 'ok', 'data' => []];
        $code = 200;

        $token = JWTAuth::getToken();
        $user = JWTAuth::toUser($token);

        $activities = Activity::where(function ($where) use ($user) {
            $where->where('origin', '!=', $user->id);
            $where->where('affected', '=', $user->id);
        })->where(function ($where) use ($user) {
            $where->whereNotIn('affected', HiddenUser::select('user_id')
                ->where('user_hidden_id', '=', $user->id)->get());
            $where->whereNotIn('origin', HiddenUser::select('user_id')
                ->where('user_hidden_id', '=', $user->id)->get());
        })
            ->orderBy('created_at', 'DESC')
            ->paginate($this->_limit);

        if ($activities->isEmpty()):
            $code = 200;
        else:
            $this->_preparedMyActivity($activities);
            $response['data'] = $activities;
        endif;

        return response()->json($response, $code);
    }

    public function getFollowsActivities(Request $request)
    {

        $this->_setPagination($request);
        $response = ['status' => 'fail', 'data' => []];
        $code = 200;

        $token = JWTAuth::getToken();
        $user = JWTAuth::toUser($token);

        $followingList = UserFollow::getFollowings($user);

        $activities = Activity::select('users.id', 'users.username','activities.affected','activities.origin', 'activities.action', DB::raw('count(*)as total, max(activities.created_at) as last_date'))
            ->join('users', 'users.id', '=', 'activities.origin')
            ->whereIn('activities.origin', $followingList)
            ->whereNotIn('activities.affected', HiddenUser::select('user_id')
                ->where('user_hidden_id', $user->id)->get())
            ->whereNotIn('activities.origin', HiddenUser::select('user_id')
                ->where('user_hidden_id', $user->id)->get())
            ->where('users.banned', 0)
            ->groupBy('users.id', 'activities.action')
            ->orderBy('last_date', 'DESC')
            ->paginate($this->_limit);
        /* $activities = Activity::whereIn('origin', $followingList)
          ->orWhereIn('affected', $followingList)
          ->orderBy('created_at', 'DESC')
          ->paginate($this->_limit); */

        if ($activities->isEmpty()):
            $code = 200;
        else:
            $response['status'] = 'ok';
            $this->_prepareFollowsActivities($activities);
            $response['data'] = $activities;
        endif;

        return response()->json($response, $code);
    }

    public function hiddenUser(Request $request)
    {
        $response = ['status' => 'fail', 'data' => []];
        $code = 200;
        $token = JWTAuth::getToken();
        $user = JWTAuth::toUser($token);
        if ($user->id != $request->user_id):

            $hidden_user = HiddenUser::where(['user_id' => $user->id, 'user_hidden_id' => $request->user_id])->first();
            if (!$hidden_user):
                $user_disable = User::find($request->user_id);
                event(new ChallengeCancelUser($user_disable, false));
                $user_hidden = new HiddenUser();
                $user_hidden->user_hidden_id = $request->user_id;
                $user_hidden->user_id = $user->id;
                $user_hidden->social = $request->social;
                if ($user_hidden->save()):
                    $code = 200;
                    $response['status'] = 'ok';
                    $response['data'] = ['message' => 'user hidden'];
                endif;
            else:
                $response['status'] = 'ok';
                $response['data'] = ['message' => 'this user is hidden'];
            endif;
        endif;
        return response()->json($response, $code);
    }

    public function removeUsers(Request $request)
    {
        $token = JWTAuth::getToken();
        $user = JWTAuth::toUser($token);
        $response = ['status' => 'fail', 'data' => []];

        $hidden_user = new HiddenUser();
        if ($request->option == "all"):
            $code = 200;
            $where = ['user_id' => $user->id];
            $message = 'Unhidden users';
        else:
            $code = 200;
            $message = 'Required field user_id';
            if (!empty($request->user_id)):
                $user_enable = User::find($request->user_id);
                event(new ChallengeCancelUser($user_enable, true));
                $where = ['user_id' => $user->id, 'user_hidden_id' => $request->user_id];
                $message = 'Unhidden user';
            else:
                $response['data'] = ['message' => $message];
                return response()->json($response, $code);
            endif;
        endif;
        if ($hidden_user->where($where)->delete()):
            $response['status'] = 'ok';
            $response['data'] = ['message' => $message];

        endif;

        return response()->json($response, $code);
    }

    /*
      public function discoverUsers()
      {
      $token = JWTAuth::getToken();
      $user = JWTAuth::toUser($token);
      $social_twitter = new UserSocials();

      if ($user->twitter_id):
      $users = $social_twitter->getFriendUserTwitter($user->token_twitter, $user->secrect_twitter);
      endif;
      if ($user->fb_id):
      $fb = new \Facebook\Facebook([
      'app_id' => config('app.facebook.app_id'),
      'app_secret' => config('app.facebook.app_secret'),
      'default_graph_version' => config('app.facebook.default_graph_version'),
      'enable_beta_mode' => config('app.facebook.enable_beta_mode'),
      ]);
      $req = $fb->get("/me/friends?name,id", $user->token_facebook);
      $users = $req->getDecodedBody();
      dd($users);
      endif;
      if ($user->google_id):
      $client = new \Google_Client();
      $client->setAuthConfig(config('app.google.auth_file'));
      $client->setScopes(array('https://www.googleapis.com/auth/contacts', 'https://www.googleapis.com/auth/contacts.readonly'));
      $client->fetchAccessTokenWithAuthCode("4/Ewr0Hn5ntPyq0rwvj9fvLc-rs_J9tIzc8tL1ZlJuI5g");
      $plus = new \Google_Service_Plus($client);
      $user_plus = $plus->people->get('me');
      dd($user_plus);
      $token = $client->fetchAccessTokenWithAuthCode("4/Ewr0Hn5ntPyq0rwvj9fvLc-rs_J9tIzc8tL1ZlJuI5g");
      $people = new \Google_Service_People($client);

      $params = ['resourceName' => 'people/me', 'request.includeField' => 'person.names,person.email_addresses', 'pageSize' => $this->_limit];
      empty($this->_page) or ($params['pageToken'] = $this->_page);

      $friends = $people->people_connections->listPeopleConnections('people/me');
      endif;
      } */

    public function getFriendsDiscover(Request $request)
    {
        $this->_setPagination($request);
        $ids = explode(",", $request->ids);
        $ids_user = [];

        $token = JWTAuth::getToken();
        $profile = JWTAuth::toUser($token);
        $code = 200;
        $response = ['status' => 'fail', 'data' => []];
        if (isset($request->social)):
            if($request->social == "google"):
                $mails = $ids;
                $users = User::select("id", "firstname", "lastname", "fb_id", "twitter_id", "google_id", "profile_pic")
                    ->whereIn("email", $mails)
                    ->whereNotIn('users.id', UserFollow::select('user_follows.follows_to as follows_user')
                        ->where('user_follows.user_id', '=', $profile->id)
                        ->get()
                    )
                    ->paginate($this->_limit);
            else:
                foreach ($ids as $id):
                    $user_hidden = HiddenUser::where(["user_hidden_id" => $id, "user_id" => $profile->id, "social" => $request->social])->first();
                    if (!$user_hidden):
                        switch ($request->social):
                            case 'facebook':
                                $user_social = User::where(['fb_id' => $id])->first();
                                break;
                            case 'twitter';
                                $user_social = User::where(['twitter_id' => $id])->first();
                                break;
                            case 'google':
                                $user_social = User::where(['google_id' => $id])->first();
                                break;
                        endswitch;
    
                        if (isset($user_social)):
                            $ids_user[] = $user_social->id;
                        endif;
                    endif;
                endforeach;

                $users = User::select("id", "firstname", "lastname", "fb_id", "twitter_id", "google_id", "profile_pic")
                    ->whereIn("id", $ids_user)
                    ->whereNotIn('users.id', UserFollow::select('user_follows.follows_to as follows_user')
                        ->where('user_follows.user_id', '=', $profile->id)
                        ->get()
                    )
                    ->paginate($this->_limit);
            endif;
        else:
            $users = User::select(DB::raw('users.*'),
                DB::raw('(' .
                    'select SUM(lp.quantity) FROM list_popularities lp ' .
                    'INNER JOIN lists on lists.id = lp.list_id ' .
                    'INNER JOIN users us on lists.user_id = us.id ' .
                    'WHERE us.id = users.id and us.banned = 0' .
                    ') as lists_popular'),
                DB::raw('(' .
                    'select count(*) FROM lists li ' .
                    'INNER JOIN users us on li.user_id = us.id ' .
                    'WHERE li.user_id= users.id and li.enable=1' .
                    ') as total_list'),
                DB::raw('(' .
                    'select count(*) FROM user_follows uf ' .
                    'INNER JOIN users us on uf.user_id = us.id ' .
                    'WHERE uf.follows_to = users.id and us.banned = 0' .
                    ') as total_follow'))
                ->from(DB::raw('users users'))
                ->where('users.banned', 0)
                ->whereNotIn('users.id', HiddenUser::select('hidden_users.user_id as users_hiddens')
                    ->where('hidden_users.user_hidden_id', '=', $profile->id)
                    ->get()
                )
                ->whereNotIn('users.id', UserFollow::select('user_follows.follows_to as follows_user')
                    ->where('user_follows.user_id', '=', $profile->id)
                    ->get()
                )
                ->orderByDesc('lists_popular', 'total_list', 'total_follow')
                ->where('users.id', '!=', $profile->id)
                //->inRandomOrder()
                ->paginate($this->_limit);

        endif;

        if (!empty($users)):
            $this->_returnImageUser($users);
            $code = 200;
            $response["status"] = "ok";
            $response["data"] = $users;
        endif;
        return response()->json($response, $code);
    }

    public function getHashTags(Request $request)
    {
        $params = array();
        parse_str($request->name, $params);
        $token = JWTAuth::getToken();
        $user = JWTAuth::toUser($token);
        $quantity = isset($request->quantity) ? $request->quantity : $this->_limit;
        $code = 200;
        $response = ['status' => 'fail', 'data' => []];
        $queryHash = ListHashtag::select(DB::raw('list_hashtags.hashtag_id as id'),DB::raw('hashtags.keyword as keyword'), DB::raw('if(hashtags.id>0,3,5) as target'), DB::raw('count(list_hashtags.hashtag_id) as trending'))
            ->join('hashtags', 'hashtags.id', '=', 'list_hashtags.hashtag_id');
            if(is_array($params) and count($params) > 0):
                foreach ($params as $key => $name) {
                    $queryHash->orWhere('hashtags.keyword', 'like', $name . '%');
                }
            endif;
        $queryHash->groupBy('list_hashtags.hashtag_id');
        
        $queryThemes = UserList::select('lists.id as id', 'lists.name as keyword', DB::raw('if(lists.id>0,5,3) as target'), DB::raw('COUNT( lists.name ) AS trending'));
            if(is_array($params) and count($params) > 0):
                foreach ($params as $key => $name) {
                    $queryThemes->orWhere('lists.name', 'like', $name . '%');
                }
            endif;
        $queryThemes->groupBy('lists.name');
        $queryThemes->union($queryHash);
        $hashtags = $queryThemes->get();
        $this->_prepareHashtag($hashtags);
        if ($hashtags):
            $response['status'] = 'ok';
            $response['data'] = $hashtags;
            $code = 200;
        endif;
        return response()->json($response, $code);
    }

    public function getFollowersById($id, Request $request)
    {

        $this->_setPagination($request);
        $response = ['status' => 'ok', 'data' => [], 'quantity' => $this->_limit, 'page' => $this->_page];
        $code = 200;

        $token = JWTAuth::getToken();
        $user = JWTAuth::toUser($token);

        $random = '';
        !!$request->get('random') and ($random = $request->get('random'));
        $flag = $random === 'true' ? true : false;

        $users = User::getUserFollowers($id, $flag, $request->term)->paginate($this->_limit);

        if (!$users->isEmpty()):
            foreach ($users as $follower):
                $follower = UserFollow::setIsFollowing($follower, $user->id, $follower->id);
                $follower->profile_pic = User::getImageProfile($follower);
            endforeach;

            $response['data'] = $users;
        else:
            $code = 200;
        endif;

        return response()->json($response, $code);
    }

    public function getUserFollowingsById($id, Request $request)
    {

        $this->_setPagination($request);
        $response = ['status' => 'ok', 'data' => []];
        $code = 200;

        $token = JWTAuth::getToken();
        $user = JWTAuth::toUser($token);

        $random = '';
        !!$request->get('random') and ($random = $request->get('random'));
        $flag = $random === 'true' ? true : false;

        $users = User::getUserFollows($id, $flag, $request->term)->paginate($this->_limit);

        if (!$users->isEmpty()):
            foreach ($users as $follow):
                $follow = UserFollow::setIsFollowing($follow, $user->id, $follow->id);
                $follow->profile_pic = User::getImageProfile($follow);
            endforeach;
            $response['data'] = $users;
        else:
            $code = 200;
        endif;

        return response()->json($response, $code);
    }

    //Admin
    public function getUsersAdmin(Request $request)
    {
        $response = ['status' => 'ok', 'data' => []];
        $code = 200;
        $this->_setPagination($request);
        $users = User::getFilterUser($request)->paginate($this->_limit);
        if ($users):
            $this->_preparedUser($users);
            $response['status'] = 'ok';
            $response['data'] = $users;
        endif;
        return response()->json($response, $code);
    }

    //Admin
    public function getUserByIdAdmin($id)
    {
        $response = ['status' => 'fail', 'data' => []];
        $code = 200;

        $user = User::find($id);
        if ($user):
            $this->_preparedUser([$user]);
            $response['status'] = 'ok';
            $response['data'] = $user;
        endif;

        return response()->json($response, $code);
    }

    //Admin
    public function setBannedUser(Request $request, $idUser)
    {
        $response = ['status' => 'ok', 'data' => []];
        $code = 200;
        $token = JWTAuth::getToken();
        $user = JWTAuth::toUser($token);
        if ($user->role == 2):
            $data = $request->all();
            $userFind = User::find($idUser);
            if ($userFind):
                if ($userFind->validateStatus($data)):
                    $status = $data['status'];
                    if ($status == 0 || $status == 1):
                        if ($userFind->banned != $status):
                            $userFind->banned = $status;
                            if ($status):
                                $enable = false;
                            else:
                                $enable = true;
                            endif;
                            event(new EventActivitiesUser($userFind, 'all', $enable));
                            event(new ChallengeCancelUser($userFind, $enable));
                            $userFind->save();
                        endif;
                        $userFind = User::preparedUser($userFind);
                        $response['data'] = $userFind;
                    else:
                        $response['data'] = 'The status you attempt to set, is not valid';
                        $code = 404;
                        return response()->json($response, $code);
                    endif;
                else:
                    $response['data'] = $userFind->errors();
                endif;
            else:
                $response['data'] = 'The user you attempt disable/enable, is not valid';
                $code = 200;
            endif;
        else:
            $response['data'] = 'The user not admin';
        endif;

        return response()->json($response, $code);
    }

    public function logout()
    {

        $response = ['status' => 'fail', 'data' => []];
        $code = 200;

        $token = JWTAuth::getToken();
        $user = JWTAuth::toUser($token);
        $user->token_android = '';
        $save = $user->save();

        if ($save):
            $code = 200;
            $response['status'] = 'ok';
            $response['data'] = ['message' => 'logout'];
            JWTAuth::invalidate(JWTAuth::getToken());
        endif;

        return response()->json($response, $code);
    }

    public function setAccount(Request $request)
    {

        $response = ['status' => 'ok', 'data' => []];
        $code = 200;

        $token = JWTAuth::getToken();
        $user = JWTAuth::toUser($token);
        $data = $request->all();

        if ($user->validateStatus($data)):
            $privacy = $data['status'];
            if ($privacy == 0 || $privacy == 1):
                if ($user->private != $privacy):
                    $user->private = $privacy;
                    $user->save();
                endif;
                $user = User::prepareLazyReferences($user);
                $response['data'] = $user;
            else:
                $response['data'] = 'The status you attempt to set, is not valid';
                $code = 400;
            endif;
        else:
            $response['data'] = $user->errors();
        endif;

        return response()->json($response, $code);
    }

    public function getHiddenUsers(Request $request)
    {

        $response = ['status' => 'fail', 'data' => []];
        $code = 200;

        $token = JWTAuth::getToken();
        $user = JWTAuth::toUser($token);

        $this->_setPagination($request);

        $usersHidden = User::select('users.*')
            ->join('hidden_users', 'hidden_users.user_hidden_id', '=', 'users.id')
            ->where(['hidden_users.user_id' => $user->id])
            ->groupBy('users.id')
            ->paginate($this->_limit);

        if ($usersHidden->isEmpty()):
            $code = 200;
        else:
            $response['status'] = 'ok';
            $this->_preparedUserLazy($usersHidden);
            $response['data'] = $usersHidden;
        endif;

        return response()->json($response, $code);
    }

    public function getReportsUser(Request $request)
    {

        $response = ['status' => 'fail', 'data' => []];
        $users = Report::select('user_affect.*', DB::raw('count(reports.id) as total'))
            ->join(DB::raw('users as user_origin'), 'user_origin.id', '=', 'reports.origin_id')
            ->join(DB::raw('users as user_affect'), 'user_affect.id', '=', 'reports.affect_id')
            ->where('target', 2);
        $order = $request->order == 0 ? 0 : 1;
        if ($request->username):
            $users->where(function ($users) use ($request) {
                $users->where('user_affect.username', 'like', '' . $request->username . '%');
            });
        endif;
        if ($request->firstname):
            $users->where(function ($users) use ($request) {
                $users->where('user_affect.firstname', 'like', '' . $request->firstname . '%');
            });
        endif;
        if ($request->lastname):
            $users->where(function ($users) use ($request) {
                $users->where('user_affect.lastname', 'like', '' . $request->lastname . '%');
            });
        endif;
        if ($request->email):
            $users->where(function ($users) use ($request) {
                $users->where('user_affect.email', 'like', '' . $request->email . '%');
            });
        endif;

        $users->where('user_affect.banned', $order);
        $users = $users->groupBy('user_affect.id')->orderBy('reports.created_at')->paginate($this->_limit);
        if ($users):
            $response['status'] = 'ok';
            $response['data'] = $users;
        endif;

        $code = 200;
        return response()->json($response, $code);
    }

    public function getAwards($id)
    {

        $response = ['status' => 'fail', 'data' => []];
        $code = 200;

        $token = JWTAuth::getToken();
        $user = JWTAuth::toUser($token);
        $user_won = null;
        $user_number = null;
        $user_receive = null;
        $user_award = null;
        $not_awards = Award::where('location','neighborhood')->get();
        $challenges_won = Challenge::select(DB::raw('count(challenges.id) as total'))
            ->where('winner_id', $id)->groupBy('winner_id')->first();
        if ($challenges_won):
            $user_won = $challenges_won->total;
        endif;
        $number_challenge = Challenge::select(DB::raw('count(challenges.id) as number_challenge'))
            ->where(function ($where) use ($id) {
                $where->where('owner_id', $id);
                $where->orWhere('target_id', $id);
            })->first();
        if ($number_challenge && $number_challenge->number_challenge > 0):
            $user_number = $number_challenge->number_challenge;
        endif;

        $award_receive = UserAward::select(DB::raw('count(user_awards.id) as awards_receive'))
            ->where('user_id', $id)
            ->whereNotIn('award_id',$not_awards->pluck('id'))
            ->first();
        if ($award_receive && $award_receive->awards_receive > 0):
            $user_receive = $award_receive->awards_receive;
        endif;
        $user_awards = UserAward::select('award_id','user_id')->where('user_id', $id)->groupBy('award_id')->get();
        $response['data']['awards']=null;
        if (count($user_awards) > 0):
            foreach ($user_awards as $user_award):
                $this->preparedAwards([$user_award]);
                $response['data']['awards'][] = $user_award;
            endforeach;
        endif;


        $response['status'] = 'ok';
        $response['data']['challenges'] = ['challenges_won' => $user_won,
            'challenges_number' => $user_number,
            'award_receive' => $user_receive];

        return response()->json($response, $code);
    }

    public function preparedAwards($user_awards)
    {
        foreach ($user_awards as &$user_award):
            $user_award = UserAward::preparedAwards($user_award);
        endforeach;
    }

    public function updateImageChallengeList(Request $request){

        $response = ['status' => 'fail', 'data' => []];
        $code = 200;

        $this->rules = [
            'id' => 'required',
            'type' => 'required',
            'image' => 'required',
        ];

        $data = $request->all();

        // make a new validator object
        $v = Validator::make($data, $this->rules);
        // check for failure
        if ($v->fails()) {
            // set errors and return false
            return response()->json($v->errors()->toArray(), 200);
        }


        if($request->type != "challenge" && $request->type != "list"){
            $response = ['status' => 'fail', 'data' => [
                'error' => 'please enter type as challenge or list'
            ]];
            return response()->json($response, 200);
        }

        // validation pass
        if($request->type == "challenge"){
            $object = Challenge::
            select('*')->where('id', $request->id)->first();
        }elseif($request->type == "list") {
            $object = UserList::
            select('*')->where('id', $request->id)->first();
        }
        

        if(!$object){
            $response = ['status' => 'fail', 'data' => [
                'error' => 'object not found'
            ]];
            return response()->json($response, 200);
        }

        //checks if is there a pic already
        if (!empty($object->image_mini)):
            $path = str_replace(url('/'), '', $object->image_mini);
            file_exists(public_path() . $path) and unlink(public_path() . $path);
        endif;
        $comp = explode(';', $request->image);
        $ext = explode('/', $comp[0])[1];
        $img = explode(',', $comp[1])[1];
        $image = base64_decode($img);
        $ext === 'jpeg' and ($ext = 'jpg');
        $filename = '/image_mini/' . md5(str_random(8)) . ".{$ext}";
        file_put_contents(public_path() . '/' . $filename, $image);
        $object->image_mini = url('/') . $filename;

        $object->save();

        $response['status'] = 'ok';
        return response()->json($response, 200);
        
    }
}
