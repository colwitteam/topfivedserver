<?php
namespace App\Http;

use Illuminate\Foundation\Http\Kernel as HttpKernel;
/**
 * @apiDefine NotContent Successfully Created reg.
 * @apiErrorExample Success-Response:
 *     HTTP/1.1 201 Not Content
 *     {
 *       "status": "ok",
 *       "data": []
 *     }
 */
/**
 * @apiDefine NotAcceptable Bad request.
 *
 * @apiErrorExample BadRequest-Error:
 *     HTTP/1.1 400 Bad Request
 *     {
 *       "status": "fail",
 *       "data": []
 *     }
 */
/**
 * @apiDefine NotFound Not Found.
 *
 * @apiErrorExample NotFound-Error:
 *     HTTP/1.1 404 Not Found
 *     {
 *       "status": "fail",
 *       "data": []
 *     }
 */

/**
 * @apiDefine InternalError Server error.
 *
 * @apiErrorExample Internal-Error:
 *     HTTP/1.1 500 Server Error
 *     {
 *       "status": "fail",
 *       "data": []
 *     }
 */

/**
 * @apiDefine Forbidden Forbidden.
 *
 * @apiErrorExample NotAuthorized-Error:
 *     HTTP/1.1 403 Forbidden
 *     {
 *       "status": "fail",
 *       "data": []
 *     }
 */
class Kernel extends HttpKernel
{
    /**
     * The application's global HTTP middleware stack.
     *
     * These middleware are run during every request to your application.
     *
     * @var array
     */
    protected $middleware = [
        \Illuminate\Foundation\Http\Middleware\CheckForMaintenanceMode::class,
        \Illuminate\Foundation\Http\Middleware\ValidatePostSize::class,
        \App\Http\Middleware\TrimStrings::class,
        \Illuminate\Foundation\Http\Middleware\ConvertEmptyStringsToNull::class,
        \Illuminate\Session\Middleware\StartSession::class,
        \Illuminate\View\Middleware\ShareErrorsFromSession::class,
    ];

    /**
     * The application's route middleware groups.
     *
     * @var array
     */
    protected $middlewareGroups = [
        'web' => [
            \Illuminate\Session\Middleware\StartSession::class,
            \App\Http\Middleware\EncryptCookies::class,
            \Illuminate\Cookie\Middleware\AddQueuedCookiesToResponse::class,
            \Illuminate\View\Middleware\ShareErrorsFromSession::class,
            \Illuminate\Routing\Middleware\SubstituteBindings::class,

        ],

        'api' => [
            \Illuminate\Session\Middleware\StartSession::class,
            'throttle:60,1',
            'bindings',
        ],
    ];

    /**
     * The application's route middleware.
     *
     * These middleware may be assigned to groups or used individually.
     *
     * @var array
     */
    protected $routeMiddleware = [
        'bindings' => \Illuminate\Routing\Middleware\SubstituteBindings::class,
        'can' => \Illuminate\Auth\Middleware\Authorize::class,
        'guest' => \App\Http\Middleware\RedirectIfAuthenticated::class,
        'throttle' => \Illuminate\Routing\Middleware\ThrottleRequests::class,
        'checkToken' => \App\Http\Middleware\CheckToken::class,
        'authUserApi'=>\App\Http\Middleware\CheckAuthUser::class,
        'checkHiddenUser'=>\App\Http\Middleware\CheckUserHidden::class,
    ];
}
