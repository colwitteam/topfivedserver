<?php

namespace App\Http\Middleware;

use Closure;
use App\Helpers\RequestCurl;
use Session;

class CheckAuthUser
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        $user = $this->getUserAuth();
        if($user['status']!='ok'):
            Session::forget("access_token");
            return redirect("admin/login");
        endif;
        $response = $next($request);

        return $response->header('Cache-Control','nocache, no-store, max-age=0, must-revalidate')
            ->header('Pragma','no-cache')
            ->header('Expires','Fri, 01 Jan 1990 00:00:00 GMT');
    }

    public function getUserAuth(){
        $url_user = env('APP_URL_API')."profile";
        $auth_user = new RequestCurl();
        $auth_user->settingHeader('Bearer '.Session::get('access_token'));
        $response = $auth_user->requestUrl($url_user,'get');
        return $response;


    }
}
