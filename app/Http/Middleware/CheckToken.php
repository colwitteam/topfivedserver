<?php
namespace App\Http\Middleware;

use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;
use Closure;
/**
 * @author Javier Serrano
 * @todo Handling hashes from a database in regarding acting as thirdparty provider for anyone.
 *
 */
class CheckToken
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $request->session()->regenerate();
        $response = ['status'=>'fail','data'=>[]];
        $code = 400;

        try {
            if (!!$user = JWTAuth::parseToken()->authenticate()):
                return $next($request);
            endif;
        } catch (\Tymon\JWTAuth\Exceptions\TokenExpiredException $e) {
            $response['status'] = 'token_expired';
            $code = $e->getStatusCode();
        } catch (\Tymon\JWTAuth\Exceptions\TokenInvalidException $e) {
            $response['status'] = 'token_invalid';
            $code = $e->getStatusCode();
        } catch (\Tymon\JWTAuth\Exceptions\JWTException $e) {
            $response['status'] = 'token_absent';
            $code = $e->getStatusCode();

        }

        return response()->json($response, $code);
    }
}
