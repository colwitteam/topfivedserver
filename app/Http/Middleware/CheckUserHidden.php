<?php

namespace App\Http\Middleware;

use App\Challenge;
use App\HiddenUser;
use App\User;
use App\UserList;
use Closure;
use JWTAuth;
use App\UserFollow;

class CheckUserHidden
{

    private $userPrivacy;
    private $userContext;
    private $list;
    private $challenge;
    private $type_view;

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        $token = JWTAuth::getToken();
        $this->userContext = JWTAuth::toUser($token);

        $check = 1;//$check 0 = false $check 1 = user is enable $check = 2 user is private no existe 3
        $code = 200;
        $response = ['status' => 'fail', 'data' => []];
        //lists
        $method = $this->ejecuteMethod($request->route()->getAction()['controller']);

        if ($request->is('api/list/*') || $request->is('api/like') || $request->is('api/comment/*') || $request->is('api/list_saves')):
            $check = $this->checkList($request, $method);
        elseif ($request->is('api/challenge') || $request->is('api/challenge/*')):
            //challenges
            $check = $this->checkChallenge($request, $method);
        elseif ($request->is('api/message') || $request->is('api/message/*')):
            $check = $this->checkMessage($request, $method);
        elseif ($request->is('api/user/*') || $request->is('api/follow') || $request->is('api/user/followers/*') || $request->is('api/user/following/*' || $request->is('api/feed/*') || $request->is('api/users/challenge/*'))):
            $check = $this->CheckUser($request, $method);
        endif;
        
        if ($check == 1):
            $response['data'] = ['status' => $this->type_view . ' disabled'];
            return response()->json($response, $code);
        elseif ($check == 2):
            $response['data'] = ['status' => $this->type_view . ' private'];
            return response()->json($response, $code);
        else:
            //$accountPrivate = 1 indica que la lista es privada
            // 2 indica si el usuario es public o si el usuario logeado sigue al usuario
            // 0 indica si es usuario es privado

            $accountPrivate = 2;
            if ($method != "followUser" || ($request->is('api/challenge/*') && $method == 'store')):

                $accountPrivate = $this->validateAccount($request, $method);
            endif;
            if ($accountPrivate == 0):
                $response['data'] = ['status' => $this->type_view . ' private'];
                return response()->json($response, $code);
            elseif ($accountPrivate == 1):
                $response['data'] = ['status' => $this->type_view . ' private'];
                return response()->json($response, $code);
              elseif ($accountPrivate == 3):
                $response['data'] = ['status' => $this->type_view . ' private'];
                return response()->json($response, $code);
            else:
                return $next($request);
            endif;
        endif;
    }
    /*Se valida si la cuenta pertenece a una cuenta privada o public y se determina si el usuario logeado sigue al
    usuario privado*/
    private function validateAccount($request = null, $method = null)
    {
        if (($request->is('api/user/*') && $method == 'show')):
            return 2;
        elseif (($request->is('api/challenge/*') && $method == 'show')):
            $user_owner = User::where(['id' => $this->challenge->owner_id, 'private' => 1])->first();
            $user_target = User::where(['id' => $this->challenge->target_id, 'private' => 1])->first();            
            if ($user_owner || $user_target):
                if ($user_owner):
                    $follow = UserFollow::where(['user_id' => $this->userContext->id])
                        ->where(['follows_to' => $user_owner->id])->first();
                    if ($follow || $user_owner->id == $this->userContext->id):
                        return 2;
                    else:
                        return 0;
                    endif;
                else:
                    $follow_second = UserFollow::where(['user_id' => $this->userContext->id])
                        ->where(['follows_to' => $user_target->id])->first();

                    if ($follow_second || $user_target->id == $this->userContext->id):
                        return 2;
                    else:
                        return 0;
                    endif;
                endif;
          
            endif;
            return 2;
        else:
            if ($this->userPrivacy != null):
                if ($this->userPrivacy->id != $this->userContext->id):

                    if ($this->list != null):
                        if ($this->list->is_private != 0):
                            $this->type_view = 'list';
                            return 1;
                        endif;
                    endif;
                    if ($this->userPrivacy->private != 1):
                        return 2;
                    else:
                        if (($request->is('api/challenge') && $method == 'store')):
                            $this->type_view = 'users';
                            $follow_private = UserFollow::where(['user_id' => $this->userContext->id])
                                ->where(['follows_to' => $this->userPrivacy->id])->first();
                            if ($follow_private):
                                $follow_current = UserFollow::where(['user_id' => $this->userPrivacy->id])
                                    ->where(['follows_to' => $this->userContext->id])->first();
                                if ($follow_current):
                                    return 2;
                                endif;
                                return 0;
                            endif;
                            return 0;
                        endif;
                        $follow = UserFollow::where(['user_id' => $this->userContext->id])
                            ->where(['follows_to' => $this->userPrivacy->id])->first();

                        if ($follow):
                            return 2;
                        else:
                            $this->type_view = 'user';
                            return 0;
                        endif;
                    endif;
                else:
                    return 2;
                endif;
            else:
                return 2;
            endif;
        endif;
    }
    /*se verifica si el usuario ha sido bloqueado por el dueño de la lista */
    private function checkList($request)
    {
        $this->type_view = 'List';
        $method = $this->ejecuteMethod($request->route()->getAction()['controller']);
        switch ($method):
            //this case where request id
            case 'getList':
            case 'getListComments':
            case 'getLocationsList':
            case 'getUsersLikesByList':
                $this->list = UserList::find($request->id);
                if ($this->list):
                    if ($this->list->enable == 1):
                        if ($this->list):
                            return $this->userEnabled($this->list->user_id);
                        else:
                            return 0;
                        endif;
                    else:
                        return 1;
                    endif;
                    return 1;
                endif;
                break;
            case 'getAllByUser':
                $user = User::find($request->id);
                if ($user):
                    return $this->userEnabled($user->id);
                endif;
                return 0;
                break;
            //this cases where request using list_id
            case 'setLike':
            case 'commentList':
            case 'store':
                $list = UserList::find($request->list_id);
                if ($list):
                    if ($list->is_private == 1 && $list->user_id != $this->userContext->id && $list->enable == 1):
                        return 2;
                    elseif ($list->enable == 2):
                        return 1;
                    endif;
                    return $this->userEnabled($list->user_id);
                endif;
                return 0;
                break;
        endswitch;

        return true;
    }
    /*se verifica si el usuario ha sido bloqueado por el dueño y competidor de la challenges si ha sido bloqueado
    no se generara ningun de estos metods*/
    private function checkChallenge($request, $method)
    {
        $this->type_view = 'challenge';
        switch ($method):
            case 'store':
                if ($this->userContext->private == 0):
                    return $this->createChallenge($request);
                else:
                    $folling_to_user = $this->getMyFollowers($request->target_id);
                    if ($folling_to_user):
                        return $this->createChallenge($request);
                    else:
                        $this->type_view = 'your not following';
                        return 1;
                    endif;
                endif;
                break;
            case 'replyChallenge':
                if ($request->status == 2):
                    $challenge = Challenge::find($request->id);
                    if ($this->userContext->private == 0):
                        return $this->replyChallenge($challenge);
                    else:
                        $folling_to_user = $this->getMyFollowers($challenge->owner_id);
                        if ($folling_to_user):
                            return $this->replyChallenge($challenge);
                        else:
                            $this->type_view = 'your not following';
                            return 1;
                        endif;
                    endif;
                endif;
                return 0;
                break;
            case 'startChallenge':
                if ($request->status == 4):
                    $challenge = Challenge::find($request->id);

                    if ($this->userContext->private == 0):
                        return $this->replyChallenge($challenge, true);
                    else:
                        $folling_to_user = $this->getMyFollowers($challenge->target_id);
                        if ($folling_to_user):
                            return $this->replyChallenge($challenge, true);
                        else:
                            $this->type_view = 'your count is';
                            return 1;
                        endif;
                    endif;
                endif;
                return 0;
                break;
            case 'show':
                $this->challenge = Challenge::find($request->challenge);
                if ($this->challenge):
                    $disanbled = $this->userEnabled($this->challenge->target_id);
                    if (!$disanbled):
                        $disanbled = $this->userEnabled($this->challenge->owner_id);
                    endif;
                    return $disanbled;
                endif;
                return 0;
                break;
        endswitch;
    }
    /*Se verifica si el usuario logeado ha sido bloqueado por el usuario*/
    private function checkMessage($request, $method)
    {

        $this->type_view = 'user';
        switch ($method):
            case 'store':
                $user = User::find($request->user_receiver);
                if($user):
                    $user_message = User::find($request->user_receiver);

                    if($user_message->banned==0):
                        $disanbled = $this->userEnabled($user->id);
                        if($disanbled==0):
                            $disanbled = $this->disabledForUser($request->user_receiver);
                        endif;

                        return $disanbled;
                    endif;
                    return 1;
                endif;
                return 0;
                break;
        endswitch;
    }
    //determina si el usuario esta habilitado por el otro
    private function checkUser($request, $method)
    {
        $this->type_view = 'user';
        switch ($method):
            case 'followUser':
                return $this->userEnabled($request->userId);
                break;
            case 'getFollowersById':
            case 'getUserFollowingsById':
            case 'showByUser':
            case 'show':
                return $this->userEnabled($request->id);
                break;
            case 'getFeed':
                return $this->userEnabled($request->uid);
                break;
        endswitch;
    }
    //detecta que metodo se esta usando get delete put post
    private function ejecuteMethod($method)
    {
        $method = explode('@', $method);
        return $method[1];
    }
    //determina si el usuario logeado ha bloqueado al otro usuario
    private function userEnabled($user_hidden)
    {
        $this->userPrivacy = User::find($user_hidden);

        $userHidden = HiddenUser::where(['user_id' => $user_hidden, 'user_hidden_id' => $this->userContext->id])->first();
      if ($userHidden):
            return 1;
        endif;
        return 0;
    }
    //determina si el usuario logeado ha sido bloqueado por el usuario
    private function disabledForUser($user_hidden)
    {
        $userHiddenAffect = HiddenUser::where(['user_id' => $this->userContext->id, 'user_hidden_id' => $user_hidden])->first();
        if ($userHiddenAffect):
            return 1;
        endif;
        return 0;
    }
    //Se obtiene todo los seguidores del usuario logeado
    private function getMyFollowers($user)
    {
        $user = UserFollow::where('user_id', $user)->where('follows_to', $this->userContext->id)->first();
        if ($user):
            return true;
        endif;
        return false;
    }
    //Se calcula si algun usuario a bloqueado uno al otro
    private function createChallenge($request)
    {
        $user_target = User::find($request->target_id);
        if ($user_target):
            if ($user_target->banned == 0):
                $user_enabled = $this->userEnabled($request->target_id);
                if ($user_enabled == 0):
                    $user_enabled = $this->disabledForUser($request->target_id);
                    return $user_enabled;
                else:
                    return $user_enabled;
                endif;
            endif;
            $this->type_view = 'user';
            return 1;
        endif;
        return 0;
    }
    // se determina si algun usuario ha bloqueado al otro 
    private function replyChallenge($challenge, $target = null)
    {

        if ($challenge):
            $compare_user = $challenge->owner_id;
            if ($target):
                $compare_user = $challenge->target_id;
            endif;
            $user_banned = User::find($compare_user);
            if ($user_banned->banned == 0):
                $user_enabled = $this->userEnabled($compare_user);
                if ($user_enabled == 0):
                    $user_enabled = $this->disabledForUser($compare_user);
                    return $user_enabled;
                else:
                    return $user_enabled;
                endif;
            endif;
            $this->type_view = 'user';
            return 1;
        endif;
        return 0;
    }
}
