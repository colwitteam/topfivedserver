<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @todo validates if a list has and/or belongs to many categories
 * @author rantes
 *
 */
class ListCategory extends Model
{
    protected $fillable = ['user_id','category_id'];
    public $timestamps = false;
}
