<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Validator;
use JWTAuth;

class ListComment extends Model {

    private $rules = [];
    private $errors;
    protected $fillable = ['list_id', 'user_id', 'comment'];
    protected $hidden = ['created_at', 'updated_at'];

    public function validate($data) {
        $this->rules = [
            'list_id' => 'required',
            'user_id' => 'required',
            'comment' => 'required',
        ];
        // make a new validator object
        $v = Validator::make($data, $this->rules);

        // check for failure
        if ($v->fails()) {
            // set errors and return false
            $this->errors = $v->errors()->toArray();
            return false;
        }

        // validation pass
        return true;
    }

    public function errors() {
        return $this->errors;
    }

    public static function extractUsers($comment) {

        preg_match_all('~{uid:(\d+)}~', $comment, $matches);

        return $matches[1];
    }

    public static function extractHashtags($comment) {

        preg_match_all('~#(\w+)~', $comment, $matches);

        return $matches[1];
    }

    public static function prepareComment($comment) {

        $token = JWTAuth::getToken();
        $user = JWTAuth::toUser($token);
        $prepared_comment = '';
        $users_mentions = [];
        $owner = User::find($comment->user_id);

        $owner_comment = User::prepareLazy($owner);
        $hidden_user = HiddenUser::select('user_id')->where('user_id', $owner_comment->id)->where('user_hidden_id', '=', $user->id)->first();
        if (!$hidden_user):
            $id_users = self::extractUsers($comment->comment);
            $comment->time_comment = $comment->created_at->diffForHumans();
            $comment->owner_user = $owner_comment;
            if (count($id_users) > 0) :
                foreach ($id_users as $user_id) :
                    $user_find = User::find($user_id);
                    $prepared_comment = self::replaceMention($user_id, $comment->comment, $user_find->username);
                    $hidden_user_mentions = HiddenUser::select('user_id')->where('user_id', $user_id)
                                    ->where('user_hidden_id', '=', $user->id)->first();
                    if (!$hidden_user_mentions):
                        $users_mentions[] = User::prepareLazy($user_find);
                    endif;
                endforeach;
                $comment->comment = $prepared_comment;
                $comment->mentions = $users_mentions;
            endif;
            $list_hashtags = [];
            $hashtags = self::extractHashtags($comment->comment);
            if($hashtags > 0):
                foreach($hashtags as $hashtag):
                    $find_hashtag = Hashtag::select('id','keyword')->where(['keyword'=>$hashtag])->first();
                    if($find_hashtag):
                        $list_hashtags[] = Hashtag::preparedHashtag($find_hashtag);
                    endif;
                endforeach;
            endif;

            $comment->hashtags_comment= $list_hashtags;
        endif;

        return $comment;
    }

    public static function replaceMention($id, $comment, $replacement) {

        $mention = preg_replace('/({uid:[' . $id . ']+})/', $replacement, $comment);

        return $mention;
    }

    public static function replaceUser($comment) {

        $idUsers = self::extractUsers($comment);
        $users = [];
        foreach ($idUsers as $idUser):
            if (empty($users[$idUser])):
                $user = User::find($idUser);
                $users[$idUser] = $user->username;
                $mention = self::replaceMention($user->id, $comment, $users[$idUser]);
                $comment = $mention;
            endif;
        endforeach;

        return $comment;
    }

}
