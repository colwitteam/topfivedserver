<?php

namespace App;

use App\Events\ListPopularities;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;

class ListFavorite extends Model {

    protected $fillable = ['user_id', 'list_id'];
    protected $hidden = ['created_at', 'updated_at'];
//    protected $events = [
//        'saved' => ListPopularities::class
//    ];
    private $rules = [];
    private $errors;

    public function validate($data) {

        $this->rules = [
            'user_id' => 'required',
            'list_id' => 'required',
        ];
        // make a new validator object
        $v = Validator::make($data, $this->rules);

        // check for failure
        if ($v->fails()) {
            // set errors and return false
            $this->errors = $v->errors()->toArray();
            return false;
        }

        // validation pass
        return true;
    }

    public function errors() {
        return $this->errors;
    }

    public static function getUsersLikes($list) {

        $usersList = [];
        $usersLike = self::select('user_id')
                        ->where('list_id', $list->id)
                        ->groupBy('user_id')->get();

        if (!$usersLike->isEmpty()):
            foreach ($usersLike as $user):
                $usersList [] = $user->user_id;
            endforeach;
        endif;

        return $usersList;
    }

}
