<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Hashtag;

class ListHashtag extends Model
{
    public static function saveTags($listId, array $hashtags) {
        $currentHashtags = self::where('list_id', $listId)->delete();

        foreach ($hashtags as $keyword):
            $keyword = str_replace('#','',$keyword);
            $listTags = new ListHashtag();
            $hashtag = Hashtag::where(['keyword'=>$keyword])->first();
            if (empty($hashtag->id)):
                $hashtag = new Hashtag();
                $hashtag->keyword = $keyword;
                $hashtag->save();
            endif;
            $listTags->list_id = $listId;
            $listTags->hashtag_id = $hashtag->id;
            $listTags->save();
        endforeach;

        return true;
    }
    public static function preparedHashtagTop($hashtag){
        if($hashtag->target==5):
            $hashtag->type = 'theme';
        else:
            $hashtag->type = 'hashtag';
        endif;
        return $hashtag;
    }
}
