<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ListImage extends Model
{
    protected $fillable = ['list_id','image','order'];
    protected $hidden = ['created_at','updated_at'];
}
