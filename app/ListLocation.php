<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ListLocation extends Model
{
    protected $fillable = ['list_id','location_id'];
    public $timestamps = false;
}
