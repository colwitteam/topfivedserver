<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use JWTAuth;

class ListPopularity extends Model {
    
    protected $fillable = ['id', 'list_id', 'quantity'];
    protected $hidden = ['created_at', 'updated_at'];
    
    
    public function scopePopularLists($query){
        $token = JWTAuth::getToken();
        $user = JWTAuth::toUser($token);

        $query->select('list_popularities.*');
        $query->join('lists','lists.id','=','list_popularities.list_id');
        $query->join('users','users.id','=','lists.user_id');
        $query->whereNotIn('users.id',HiddenUser::select('user_id')->where('user_hidden_id', '=', $user->id)->get());
        $query->where('users.banned','=',0);
        $query->where('lists.is_private','=',0);
        $query->where('lists.enable','=',1);
        $query->orderBy('quantity', 'DESC');
        
    }
    
}
