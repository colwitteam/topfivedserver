<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ListSave extends Model
{
    protected $fillable = ['list_id','user_id'];
    protected $hidden = ['created_at','updated_at'];
}