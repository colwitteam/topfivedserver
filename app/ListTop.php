<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Top;

class ListTop extends Model {

    public $timestamps = false;

    public static function saveTops($listId, array $tops) {
        $currentTops = self::where('list_id', $listId)->delete();

        foreach ($tops as $order => $keywords):
            $listTop = new ListTop();
            $top = Top::where(['name'=>$keywords])->first();
            if (empty($top->id)):
                $top = new Top();
                $top->name = $keywords;
                $top->save();
            endif;
            $listTop->list_id = $listId;
            $listTop->top_id = $top->id;
            $listTop->order = $order;
            $listTop->save();
        endforeach;

        return true;
    }

}
