<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ListVideo extends Model
{
    protected $fillable = ['list_id','video','order'];
    protected $hidden = ['created_at','updated_at'];
}
