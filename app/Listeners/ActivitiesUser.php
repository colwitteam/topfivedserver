<?php

namespace App\Listeners;

use App\Activity;
use App\Events\EventActivitiesUser;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class ActivitiesUser
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {

    }

    /**
     * Handle the event.
     *
     * @param  EventActivitiesUser  $event
     * @return void
     */
    public function handle(EventActivitiesUser $event)
    {
        if($event->option=='all'):
            $this->deleteAllActivities($event->user_origin,$event->active_activities);
        else:
            $this->deleteItemActivity($event->activity_id);
        endif;
    }

    private function deleteAllActivities($user,$enabled){

        $activities  = Activity::where('origin',$user->id)->get();
        foreach($activities  as $activity):
            if(!$enabled):
                $activity->delete();
            endif;
        endforeach;
        if($enabled):
            $restore = Activity::where('origin',$user->id)->withTrashed()->restore();
        endif;
    }

    private function deleteItemActivity($id,$enabled){
        $activity = Activity::find($id);

        if(!$enabled):
                $activity->delete();
        endif;

        if($enabled):
            $restore = Activity::where('id',$id)->withTrashed()->restore();
        endif;
    }
}
