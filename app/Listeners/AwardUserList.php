<?php

namespace App\Listeners;

use App\Award;
use App\Events\EventAwardsUserList;
use app\Helpers\Utils;
use App\User;
use App\UserAward;
use App\UserList;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\DB;

class AwardUserList
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  EventAwardsUserList  $event
     * @return void
     */
    public function handle(EventAwardsUserList $event)
    {
        $awards_user = Award::where('type',2)->get();

        $count_list = UserList::select(DB::raw('count(user_id) as total'))->where('user_id',$event->user->id)->groupBy('user_id')->first();

        if($awards_user):
            foreach ($awards_user as $award):
                if($count_list->total>=$award->quantity):
                    $award_user = UserAward::where(['user_id'=>$event->user->id,'award_id'=>$award->id])->first();
                    if(!$award_user):
                        $award_save = UserAward::create(['user_id' => $event->user->id,
                                                        'award_id' => $award->id]);

                        $firebaseNotification = new Utils();
                        $firebaseNotification->sendNotificationFirebase($event->user->token_android, "Award list", $award_save, 'award_user');

                    endif;
                endif;
            endforeach;
        endif;
    }
}
