<?php

namespace App\Listeners;

use App\Challenge;
use App\Events\ChallengeCancelUser;
use App\Search;
use App\User;
use App\UserFollow;
use App\UserList;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use JWTAuth;

class CancelChallenge
{
    public $user;
    public $user_enabled;

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {

    }

    /**
     * Handle the event.
     *
     * @param  ChallengeCancelUser $event
     * @return void
     */
    public function handle(ChallengeCancelUser $event)
    {
        $token = JWTAuth::getToken();
        $this->user = JWTAuth::toUser($token);
        $this->user_enabled = $event->enable_user;
        if ($this->user->role == 1):
            $this->cancelChallengeForUser($event->user_disable);
        else:
            $this->cancelChallengeForAdmin($event->user_disable);
        endif;

    }

    private function cancelChallengeForUser(User $user)
    {
        $user_current = $this->user;
        $this->changeCancelChallenges($user,$user_current);
        $this->deleteFollow($user,$user_current);
        $this->searchDisable($user,$user_current);

    }

    private function cancelChallengeForAdmin(User $user)
    {
        $this->changeCancelChallenges($user);
        //$this->deleteFollow($user);
        $this->searchDisable($user);
    }
    private function changeCancelChallenges($user,$user_current=null){

        $challenges = Challenge::where(function ($where) use ($user) {
            $where->where('owner_id', $user->id);
            $where->orWhere('target_id', $user->id);
        })->where(function ($where) use ($user_current) {
            if($user_current):
                $where->where('owner_id', $user_current->id);
                $where->orWhere('target_id', $user_current->id);
            endif;
        })->get();
        if ($challenges):
            foreach ($challenges as $challenge):
                $challenge->status = 5;
                $challenge->save();
            endforeach;
        endif;
    }

    private function deleteFollow($user,$user_current=null){
        $id_current = isset($user_current)?$user_current->id:null;
        $following = UserFollow::where(['user_id' => $user->id, 'follows_to' => $id_current])
            ->orWhere(function ($where) use ($id_current, $user) {
                $where->where('follows_to', '=', $user->id);
                if($id_current):
                    $where->orWhere('user_id', '=', $id_current);
                endif;
            })->get();

        if ($following):
            foreach ($following as $foll):
                if (!$this->user_enabled):
                    $foll->delete();
                endif;
            endforeach;
        endif;
    }

    private function searchDisable($user,$user_current=null){
        $id_current = isset($user_current)?$user_current->id:null;
        $get_my_list = UserList::where('user_id', '=', $id_current)->get();

        if (!$this->user_enabled):
            $searches_lists = Search::where(['target' => 1, 'user_id' => $user->id]);
            if($id_current):
                $searches_lists->whereIn('searched_item_id', $get_my_list);
            endif;
            $searches_lists = $searches_lists->get();

            foreach ($searches_lists as $searches_list):

                $searches_list->delete();
            endforeach;
        else:
            $searches_lists = Search::where(['target' => 1, 'user_id' => $user->id]);
            if($id_current):
                $searches_lists->whereIn('searched_item_id', $get_my_list);
            endif;
            $searches_lists->withTrashed()->restore();
        endif;

        if (!$this->user_enabled):
            $searches_users = Search::where(['target'=> 2, 'user_id'=>$user->id]);

            if($id_current):
                $searches_users->where( 'searched_item_id', '=', $id_current);
            endif;
            $searches_users = $searches_users->get();

            foreach ($searches_users as $searches_user):
                $searches_user->delete();
            endforeach;
        else:
            $searches_users = Search::where(['target' => 2, 'user_id' => $user->id])->withTrashed()->restore();
        endif;
    }
}
