<?php

namespace App\Listeners;

use App\Challenge;
use App\Events\ListChangePopularities;
use App\ListPopularity;
use App\UserList;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\DB;

class ListChangePopular
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {

    }

    /**
     * Handle the event.
     *
     * @param  ListChangePopularities  $event
     * @return void
     */
    public function handle(ListChangePopularities $event){


        if($event->_list->is_private==1 || $event->_list->enable==2):
            if($event->_list->enable==2):
                $this->cancelChallenge($event->_list);
            endif;
            $this->listPrivate($event->_list);
        else:
            $this->listPublic($event->_list);
        endif;
    }
    public function listPrivate($list){

        $list_populary = ListPopularity::where('list_id', $list->id)->first();
        if($list_populary):
           $list_populary->delete();
           $list->putListPopular();
        endif;
    }

    public function listPublic($list){
        $quantity = $list->quantityPopulary();
        $total_list = ListPopularity::all();

        if(count($total_list)>=100):
            $list_popular = ListPopularity::where('quantity','<',$quantity)->orderBy('quantity')->first();
            if($list_popular):
                $list_popular->list_id = $list->id;
                $list_popular->quantity = $quantity;
                $list_popular->save();
            endif;
        else:
            $find_list = ListPopularity::where('list_id','=',$list->id)->first();
            if(!$find_list):
                $list_popular = new ListPopularity();
                $list_popular->list_id = $list->id;
                $list_popular->quantity = $quantity;
                $list_popular->save();
            else:
                $find_list->quantity = $quantity;
                $find_list->save();
            endif;
        endif;
    }

    private function cancelChallenge($list){
        $challenges = Challenge::where(function($where) use ($list){
            $where->where('owner_list_id',$list->id);
            $where->orWhere('target_list_id',$list->id);
        })->where('status','<',5)->get();

        foreach($challenges as $challenge):
                $challenge->status = 5;
                $challenge->save();
        endforeach;
    }
}
