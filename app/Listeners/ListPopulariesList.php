<?php

namespace App\Listeners;

use App\Award;
use App\Events\ListPopularities;
use app\Helpers\Utils;
use App\ListPopularity;
use App\User;
use App\UserAward;
use App\UserList;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class ListPopulariesList
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  ListPopularities $event
     * @return void
     */
    public function handle(ListPopularities $event)
    {

        $lists_popularities = ListPopularity::orderBy('quantity')->skip(0)->take(100)->get();
        $list = UserList::where('id', $event->listFavorite->list_id)->first();
        $this->awardsList($list);

        if (count($lists_popularities) >= 100):
            $last_list = $lists_popularities[0];
            if ($last_list->quantity < $list->quantityPopulary()):
                $this->updatePopylary($last_list, $list);
            endif;
        else:
            $list_exist = ListPopularity::where('list_id', $event->listFavorite->list_id)->first();
            if ($list_exist):
                $this->updatePopylary($list_exist, $list);
            else:
                $this->saveListPopulary($list);
            endif;

        endif;
    }

    public function saveListPopulary($list)
    {
        $list_popularity = new ListPopularity();
        $list_popularity->list_id = $list->id;
        $list_popularity->quantity = $list->quantityPopulary();
        $list_popularity->save();
    }

    public function updatePopylary($old_list_populary, $new_list)
    {
        $update_list = ListPopularity::find($old_list_populary->id);
        if ($old_list_populary->list_id != $new_list->id) {
            $update_list->list_id = $new_list->list_id;
        }
        $update_list->quantity = $new_list->quantityPopulary();
        $update_list->save();
    }

    public function awardsList(UserList $list)
    {

        $not_awards = Award::where('location','neighborhood')->get();

        $awards = Award::where('type',1)->whereNotIn('id',$not_awards->pluck('id'))->get();

        foreach ($awards as $award) {
            $boolean_award=false;
            $award_user = UserAward::where(['list_id' => $list->id, 'award_id' => $award->id])->first();
            if (!$award_user):
                switch ($award->location):
                    case 'neighborhood':
                        $count_like = $list->getCountHashtags($award->topic_quantity);
                        if ($count_like):
                            $count_user = $list->setAwardsNeighborhood();
                            $boolean_award = isset($count_user->count_like) && $count_user->count_like >= $award->quantity ? true : false;
                        endif;
                        break;
                    case 'city':
                    case 'country':
                        $count_like = $list->getCountHashtags($award->topic_quantity);

                        if ($count_like):
                            $boolean_award = isset($list->setAwardsOutNeighborhood($award->location, $award->topic_quantity)->count_like) && $list->setAwardsOutNeighborhood($award->location)->count_like >= $award->quantity ? true : false;

                        endif;
                        break;
                endswitch;
                if ($boolean_award):
                    $user_award = UserAward::create([
                        'user_id' => $list->user_id,
                        'list_id' => $list->id,
                        'award_id' => $award->id
                    ]);
                    $this->preparedAwardList([$user_award]);
                    $user = User::find($list->user_id);
                    $firebaseNotification = new Utils();
                    $firebaseNotification->sendNotificationFirebase($user->token_android, "Award list", $user_award, 'award_list');

                endif;
            endif;
        }
    }

    public function preparedAwardList($awards){
        foreach($awards as &$award):
            $award = Award::preparedAwardList($award);
        endforeach;
    }
}
