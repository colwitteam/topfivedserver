<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Validator;

class Location extends Model
{
    private $rules = [];
    private $errors;
    protected $fillable = ['name'];
    protected $hidden = ['created_at', 'updated_at'];

    public function validate($data) {
        $this->rules = [
            'name' => 'required|unique:locations,name,'.($this->id ? $this->id : ''),
        ];
        // make a new validator object
        $v = Validator::make($data, $this->rules);

        // check for failure
        if ($v->fails()) {
            // set errors and return false
            $this->errors = $v->errors()->toArray();
            return false;
        }

        // validation pass
        return true;
    }

    public function errors() {
        return $this->errors;
    }

    public static function getCurrentLocsId() {
        $ids = [];
        $locs = self::select('id')->get();

        foreach ($locs as $i => $loc):
            $ids[] = $loc->id;
        endforeach;

        return $ids;
    }
}
