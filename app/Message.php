<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Validator;
use App\User;
use JWTAuth;
use Illuminate\Support\Facades\DB;
use App\Helpers\Utils;

class Message extends Model
{
    private $rules = [];
    protected $fillable = [
        'user_transmitter', 'user_receiver', 'message'
    ];
    protected $hidden = [
        'created_at', 'updated_at'
    ];
    private $errors;

    public function validate($data)
    {
        $this->rules = [
            'user_transmitter' => 'required',
            'user_receiver' => 'required',
            'message' => 'required',
        ];
        $v = Validator::make($data, $this->rules);
        if ($v->fails()) {
            $this->errors = $v->errors()->toArray();
            return false;
        }
        return true;
    }

    public function errors()
    {
        return $this->errors;
    }

    public function scopeMessage($query,$user){

        $query->select('*');
        $query->from(DB::raw("(" .
            "SELECT messages.user_transmitter as user, ".
            "messages.created_at as created_at ".
            "FROM messages " .
            "WHERE messages.user_receiver = " . $user->id ." ".
            "UNION ALL " .
            "SELECT messages.user_receiver as user, ".
            "messages.created_at as created_at ".
            "FROM messages ".
            "WHERE messages.user_transmitter = " . $user->id .
            ")messages"));
        $query->groupBy('user');
        $query->orderByDesc('created_at');

    }
    public static function prepareMessage($message)
    {
        $token = JWTAuth::getToken();
        $user = JWTAuth::toUser($token);

        $user_receiver = User::find($message->user_receiver);
        $message->user_receiver = User::prepareLazy($user_receiver);

        $user_transmitter = User::find($message->user_transmitter);
        $message->user_transmitter = User::prepareLazy($user_transmitter);

        if ($user->id == $user_transmitter->id):
            $message->user_transmitter->you = true;
        else:
            $message->user_receiver->you = true;
        endif;
        $utils = new Utils();
        $message->time = $utils->formatChatDate($message->created_at, $user->timezone);
        return $message;
    }

    public static function prepareChat($chat)
    {
        $token = JWTAuth::getToken();
        $user = JWTAuth::toUser($token);
        $chat->user_receiver = $chat->user_receiver;
        $chat->user_transmitter = $chat->user_transmitter;
        if ($user->id == $chat->user_transmitter):
            $user_from = User::find($chat->user_transmitter);
            $chat->you_message = true;
        else:
            $chat->you_message = false;
            $user_from = User::find($chat->user_transmitter);
        endif;
        $chat->user_from = User::prepareLazy($user_from);
        $utils = new Utils();
        $chat->time = $utils->formatChatDate($chat->created_at, $user->timezone);
        return $chat;
    }

    public static function prepareChatGroup($chat){

        $user = User::find($chat->user);
        $chat->user = User::prepareLazy($user);
        $chat->last_message = self::lastMessage($chat);
        return $chat;
    }

    public static function lastMessage($chat){
        $token = JWTAuth::getToken();
        $user = JWTAuth::toUser($token);

        $last_message = Message::where(['user_receiver'=>$user->id,'user_transmitter'=>$chat->user->id])
            ->orWhere(['user_receiver'=>$chat->user->id,'user_transmitter'=>$user->id])
            ->orderByDesc('created_at')->first();
        $prepared_chat = self::prepareChat($last_message);
        return $prepared_chat;
    }
}
