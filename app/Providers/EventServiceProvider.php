<?php

namespace App\Providers;

use Illuminate\Support\Facades\Event;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        /*'App\Events\SomeEvent' => [
            'App\Listeners\EventListener',
        ],*/
        'App\Events\ListPopularities' => [
            'App\Listeners\ListPopulariesList'
        ],
        'App\Events\ListChangePopularities' => [
            'App\Listeners\ListChangePopular'
        ],
        'App\Events\ChallengeCancelUser'=>[
            'App\Listeners\CancelChallenge'
        ],
        'App\Events\EventActivitiesUser'=>[
            'App\Listeners\ActivitiesUser'
        ],
        'App\Events\EventAwardsUserList'=>[
            'App\Listeners\AwardUserList'
        ]
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();

        //
    }
}
