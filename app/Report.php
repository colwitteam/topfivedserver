<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Validator;

class Report extends Model {

    private $rules = [];
    protected $fillable = ['id', 'target', 'origin_id',
        'affect_id', 'comment'];
    protected $hidden = ['created_at', 'updated_at'];
    private $errors;

    public function validate($data) {

        $this->rules = [
            'target' => 'numeric|required',
            'origin_id' => 'numeric|required',
            'affect_id' => 'numeric|required'
        ];

        $v = Validator::make($data, $this->rules);

        if ($v->fails()) {
            $this->errors = $v->errors()->toArray();
            return false;
        }

        return true;
    }

    public function errors() {
        return $this->errors;
    }

    public static function preparedReport($report) {

        $user = User::find($report->origin_id);
        $report->target = $report->target == 1 ? 'List' : 'User';
        $report->origin_id = User::prepareLazy($user);

        return $report;
    }

}
