<?php

namespace App;

use App\Listeners\ListPopulariesList;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Top;
use JWTAuth;

class Search extends Model
{

    use SoftDeletes;
    protected $softDelete = true;
    protected $dates = ['deleted_at'];
    protected $fillable = ['id', 'user_id', 'term', 'target', 'searched_item_id'];
    protected $hidden = ['created_at', 'updated_at', 'deleted_at'];
    private $rules = [];

    public function validate($data)
    {

        $required = '';

        if (array_key_exists('target', $data) and !empty($data['target'])):
            $required = ($data['target'] == 1 or $data['target'] == 2 or $data['target'] == 3) ? '|required' : '';
        endif;

        $this->rules = [
            'term' => 'required',
            'target' => 'numeric|required',
            'user_id' => 'numeric|required',
            'searched_item_id' => 'numeric' . $required
        ];

        $v = Validator::make($data, $this->rules);

        if ($v->fails()) {

            $this->errors = $v->errors()->toArray();
            return false;
        }

        return true;
    }

    public function errors()
    {
        return $this->errors;
    }

    public function scopeMoreSearched($query, $target, $followings, $user)
    {
        $query->select('searches.user_id','searches.term', 'searches.searched_item_id', 'searches.target', DB::raw('count(searches.searched_item_id) as total_search'));
        $search_followings = Search::select('searches.user_id','searches.term', 'searches.searched_item_id', 'searches.target', DB::raw('count(searches.searched_item_id) as total_search'));
        if ($target == 1):
            $query->join('lists', 'lists.id', '=', 'searches.searched_item_id');
            $query->join('users', 'users.id', '=', 'lists.user_id');
            $query->where('lists.is_private', 0);
            $query->where('lists.enable', 1);
            $query->whereNotIn('lists.user_id', HiddenUser::select('hidden_users.user_id as user_id')
                ->where('hidden_users.user_hidden_id', '=', $user)->get());


            $search_followings->join('lists', 'lists.id', '=', 'searches.searched_item_id');
            $search_followings->join('users', 'users.id', '=', 'lists.user_id');
            $search_followings->where('lists.is_private', 0);
            $search_followings->where('lists.enable', 1);
            $search_followings->whereNotIn('lists.user_id', HiddenUser::select('hidden_users.user_id as user_id')
                ->where('hidden_users.user_hidden_id', '=', $user)->get());

        elseif ($target == 2):
            $query->join('users', 'users.id', '=', 'searches.searched_item_id');
            $query->whereNotIn('users.id', HiddenUser::select('hidden_users.user_id as user_id')
                ->where('hidden_users.user_hidden_id', '=', $user)->get());

            $search_followings->join('users', 'users.id', '=', 'searches.searched_item_id');
            $search_followings->whereNotIn('users.id', HiddenUser::select('hidden_users.user_id as user_id')
                ->where('hidden_users.user_hidden_id', '=', $user)->get());
        elseif ($target == 3 || $target == 4):
            $query->join('users', 'users.id', '=', 'searches.user_id');
            $search_followings->join('users', 'users.id', '=', 'searches.user_id');
        elseif ($target == 5):
            $query->join('users', 'users.id', '=', 'searches.user_id');
            /*$query->join('hashtags', 'hashtags.id', '=', 'searches.searched_item_id');*/
            $search_followings->join('users', 'users.id', '=', 'searches.searched_item_id');
           /* $tops_populars = Top::select(DB::raw('tops.name as term'), DB::raw('tops.id as searched_item_id'), DB::raw('if(tops.id>0,5,3) as target'), DB::raw('count(list_tops.top_id) as total_search'))
                ->join('list_tops', 'list_tops.top_id', '=', 'tops.id')
                ->join('lists', 'lists.id', '=', 'list_tops.list_id')
                ->join('list_popularities', 'list_popularities.list_id', '=', 'lists.id')
                ->whereNotIn('lists.user_id',
                    HiddenUser::select('hidden_users.user_id as user_id')
                        ->where('hidden_users.user_hidden_id', '=', $user)->get()
                )
                ->skip(10)
                ->take(5)
                ->where('lists.enable', 1)
                ->groupBy('tops.id');*/
            /*$query->unionAll($tops_populars);*/
        else:

            $query->join('users', 'users.id', '=', 'searches.user_id');
            $search_followings->join('users', 'users.id', '=', 'searches.searched_item_id');

        endif;

        $query->where('users.banned', 0);
        $query->where('searches.user_id', $user);
        if(isset($target)):
            $query->where('searches.target', $target);
            $search_followings->where('searches.target', $target);
        endif;
        $query->groupBy('searches.user_id','searches.term');
        $query->orderByDesc('total_search', 'DESC');

        $search_followings->where('users.banned', 0);
        $search_followings->whereIn('searches.user_id', $followings);
        $search_followings->groupBy('searches.user_id','searches.term');
        $search_followings->orderByDesc('searches.created_at');
        $query->union($search_followings);


    }

    public static function prepareSearch($search)
    {
        $token = JWTAuth::getToken();
        $user = JWTAuth::toUser($token);

        if($user->id==$search->user_id):
            $search->search_for_user = true;
        else:
            $search->search_for_user = false;
        endif;
        if ($search->target == 1):
            $list = UserList::select('lists.*')
                ->join('users', 'users.id', '=', 'lists.user_id')
                ->where('users.banned', 0)
                ->where('users.deleted_at', NULL)
                ->where('lists.id', $search->searched_item_id)
                ->where('lists.is_private', 0)
                ->where('lists.enable', 1)->first();
            if ($list):
                $search->search_item = UserList::prepareList($list);

            endif;
            $search->search_item_type = 'list';
            //
        endif;

        if ($search->target == 2):
            $user = User::find($search->searched_item_id);
            if ($user):
                $search->search_item = User::prepareLazy($user);
            endif;
            $search->search_item_type = 'user';
        endif;

        if ($search->target == 3):
            $hashtag = Hashtag::find($search->searched_item_id);
            if ($hashtag):
                $search->search_item = $hashtag;
            endif;
            $search->search_item_type = 'hashtag';
        endif;
        if ($search->target == 4):

            $search->search_item_type = 'location';
        endif;

        if ($search->target == 5):
            $top = Top::find($search->searched_item_id);
            if ($top):
                $search->search_item = $top;
            endif;
            $search->search_item_type = 'top';
        endif;

        return $search;
    }

}
