<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Term extends Model
{
    protected $fillable = ['id','terms','kind'];
    protected $hidden = ['created_at', 'updated_at'];
}
