<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Validator;

class Top extends Model {

    private $rules = [];
    private $errors;
    protected $fillable = ['name'];
    protected $hidden = ['created_at', 'updated_at','deleted_at'];

    public function validate($data) {
        $this->rules = [
            'name' => 'required'
        ];
        // make a new validator object
        $v = Validator::make($data, $this->rules);

        // check for failure
        if ($v->fails()) {
            // set errors and return false
            $this->errors = $v->errors()->toArray();
            return false;
        }

        // validation pass
        return true;
    }

    public function errors() {
        return $this->errors;
    }

    public static function getTops($listTops){
        $top=[];
        foreach($listTops as $listTop){
            $top[]= [
                'id'=>$listTop->id,
                'order'=>$listTop->order,
                'name'=>$listTop->name
            ];
        }
        return $top;
    }

}
