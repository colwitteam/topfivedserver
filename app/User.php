<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\Validator;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Helpers\RequestCurl;
use Illuminate\Support\Facades\DB;
use JWTAuth;
use Mockery\Exception;
use app\Helpers\Utils;

class User extends Authenticatable {

    use Notifiable;
    use SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'firstname', 'lastname', 'email', 'birthdate', 'sex', 'profile_pic', 'username',
        'password', 'fb_id', 'twitter_id', 'google_id', 'latitude', 'longitude', 'description',
        'timezone', 'token_android', 'address', 'city', 'country', 'quote', 'url', 'number', 'country', 'city'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token', 'created_at', 'updated_at', 'deleted_at'
    ];
    protected $dates = ['deleted_at'];
    private $rules = [];
    private $errors;

    public function _cleanupData($data) {

        if (array_key_exists('username', $data) and empty($data['username'])):
            unset($data['username']);
        endif;

        if (array_key_exists('email', $data) and empty($data['email'])):
            unset($data['email']);
        endif;

        if (array_key_exists('firstname', $data) and empty($data['firstname'])):
            unset($data['firstname']);
        endif;

        if (array_key_exists('password', $data) and empty($data['password'])):
            unset($data['password']);
        endif;

        if (array_key_exists('profile_pic', $data) and empty($data['profile_pic'])):
            unset($data['profile_pic']);
        endif;

        if (array_key_exists('sex', $data) and empty($data['sex'])):
            unset($data['sex']);
        endif;

        if (array_key_exists('birthdate', $data) and empty($data['birthdate'])):
            unset($data['birthdate']);
        endif;

        if (array_key_exists('fb_id', $data) and empty($data['fb_id'])):
            unset($data['fb_id']);
        endif;

        if (array_key_exists('twitter_id', $data) and empty($data['twitter_id'])):
            unset($data['twitter_id']);
        endif;

        if (array_key_exists('google_id', $data) and empty($data['google_id'])):
            unset($data['google_id']);
        endif;

        if (array_key_exists('latitude', $data) and empty($data['latitude'])):
            unset($data['latitude']);
        endif;

        if (array_key_exists('longitude', $data) and empty($data['longitude'])):
            unset($data['longitude']);
        endif;

        if (array_key_exists('token_android', $data) and empty($data['token_android'])):
            unset($data['token_android']);
        endif;

        if (array_key_exists('city', $data) and empty($data['city'])):
            unset($data['city']);
        endif;

        if (array_key_exists('country', $data) and empty($data['country'])):
            unset($data['country']);
        endif;

        return $data;
    }

    public function validate($data) {
        $data = $this->_cleanupData($data);

        $this->rules = [
            'firstname' => 'regex:/^[\pL\s\-]+$/u',
            'lastname' => 'regex:/^[\pL\s\-]+$/u',
            'email' => 'required|email|unique:users,email,' . ($this->id ? $this->id : ''),
            'username' => 'required|unique:users,username,' . ($this->id ? $this->id : ''),
            'password' => 'required',
            'sex' => 'numeric',
            'birthdate' => 'numeric',
            'fb_id' => 'unique:users,fb_id,',
            'google_id' => 'unique:users,google_id,',
            'twitter_id' => 'unique:users,twitter_id,',
            'latitude' => 'required|numeric',
            'longitude' => 'required|numeric',
            'token_android' => 'required|unique:users,token_android',
            'url' =>  isset($data['url'])? 'regex:#[-a-zA-Z0-9@:%_\+.~\#?&//=]{2,256}\.[a-z]{2,4}\b(\/[-a-zA-Z0-9@:%_\+.~\#?&//=]*)?#si':''
        ];
        // make a new validator object
        $v = Validator::make($data, $this->rules);

        // check for failure
        if ($v->fails()) {
            // set errors and return false
            $this->errors = $v->errors()->toArray();
            return false;
        }

        // validation pass
        return true;
    }

    public function validateUsername($data) {
        $rules = [
            'username' => 'required|unique:users',
        ];
        // make a new validator object
        $v = Validator::make($data, $rules);

        // check for failure
        if ($v->fails()) {
            // set errors and return false
            $this->errors = $v->errors()->toArray();
            return false;
        }

        // validation pass
        return true;
    }

    public function validateUpdate($data) {

        $this->rules = [
            'email' => 'email|unique:users,email,' . ($this->id ? $this->id : ''),
            'username' => 'unique:users,username,' . ($this->id ? $this->id : ''),
            'firstname' => 'regex:/^[\pL\s\-]+$/u',
            'lastname' => 'regex:/^[\pL\s\-]+$/u',
            'sex' => 'numeric',
            'fb_id' => 'unique:users,fb_id,',
            'google_id' => 'unique:users,google_id,',
            'twitter_id' => 'unique:users,twitter_id,',
            'latitude' => (isset($data['longitude']) && array_key_exists('longitude', $data) ? 'required|' : '') . 'numeric',
            'longitude' => (isset($data['latitude']) && array_key_exists('latitude', $data) ? 'required|' : '') . 'numeric',
            'token_android' => 'unique:users,token_android,',
            'url' => isset($data['url'])? 'regex:#[-a-zA-Z0-9@:%_\+.~\#?&//=]{2,256}\.[a-z]{2,4}\b(\/[-a-zA-Z0-9@:%_\+.~\#?&//=]*)?#si':''
        ];
        // make a new validator object
        $v = Validator::make($data, $this->rules);

        // check for failure
        if ($v->fails()) {
            // set errors and return false
            $this->errors = $v->errors()->toArray();
            return false;
        }

        // validation pass
        return true;
    }

    public function validateStatus($data) {

        $this->rules = [
            'status' => 'required'
        ];

        // make a new validator object
        $v = Validator::make($data, $this->rules);

        // check for failure
        if ($v->fails()) {
            // set errors and return false
            $this->errors = $v->errors()->toArray();
            return false;
        }

        // validation pass
        return true;
    }

    public function errors() {
        return $this->errors;
    }

    public static function getImageProfile($user) {
        $image = null;
        $curl_image = new RequestCurl();
        if ($user->profile_pic):
            $image = $user->profile_pic;
        elseif ($user->fb_id):
            $image = 'http://graph.facebook.com/' . $user->fb_id . '/picture?type=large';
        elseif ($user->google_id):
            $apiKey = config('app.google_api.api_key');
            $url_json = 'https://www.googleapis.com/plus/v1/people/' . $user->google_id . '?fields=image&key=' . $apiKey;
            $image_google = $curl_image->requestUrl($url_json, 'get');
            $image = $image_google['image']['url'];
        elseif ($user->twitter_id):
            $image = 'https://twitter.com/' . $user->username . '/profile_image?size=original';
        else:
            $image = env('APP_URL') . 'images/pic_default.png';
        endif;
        return $image;
    }

    public static function preparedUser($user,$addList=false) {

        $token = JWTAuth::getToken();
        $user_authenticate = JWTAuth::toUser($token);

        $followings = UserFollow::select(DB::raw('count(follows_to) as totalfollowing'))->where(['user_id' => $user->id])->groupBy('user_id')->first();
        $followers = UserFollow::select(DB::raw('count(follows_to) as totalfollowers'))->where(['follows_to' => $user->id])->groupBy('follows_to')->first();
        $user->followings = (empty($followings['totalfollowing']) ? 0 : $followings['totalfollowing']);
        $user->followers = (empty($followers['totalfollowers']) ? 0 : $followers['totalfollowers']);
        $user->following = UserFollow::where(['user_id' => $user_authenticate->id, 'follows_to' => $user->id])->first() ? true : false;
        $totalPost = UserList::select(DB::raw('count(user_id) as totalpost'))->where(['user_id' => $user->id])->groupBy('user_id')->first();

        $user->totalPost = $totalPost['totalpost'];
        $users_followers = User::select('users.username', 'users.id')->join('user_follows', 'user_follows.user_id', '=', 'users.id')->where('user_follows.follows_to', $user->id)->skip(0)->take(3)->get();
        $follower_users = '';
        $user->profile_pic = self::getImageProfile($user);
        foreach ($users_followers as $follower_user):
            $follower_users .= $follower_user->username ."{".$follower_user->id.  "},";
        endforeach;
        $follower_users = rtrim($follower_users, ',');
        if ($followers['totalfollowers'] > 3):
            $follower_users .= " +" . ($followers['totalfollowers'] - 3);
        endif;
        $user->summary_followers = $follower_users;
        $user->url = $user->url;
        $user->quote = $user->quote;
        $lists_user = [];
        if($addList == true) {

            if ($user->private == 0):

                $obje_list = UserList::where(['user_id' => $user->id])->orderByDesc('created_at')->get();
                foreach ($obje_list as $list):
                    $user_list = UserList::preparedListLazy($list);
                    if ($user_list):
                        $lists_user[] = $user_list;
                    endif;
                endforeach;
            else:
                if ($user_authenticate->id != $user->id):
                    $followers = UserFollow::where(['user_id' => $user_authenticate->id, 'follows_to' => $user->id])->first();
                    if ($followers):
                        $obje_list = UserList::where(['user_id' => $user->id, 'is_private' => 0])->orderByDesc('created_at')->get();
                        foreach ($obje_list as $list):
                            $user_list = UserList::preparedListLazy($list);
                            if ($user_list):
                                $lists_user[] = $user_list;
                            endif;
                        endforeach;
                    endif;
                else:
                    $obje_list = UserList::where(['user_id' => $user->id])->orderByDesc('created_at')->get();
                    foreach ($obje_list as $list):
                        $user_list = UserList::preparedListLazy($list);
                        if ($user_list):
                            $lists_user[] = $user_list;
                        endif;
                    endforeach;
                endif;
            endif;
        }
        $user->list_users = $lists_user;

        return $user;
    }

    public static function returnImageUser($user) {
        $user->profile_pic = self::getImageProfile($user);
        return $user;
    }

    public static function prepareLazy($user) {

        $userfind = new User();
        $userfind->id = $user->id;
        $userfind->firstname = $user->firstname;
        $userfind->lastname = $user->lastname;
        $userfind->username = $user->username;
        $userfind->profile_pic = self::getImageProfile($user);
        $userfind->private = $user->private;
        return $userfind;
    }

    public static function prepareLazyReferences($user) {
        $user_find = User::find($user->id);
        $user->id = $user->id;
        $user->firstname = $user_find->firstname;
        $user->lastname = $user_find->lastname;
        $user->profile = self::getImageProfile($user_find);
        return $user;
    }

    public function extendsTokenFacebook($token_facebook) {
        $request_curl = new RequestCurl();
        $params = ["client_id" => config('app.facebook.app_id'), "client_secret" => config('app.facebook.app_secret'), "grant_type" => "fb_exchange_token", "fb_exchange_token" => $token_facebook];
        $token = $request_curl->requestUrl("https://graph.facebook.com/oauth/access_token", "get", $params);
        return $token['access_token'];
    }

    public function setLocation($data) {

        $helper = new Utils();
        $location = $helper->getGeocodeLocation($data['latitude'], $data['longitude']);

        if (count($location) > 0):
            $data['address'] = $location['address'];
            $data['city'] = $location['city'];
            $data['country'] = $location['country'];
        endif;

        return $data;
    }

    public function scopeGetUserFollows($query, $user_id, $flag, $term = null) {
        $token = JWTAuth::getToken();
        $user = JWTAuth::toUser($token);

        $query->select('users.id as id', 'users.firstname', 'users.lastname', 'users.username')
                ->join('user_follows', 'users.id', '=', 'user_follows.follows_to')
                ->where(['user_follows.user_id' => $user_id]);
        if ($term):
            $query->where(function ($query) use ($term) {
                $query->orWhere('users.firstname', 'like', "{$term}%");
                $query->orWhere('users.lastname', 'like', "{$term}%");
                $query->orWhere('users.username', 'like', "{$term}%");
            });

        endif;

        $query->where('banned', '=', 0);
        $query->whereNotIn('users.id', HiddenUser::select('user_id')->where('user_hidden_id', '=', $user->id)->get());

        $query->groupBy('users.id')
                ->orderBy($flag ? DB::raw('RAND()') : 'users.id', 'ASC');
    }

    public function scopeGetUserFollowers($query, $user_id, $flag, $term = null) {
        $token = JWTAuth::getToken();
        $user = JWTAuth::toUser($token);

        $query->select('users.id as id', 'users.firstname', 'users.lastname', 'users.username')
                ->join('user_follows', 'users.id', '=', 'user_follows.user_id')
                ->where(['user_follows.follows_to' => $user_id]);
        if ($term):
            $query->where(function ($query) use ($term) {
                $query->orWhere('users.firstname', 'like', "{$term}%");
                $query->orWhere('users.lastname', 'like', "{$term}%");
                $query->orWhere('users.username', 'like', "{$term}%");
            });
        endif;

        $query->where('banned', '=', 0);

        $query->whereNotIn('users.id', HiddenUser::select('user_id')->where('user_hidden_id', '=', $user->id)->get());
        $query->groupBy('users.id')
                ->orderBy($flag ? DB::raw('RAND()') : 'users.id', 'ASC');
    }

    public function scopeGetFilterUser($query, $request) {
        $query->select("users.*");
        if ($request):            
             if($request->username):
                $query->where(function ($query) use ($request) {
                   $query->where('users.username', 'like', '%'. $request->username.'%'); 
                });
            endif;
            
            if ($request->firstname):
                $query->where(function ($query) use ($request) {
                    $query->where('users.firstname', 'like', '%'.$request->firstname.'%');
                });
            endif;
            
            if($request->lastname):
                $query->where(function ($query) use ($request) {
                    $query->where('users.lastname', 'like', '%'. $request->lastname.'%');
                });
            endif;
            
            if($request->email):
                $query->where(function ($query) use ($request) {
                    $query->where('users.email', 'like', '%'. $request->email.'%');
                });
            endif;
        endif;
        $query->where('users.role', '!=', 2);
        $query->orderBy('users.id', 'DESC');
    }

    public function scopeGetUserByTerm($query, $term) {
        $query->select('users.*');
        if ($term):
            $query->where(function ($query) use ($term) {
                $query->orWhere('users.username', 'like', "{$term}");
                $query->orWhere('users.firstname', 'like', "{$term}");
                $query->orWhere('users.lastname', 'like', "{$term}");
            });
        endif;
    }

    public function getMyFollowingsActive(){
        $hidde_users = HiddenUser::select('hidden_users.user_id as user_id')->where('hidden_users.user_hidden_id', '=', $this->id)->get();

        $followings = UserFollow::select('follows_to')
            ->join('users','users.id','=','user_follows.follows_to')
            ->where('user_follows.user_id',$this->id)
            ->whereNotIn('follows_to', $hidde_users)
            ->where('users.banned',0)
            ->get();
        $ids = [];
        foreach ($followings as $following):
            $ids[] = $following->follows_to;
        endforeach;

        return $ids;
    }
}
