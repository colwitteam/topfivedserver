<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserAward extends Model
{
    protected $fillable = ['id', 'user_id', 'award_id', 'list_id'];
    protected $timestamp = 'true';

    public static function preparedAwards($award_user)
    {
        $award_user->award = Award::find($award_user->award_id);

        if ($award_user->award->type == 1):
            $get_lists = self::select('list_id')->where('award_id', $award_user->award_id)->where('user_id', $award_user->user_id)->get();
            $lists = self::getPreparedList($get_lists);
            $award_user->lists = $lists;
        endif;
    }

    public static function getPreparedList($id_lists)
    {

        foreach ($id_lists as $id_list):
            $list = UserList::where('id', $id_list->list_id)->first();
            if($list):
                $list_prepared[] = UserList::preparedListLazy($list);
            endif;
        endforeach;
        return $list_prepared;
    }
}
