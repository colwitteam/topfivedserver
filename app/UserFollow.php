<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserFollow extends Model {

    protected $fillable = [
        'user_id', 'follows_to'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'created_at', 'updated_at'
    ];

    public static function getFollowings($user) {

        $followingList = [];

        $followings = self::where(['user_id' => $user->id])->get();

        if (!empty($followings)):
            foreach ($followings as $follow):
                $followingList [] = $follow->follows_to;
            endforeach;
        endif;

        return $followingList;
    }

    public static function setIsFollowing($object, $idUser, $idFollow) {

        $following = self::where('user_id', $idUser)
                        ->where('follows_to', $idFollow)->first();
        if ($following):
            $object->isFollowing = true;
        else:
            $object->isFollowing = false;
        endif;

        return $object;
    }

}
