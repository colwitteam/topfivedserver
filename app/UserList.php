<?php

namespace App;

use App\Events\EventAwardsUserList;
use App\Listeners\AwardUserList;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Validator;
use Illuminate\Database\Eloquent\SoftDeletes;
use JWTAuth;
use Illuminate\Support\Facades\DB;
use App\ListTop;
use App\Category;
use app\Helpers\Utils;

class UserList extends Model
{

    use SoftDeletes;

    protected $table = 'lists';
    protected $fillable = ['id', 'user_id', 'image_mini', 'name', 'comments', 'is_private', 'latitude', 'longitude', 'address', 'city', 'country'];
    protected $dates = ['deleted_at'];
    protected $hidden = ['created_at', 'updated_at', 'deleted_at'];
    private $rules = [];
    private $errors;
    private $location = '';
    protected $events = [
        'saved' => EventAwardsUserList::class
    ];

    public function validate($data)
    {

        $this->rules = [
            'user_id' => 'required',
            'name' => 'required',
            /* 'categoryLocation' => 'required', */
            /* 'hashtags' => 'required',*/
            /* 'comments' => 'required', */
            'is_private' => 'required',
            'top' => 'required',
            'latitude' => (isset($data['longitude']) && array_key_exists('longitude', $data) ? 'required|' : '') . 'numeric',
            'longitude' => (isset($data['latitude']) && array_key_exists('latitude', $data) ? 'required|' : '') . 'numeric'
        ];
        // make a new validator object
        $v = Validator::make($data, $this->rules);
        // check for failure
        if ($v->fails()) {
            // set errors and return false
            $this->errors = $v->errors()->toArray();
            return false;
        }
        // validation pass
        return true;
    }

    public function validateUpdate($data)
    {

        $this->rules = [
            'latitude' => (isset($data['longitude']) && array_key_exists('longitude', $data) ? 'required|' : '') . 'numeric',
            'longitude' => (isset($data['latitude']) && array_key_exists('latitude', $data) ? 'required|' : '') . 'numeric'
        ];

        // make a new validator object
        $v = Validator::make($data, $this->rules);

        // check for failure
        if ($v->fails()) {
            // set errors and return false
            $this->errors = $v->errors()->toArray();
            return false;
        }
        // validation pass
        return true;
    }

    public function errors()
    {

        return $this->errors;
    }

    public function scopeFilter($query, $filter)
    {
        $token = JWTAuth::getToken();
        $user = JWTAuth::toUser($token);
        $order = 'lists.created_at';
        $mode = "desc";
        if(isset($filter['order'])):
            $order = 'distance';
            $mode = 'asc';

        endif;
        $query->select("lists.*");
        $query->from(DB::raw("(" .
            "SELECT ( 6373 * acos ( cos ( radians($user->latitude) ) * cos( radians( lists.latitude ) ) * ".
            "cos( radians( lists.longitude ) - radians($user->longitude) ) + sin ( radians($user->latitude) ) * ".
            "sin( radians( lists.latitude ) ) ) ) AS `distance` ,lists.* FROM lists " .
            "WHERE lists.user_id = " . $user->id . " AND lists.enable=1 " .
            "UNION ALL " .
            "SELECT ( 6373 * acos ( cos ( radians($user->latitude) ) * cos( radians( lists.latitude ) ) * ".
            "cos( radians( lists.longitude ) - radians($user->longitude) ) + sin ( radians($user->latitude) ) * ".
            "sin( radians( lists.latitude ) ) ) ) AS `distance` ,lists.* FROM lists " .
            "INNER JOIN users ON users.id = lists.user_id " .
            "LEFT JOIN hidden_users ON users.id = hidden_users.user_hidden_id " .
            "LEFT JOIN (" .
            "SELECT users.id AS id FROM users " .
            "WHERE users.private = 0) AS user_public ON user_public.id=lists.user_id " .
            "INNER JOIN (" .
            "SELECT users.id AS id FROM users " .
            "LEFT JOIN user_follows ON user_follows.follows_to=users.id " .
            "WHERE users.private = 1 AND user_follows.follows_to IN " .
            "(SELECT user_follows.follows_to FROM user_follows WHERE user_follows.user_id=$user->id)) " .
            "AS user_private ON user_private.id=lists.user_id " .
            "WHERE lists.is_private=0 AND lists.enable=1 AND users.banned=0 " .
            "UNION ALL SELECT ( 6373 * acos ( cos ( radians($user->latitude) ) * cos( radians( lists.latitude ) ) * ".
            "cos( radians( lists.longitude ) - radians($user->longitude) ) + sin ( radians($user->latitude) ) * ".
            "sin( radians( lists.latitude ) ) ) ) AS `distance` ,lists.* FROM lists " .
            "INNER JOIN users ON users.id = lists.user_id " .
            "LEFT JOIN hidden_users ON users.id = hidden_users.user_hidden_id " .
            "INNER JOIN (" .
            "SELECT users.id AS id FROM users " .
            "WHERE users.private = 0) AS user_public ON user_public.id=lists.user_id " .
            "WHERE lists.is_private=0 AND lists.enable=1 AND users.banned=0" .
            ")lists"));

        /*  $sub_query = HiddenUser::select('hidden_users.user_id as users_hiddens')
              ->where('hidden_users.user_hidden_id', '=', $user->id);*/

        if (count($filter) > 0):

            if (!empty($filter['query'])):
                $name = trim($filter['query']);
                $query->where("lists.name", "like", "%$name%");
            endif;
            if (!empty($filter['id_user'])):
                $idUser = $filter['id_user'];
                $query->where("lists.user_id", "=", "$idUser");
            endif;
            /* if (!empty($filter['category'])):
              $category = $filter["category"];
              $query->leftJoin("list_categories", "list_categories.list_id", "=", "lists.id");
              $query->leftJoin("categories", "list_categories.category_id", "=", "categories.id");
              $query->where("categories.name", "like", "%$category%");
              endif; */
            if (!empty($filter["top"])):
                $query->leftJoin("list_tops", "list_tops.list_id", "=", "lists.id");
                $query->leftJoin("tops", "tops.id", "=", "list_tops.top_id");
                $query->where("tops.name", "like", "%{$filter["top"]}%");
            endif;
            if (!empty($filter['location'])):
                /*      $query->leftJoin('list_locations', 'list_locations.list_id', '=', 'lists.id');
                      $query->leftJoin('locations', 'locations.id', '=', 'list_locations.location_id');*/
                $query->where('lists.city', 'like', '%' . $filter['location'] . '%');
                $query->orWhere('lists.country', 'like', '%' . $filter['location'] . '%');
            endif;
            if (!empty($filter['hashtag'])):
                $query->leftJoin('list_hashtags', 'list_hashtags.list_id', '=', 'lists.id');
                $query->leftJoin('hashtags', 'hashtags.id', '=', 'list_hashtags.hashtag_id');
                $query->where('hashtags.keyword', 'like', '%' . $filter['hashtag'] . '%');
                $query->where(function ($where) {
                    $where->where('list_hashtags.comment_id', NULL);
                    $where->orWhere('list_hashtags.comment_id', 0);
                });
            endif;
        endif;
        $query->whereNotIn('lists.user_id', HiddenUser::select('hidden_users.user_id as users_hiddens')
            ->where('hidden_users.user_hidden_id', '=', $user->id)
            //->union($sub_query)
            ->get());

        $query->groupBy('lists.id');
        $query->orderBy($order, $mode);

    }

    public function scopeThemes($query, $theme)
    {
        $query->select('name', DB::raw('count(lists.name) as total'));
        $query->where('name', 'like', "%{$theme}%");
        $query->groupBy('name');
    }

    public function _cleanupData($data)
    {
        if (array_key_exists('name', $data) and empty($data['name'])):
            unset($data['name']);
        endif;

        if (array_key_exists('comments', $data) and empty($data['comments'])):
            unset($data['comments']);
        endif;

        if (array_key_exists('is_private', $data) and (empty($data['is_private']) && $data['is_private'] == '')):
            unset($data['is_private']);
        endif;

        if (array_key_exists('password', $data) and empty($data['password'])):
            unset($data['password']);
        endif;

        if (array_key_exists('latitude', $data) and empty($data['latitude'])):
            unset($data['latitude']);
        endif;

        if (array_key_exists('longitude', $data) and empty($data['longitude'])):
            unset($data['longitude']);
        endif;

        return $data;
    }

    public static function validateUserPrivate($userContext, $userList)
    {

        if ($userList->private != 0):
            if ($userList->id != $userContext->id):
                $follow = UserFollow::where('user_id', $userContext->id)
                    ->where('follows_to', $userList->id)->first();
                if ($follow):
                    return true;
                else:
                    return false;
                endif;
            else:
                return true;
            endif;
        else:
            return true;
        endif;

    }

    public static function prepareList($list)
    {
        $token = JWTAuth::getToken();
        $user = JWTAuth::toUser($token);
        $list->id = $list->id;
        $list->user_id = $list->user_id;

        if ($list->is_private == 0):
            $user_owner = User::find($list->user_id);

            if (self::validateUserPrivate($user, $user_owner) || $user->role = 2):

                $list->user_owner = User::prepareLazy($user_owner);
                $list->saved_by_user = (ListSave::where(['user_id' => $user->id, 'list_id' => $list->id])->first()) ? true : false;
                $list->list_comment = $list->comments;
                $total_like = ListFavorite::select(DB::raw('count(id) as totalLikes, list_id'))->where(['list_id' => $list->id])->groupBy('list_id')->first();
                $list->totalLikes = 0 + $total_like['totalLikes'];
                $list->like = ListFavorite::where(['user_id' => $user->id, 'list_id' => $list->id])->first();
                //search top of the list
                $list->tops = self::searchTops($list);
                /* $list->categories = Category::select('categories.id', 'categories.name')
                  ->join('list_categories', 'categories.id', '=', 'list_categories.category_id')
                  ->where(['list_categories.list_id' => $list->id])
                  ->get();

                  $list->location = Location::select('locations.id', 'locations.name')
                  ->join('list_locations', 'locations.id', '=', 'list_locations.location_id')
                  ->where(['list_locations.list_id' => $list->id])
                  ->get(); */

                $hashtags = "";

                $tags = ListHashtag::select('*')
                    ->where(['list_id' => $list->id])
                    ->where(function ($where) {
                        $where->where('list_hashtags.comment_id', NULL);
                        $where->orWhere('list_hashtags.comment_id', 0);
                    })->get();
                if (!empty($tags)):
                    $hashtags = Hashtag::getHashTag($tags);
                endif;

                $list->hashtags = $hashtags;
                $list->pic = ListImage::where(['list_id' => $list->id])->get();
                $list->video = ListVideo::where(['list_id' => $list->id])->get();
                //comments list
                $list->total_comments = ListComment::where(['list_id' => $list->id])->count();
                $list_comments = ListComment::where(['list_id' => $list->id])
                    ->orderBy('created_at', 'DESC')
                    ->skip(0)->take(3)->get();
                $comments = [];
                foreach ($list_comments as $list_comment) :
                    $comments[] = ListComment::prepareComment($list_comment);
                endforeach;
                $list->comments = $comments;
            endif;
        else:
            if ($user->id == $list->user_id || $user->role = 2):
                $user_owner = User::find($list->user_id);
                $list->user_owner = User::prepareLazy($user_owner);
                $list->saved_by_user = (ListSave::where(['user_id' => $user->id, 'list_id' => $list->id])->first()) ? true : false;
                $list->list_comment = $list->comments;
                $total_like = ListFavorite::select(DB::raw('count(id) as totalLikes, list_id'))->where(['list_id' => $list->id])->groupBy('list_id')->first();
                $list->totalLikes = 0 + $total_like['totalLikes'];
                $list->like = ListFavorite::where(['user_id' => $user->id, 'list_id' => $list->id])->first();
                //search top of the list
                $list->tops = self::searchTops($list);
                /* $list->categories = Category::select('categories.id', 'categories.name')
                  ->join('list_categories', 'categories.id', '=', 'list_categories.category_id')
                  ->where(['list_categories.list_id' => $list->id])
                  ->get();

                  $list->location = Location::select('locations.id', 'locations.name')
                  ->join('list_locations', 'locations.id', '=', 'list_locations.location_id')
                  ->where(['list_locations.list_id' => $list->id])
                  ->get(); */

                $hashtags = "";

                $tags = ListHashtag::where(['list_id' => $list->id])->select('*')->get();
                if (!empty($tags)):
                    $hashtags = Hashtag::getHashTag($tags);
                endif;

                $list->hashtags = $hashtags;
                $list->pic = ListImage::where(['list_id' => $list->id])->get();
                $list->video = ListVideo::where(['list_id' => $list->id])->get();
                //comments list
                $list->total_comments = ListComment::where(['list_id' => $list->id])->count();
                $list_comments = ListComment::where(['list_id' => $list->id])->skip(0)->take(2)->get();
                $comments = [];
                foreach ($list_comments as $list_comment) {
                    $comments[] = ListComment::prepareComment($list_comment);
                }

            endif;
        endif;

        return $list;
    }

    public static function prepareListPublic($list)
    {
        //$token = JWTAuth::getToken();
        //$user = JWTAuth::toUser($token);
        $list->id = $list->id;
        $list->user_id = $list->user_id;

        if ($list->is_private == 0):
            $user_owner = User::find($list->user_id);

            //if (self::validateUserPrivate($user, $user_owner) || $user->role = 2):

                $list->user_owner = User::prepareLazy($user_owner);
                //$list->saved_by_user = (ListSave::where(['user_id' => $user->id, 'list_id' => $list->id])->first()) ? true : false;
                //$list->saved_by_user = false;

                //$list->list_comment = $list->comments;
                //$total_like = ListFavorite::select(DB::raw('count(id) as totalLikes, list_id'))->where(['list_id' => $list->id])->groupBy('list_id')->first();
                //$list->totalLikes = 0 + $total_like['totalLikes'];
                //$list->like = ListFavorite::where(['user_id' => $user->id, 'list_id' => $list->id])->first();
                //search top of the list
                $list->tops = self::searchTops($list);
                /* $list->categories = Category::select('categories.id', 'categories.name')
                  ->join('list_categories', 'categories.id', '=', 'list_categories.category_id')
                  ->where(['list_categories.list_id' => $list->id])
                  ->get();

                  $list->location = Location::select('locations.id', 'locations.name')
                  ->join('list_locations', 'locations.id', '=', 'list_locations.location_id')
                  ->where(['list_locations.list_id' => $list->id])
                  ->get(); */

                //$hashtags = "";

                /*$tags = ListHashtag::select('*')
                    ->where(['list_id' => $list->id])
                    ->where(function ($where) {
                        $where->where('list_hashtags.comment_id', NULL);
                        $where->orWhere('list_hashtags.comment_id', 0);
                    })->get();
                if (!empty($tags)):
                    $hashtags = Hashtag::getHashTag($tags);
                endif;*/

                /*$list->hashtags = $hashtags;
                $list->pic = ListImage::where(['list_id' => $list->id])->get();
                $list->video = ListVideo::where(['list_id' => $list->id])->get();
                //comments list
                $list->total_comments = ListComment::where(['list_id' => $list->id])->count();
                $list_comments = ListComment::where(['list_id' => $list->id])
                    ->orderBy('created_at', 'DESC')
                    ->skip(0)->take(3)->get();
                $comments = [];
                foreach ($list_comments as $list_comment) :
                    $comments[] = ListComment::prepareComment($list_comment);
                endforeach;
                $list->comments = $comments;*/
            //endif;
        else:
            if ($user->id == $list->user_id || $user->role = 2):
                $user_owner = User::find($list->user_id);
                $list->user_owner = User::prepareLazy($user_owner);
                $list->saved_by_user = (ListSave::where(['user_id' => $user->id, 'list_id' => $list->id])->first()) ? true : false;
                $list->list_comment = $list->comments;
                $total_like = ListFavorite::select(DB::raw('count(id) as totalLikes, list_id'))->where(['list_id' => $list->id])->groupBy('list_id')->first();
                $list->totalLikes = 0 + $total_like['totalLikes'];
                $list->like = ListFavorite::where(['user_id' => $user->id, 'list_id' => $list->id])->first();
                //search top of the list
                $list->tops = self::searchTops($list);
                /* $list->categories = Category::select('categories.id', 'categories.name')
                  ->join('list_categories', 'categories.id', '=', 'list_categories.category_id')
                  ->where(['list_categories.list_id' => $list->id])
                  ->get();

                  $list->location = Location::select('locations.id', 'locations.name')
                  ->join('list_locations', 'locations.id', '=', 'list_locations.location_id')
                  ->where(['list_locations.list_id' => $list->id])
                  ->get(); */

                $hashtags = "";

                $tags = ListHashtag::where(['list_id' => $list->id])->select('*')->get();
                if (!empty($tags)):
                    $hashtags = Hashtag::getHashTag($tags);
                endif;

                $list->hashtags = $hashtags;
                $list->pic = ListImage::where(['list_id' => $list->id])->get();
                $list->video = ListVideo::where(['list_id' => $list->id])->get();
                //comments list
                $list->total_comments = ListComment::where(['list_id' => $list->id])->count();
                $list_comments = ListComment::where(['list_id' => $list->id])->skip(0)->take(2)->get();
                $comments = [];
                foreach ($list_comments as $list_comment) {
                    $comments[] = ListComment::prepareComment($list_comment);
                }

            endif;
        endif;

        return $list;
    }

    public function setLocations($user, $data)
    {

        if (!array_key_exists('latitude', $data) or !array_key_exists('longitude', $data)):
            if ($user->latitude):
                $data['latitude'] = $user->latitude;
            endif;
            if ($user->longitude):
                $data['longitude'] = $user->longitude;
            endif;
            if ($user->address):
                $data['address'] = $user->address;
            endif;
            if ($user->city):
                $data['city'] = $user->city;
            endif;
            if ($user->country):
                $data['country'] = $user->country;
            endif;
        else:
            $data = $this->setLocation($data);
        endif;

        return $data;
    }

    public function setLocation($data)
    {

        $helper = new Utils();
        $location = $helper->getGeocodeLocation($data['latitude'], $data['longitude']);

        if (count($location) > 0):
            $data['address'] = $location['address'];
            $data['city'] = $location['city'];
            $data['country'] = $location['country'];
        endif;

        return $data;
    }

    public static function searchTops($list)
    {

        $listTops = ListTop::select('tops.id', 'list_tops.order', 'tops.name')
            ->join('tops', 'tops.id', '=', 'list_tops.top_id')
            ->where(['list_id' => $list->id])
            ->orderBy('order', 'asc')
            ->get();
        $searchTop = Top::getTops($listTops); //send array

        return $searchTop;
    }

    public static function searchHashtags($list)
    {

        $listHashtags = ListHashtag::select('hashtags.id', 'hashtags.keyword')
            ->join('hashtags', 'hashtags.id', '=', 'list_hashtags.hashtag_id')
            ->where(['list_id' => $list->id])
            ->get();
        $searchHashtag = Hashtag::getHashTag($listHashtags); //send array
        return $searchHashtag;
    }

    public static function preparedListLazy($list)
    {

        $token = JWTAuth::getToken();
        $user = JWTAuth::toUser($token);
        
        if($list != null):
            if ($list->is_private == 0):
                $list->list_comment = $list->comments;
                $list->tops = self::searchTops($list);
                $list->pic = ListImage::where(['list_id' => $list->id])->get();
                $total_like = ListFavorite::select(DB::raw('count(id) as totalLikes, list_id'))
                    ->where(['list_id' => $list->id])->groupBy('list_id')->first();
                $list->totalLikes = 0 + $total_like['totalLikes'];
                $list->like = ListFavorite::where(['user_id' => $user->id, 'list_id' => $list->id])->first();
                $list->saved_by_user = (ListSave::where(['user_id' => $user->id, 'list_id' => $list->id])->first()) ? true : false;

                $list_comments = ListComment::where(['list_id' => $list->id])
                    ->orderBy('created_at', 'DESC')
                    ->skip(0)->take(3)->get();
                $comments = [];
                foreach ($list_comments as $list_comment) :
                    $comments[] = ListComment::prepareComment($list_comment);
                endforeach;
                $list->comments = $comments;
                $list->total_comments = ListComment::where(['list_id' => $list->id])->count();
                return $list;
            else:
                if ($user->id == $list->user_id):
                    $list->list_comment = $list->comments;
                    $list->tops = self::searchTops($list);
                    $list->pic = ListImage::where(['list_id' => $list->id])->get();
                    $total_like = ListFavorite::select(DB::raw('count(id) as totalLikes, list_id'))
                        ->where(['list_id' => $list->id])->groupBy('list_id')->first();
                    $list->totalLikes = 0 + $total_like['totalLikes'];
                    $list->like = ListFavorite::where(['user_id' => $user->id, 'list_id' => $list->id])->first();
                    $list->saved_by_user = (ListSave::where(['user_id' => $user->id, 'list_id' => $list->id])->first()) ? true : false;

                    $list_comments = ListComment::where(['list_id' => $list->id])
                        ->orderBy('created_at', 'DESC')
                        ->skip(0)->take(3)->get();
                    $comments = [];
                    foreach ($list_comments as $list_comment) :
                        $comments[] = ListComment::prepareComment($list_comment);
                    endforeach;
                    $list->comments = $comments;
                    $list->total_comments = ListComment::where(['list_id' => $list->id])->count();
                    return $list;
                endif;
            endif;
        endif;
    }

    public static function preparedListLazyPublic($list)
    {
        
        if($list != null):
            if ($list->is_private == 0):
                //$list->list_comment = $list->comments;
                $list->tops = self::searchTops($list);
                //$list->pic = ListImage::where(['list_id' => $list->id])->get();
                /*$total_like = ListFavorite::select(DB::raw('count(id) as totalLikes, list_id'))
                    ->where(['list_id' => $list->id])->groupBy('list_id')->first();
                $list->totalLikes = 0 + $total_like['totalLikes'];
                $list->like = ListFavorite::where(['user_id' => $user->id, 'list_id' => $list->id])->first();
                $list->saved_by_user = (ListSave::where(['user_id' => $user->id, 'list_id' => $list->id])->first()) ? true : false;

                $list_comments = ListComment::where(['list_id' => $list->id])
                    ->orderBy('created_at', 'DESC')
                    ->skip(0)->take(3)->get();
                $comments = [];
                foreach ($list_comments as $list_comment) :
                    $comments[] = ListComment::prepareComment($list_comment);
                endforeach;
                $list->comments = $comments;
                $list->total_comments = ListComment::where(['list_id' => $list->id])->count();*/
                return $list;            
            endif;
        endif;
    }

    public static function preparedMiniList($list)
    {
        $miniList = new UserList();
        $miniList->name = $list->name;
        $miniList->id_list = $list->id;
        $miniList->user_id = $list->user_id;
        $miniList->tops = self::searchTops($list);
        return $miniList;
    }

    public function quantityPopulary()
    {
        $finale_quantity = 0;
        $quantity = $this::select(DB::raw('count(list_favorites.id) as total_like'), DB::raw('datediff(CURDATE(),lists.created_at) as day_current'))
            ->join('list_favorites', 'list_favorites.list_id', '=', 'lists.id')->where('lists.id', $this->id)->groupBy('lists.id')->first();
        if ($quantity):
            $total_like = isset($quantity->total_like) ? $quantity->total_like : 0;

            $finale_quantity = $total_like * (1 - (config('app.discount_like.value') * $quantity->day_current));
            if ($finale_quantity < 0):
                $finale_quantity = 0;
            endif;
        endif;
        return $finale_quantity;
    }

    public function putListPopular()
    {
        $list_populars = ListPopularity::all();

        if (count($list_populars) < 100):
            $lists = $this::select('lists.id as id')->join('list_favorites', 'list_favorites.list_id', '=', 'lists.id')
                ->where('lists.is_private', 0)
                ->whereNotExists(function ($query) {
                    $query->select(DB::raw(1))
                        ->from('list_popularities')
                        ->whereRaw('lists.id = list_popularities.list_id');
                })
                ->groupBy('lists.id')
                ->orderBy('list_favorites.created_at', 'desc')
                ->skip(0)->take(10)->get();
            $point_high = 0;
            $list_high = null;

            if ($lists):
                foreach ($lists as $list):
                    if ($point_high < $list->quantityPopulary()):
                        $point_high = $list->quantityPopulary();
                        $list_high = $list;
                    endif;
                endforeach;
                if ($list_high):
                    $list_popular = new ListPopularity();
                    $list_popular->list_id = $list_high->id;
                    $list_popular->quantity = $point_high;
                    $list_popular->save();
                endif;
            endif;
        endif;
    }

    public function actionList($type)
    {

        $token = JWTAuth::getToken();
        $user = JWTAuth::toUser($token);
        $message = '';
        $message .= 'El usuario ' . $user->username . ' ';
        switch ($type):
            case 'liked':
                $message .= 'te ha dado like a tu lista ';
                break;
            case 'follow':
                $message .= 'ha compartido tu lista ';
                break;
            case 'saved':
                $message .= 'haguardado tu lista ';
                break;
        endswitch;
        $message .= $this->name;
        return $message;
    }

    public function setAwardsNeighborhood()
    {
        $count_likes = $this->select('lists.id as id', DB::raw('count(list_favorites.list_id) as count_like'))
            ->join('list_favorites', 'list_favorites.list_id', '=', 'lists.id')
            ->where('lists.id', $this->id)
            ->whereIn('list_favorites.user_id', function ($query) {
                $milles = 5;
                $radius = ($milles * (1.60934));
                $query->select('users.id as id_user')
                    ->from('users')
                    ->where(DB::raw('ACOS( SIN( RADIANS( latitude ) ) * SIN( RADIANS( ' . $this->latitude . ') )'
                        . ' + COS( RADIANS( latitude ) )* COS( RADIANS( ' . $this->latitude . ' ) ) '
                        . '* COS( RADIANS( longitude ) - RADIANS( ' . $this->longitude . ' ) ) ) '
                        . '* 6380 '), "<", $radius)
                    ->whereRaw('users.id = list_favorites.user_id');
            })
            ->groupBy('lists.id')
            ->first();
        //dd($count_likes->toSql());
        return $count_likes;
    }

    public function setAwardsOutNeighborhood($location)
    {
        $this->location = $location;

        $count = null;

        if ($location == 'city'):
            $count = $this->getAwardsCity($location);
        else:
            $count = $this->getAwardsCountry($location);
        endif;

        return $count;
    }

    public function getAwardsCity($location)
    {
        $count_like = $this->select('lists.city as city', 'lists.country as country', DB::raw('count(list_favorites.list_id) as count_like'))
            ->join('list_favorites', 'list_favorites.list_id', '=', 'lists.id')
            ->join('users', 'users.id', '=', 'list_favorites.user_id')
            ->where('lists.' . $location, $this->city)
            ->where('lists.id', $this->id)
            ->whereIn('list_favorites.user_id', function ($query) {
                $query->select('users.id as id_user')->from('users');
                if ($this->location == 'city'):
                    $query->whereRaw('users.city = lists.city');
                else:
                    $query->whereRaw('users.country = lists.country');
                endif;
                $query->whereRaw('users.id = list_favorites.user_id');
            })
            ->groupBy('lists.id')
            ->first();

        return $count_like;
    }

    public function getAwardsCountry($location)
    {
        $count_like = $this->select('lists.city as city', 'lists.country as country', DB::raw('count(list_favorites.list_id) as count_like'))
            ->join('list_favorites', 'list_favorites.list_id', '=', 'lists.id')
            ->join('users', 'users.id', '=', 'list_favorites.user_id')
            ->where('lists.' . $location, $this->country)
            ->where('lists.id', $this->id)
            ->whereIn('list_favorites.user_id', function ($query) {
                $query->select('users.id as id_user')->from('users');
                if ($this->location == 'city'):
                    $query->whereRaw('users.city = lists.city');
                else:
                    $query->whereRaw('users.country = lists.country');
                endif;
                $query->whereRaw('users.id = list_favorites.user_id');
            })
            ->groupBy('lists.id')
            ->first();
        return $count_like;
    }

    public function getCountHashtags($quantity)
    {

        $count_category = $this->select("hashtags.id AS hashtag_id", DB::raw('count(list_hashtags.list_id) as count_like'))
            ->join('list_hashtags', 'list_hashtags.list_id', '=', 'lists.id')
            ->join('hashtags', 'hashtags.id', '=', 'list_hashtags.hashtag_id')
            ->whereIn('list_hashtags.hashtag_id', function ($query) {
                $query->select('list_hashtags.hashtag_id')->from('list_hashtags')->where('list_hashtags.list_id', $this->id);
            })
            ->groupBy('hashtags.id')
            ->having('count_like', '>=', $quantity)
            ->first();

        return $count_category;
    }

    public function scopeFilterAdmin($query, $filter)
    {

        $query->select("lists.*");

        if (count($filter) > 0):

            if (!empty($filter['name'])):
                $name = trim($filter['name']);
                $query->where("lists.name", "like", "%$name%");
            endif;
            if (!empty($filter["top"])):
                $query->leftJoin("list_tops", "list_tops.list_id", "=", "lists.id");
                $query->leftJoin("tops", "tops.id", "=", "list_tops.top_id");
                $query->where("tops.name", "like", "%{$filter["top"]}%");
            endif;
            if (!empty($filter['hashtag'])):
                $query->leftJoin('list_hashtags', 'list_hashtags.list_id', '=', 'lists.id');
                $query->leftJoin('hashtags', 'hashtags.id', '=', 'list_hashtags.hashtag_id');
                $query->where('hashtags.keyword', 'like', '%' . $filter['hashtag'] . '%');
            endif;

            if (!empty($filter['term'])):
                $term = $filter['term'];
                $query->join('users', 'users.id', '=', 'lists.user_id');
                $query->where(function ($where) use ($term) {
                    $where->where('users.firstname', 'like', $term . '%');
                    $where->orWhere('users.lastname', 'like', $term . '%');
                    $where->orWhere('users.username', 'like', $term . '%');
                });
            endif;
        endif;
        //  $query->groupBy('lists.id');
        $query->orderBy('lists.created_at', 'desc');
    }

}
