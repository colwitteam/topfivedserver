<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id',true);
            $table->string('email')->nullable(true);
            $table->string('username')->nullable(true);
            $table->string('password')->nullable(true);
            $table->string('firstname');
            $table->string('profile_pic')->nullable(true);
            $table->string('lastname');
            $table->integer('sex')->nullable(true);
            $table->biginteger('birthdate');
            $table->string('fb_id')->nullable(true);
            $table->string('twitter_id')->nullable(true);
            $table->string('google_id')->nullable(true);           
            $table->double('latitude');
            $table->double('longitude');
            $table->string('country')->nullable(true);
            $table->string('city')->nullable(true);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    public function down()
    {
        Schema::drop('users');
    }
}
