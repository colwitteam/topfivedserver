<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserFollowsTable extends Migration
{
    public function up()
    {
        Schema::create('user_follows', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id');
            $table->integer('follows_to');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('user_follows');
    }
}
