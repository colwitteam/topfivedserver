<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateListsTable extends Migration
{
    public function up()
    {
        Schema::create('lists', function (Blueprint $table) {
            $table->increments('id', true);
            $table->integer('user_id')->unsigned();
            $table->string('name');
            $table->string('image_mini')->nullable(true);
            $table->longText('comments');
            $table->integer('is_private');            
            $table->double('latitude')->nullable(true);
            $table->double('longitude')->nullable(true);                 
            $table->timestamps();
            $table->softDeletes();
        });
    }

    public function down()
    {
        Schema::drop('lists');
    }
}
