<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTermsTable extends Migration
{
    public function up()
    {
        Schema::create('terms', function (Blueprint $table) {
            $table->increments('id');
            $table->string('term');
            $table->string('kind'); //can be hashtag or term
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('terms');
    }
}
