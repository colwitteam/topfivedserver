<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateListFavoritesTable extends Migration
{
    public function up()
    {
        Schema::create('list_favorites', function (Blueprint $table) {
            $table->increments('id',true);
            $table->integer('user_id')->unsigned();
            $table->integer('list_id')->unsigned();
            $table->timestamps();

        });
    }

    public function down()
    {
        Schema::drop('list_favorites');
    }
}
