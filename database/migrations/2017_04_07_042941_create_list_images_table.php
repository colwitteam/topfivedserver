<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateListImagesTable extends Migration
{
    public function up()
    {
        Schema::create('list_images', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('list_id');
            $table->string('image');
            $table->integer('order');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('list_images');
    }
}
