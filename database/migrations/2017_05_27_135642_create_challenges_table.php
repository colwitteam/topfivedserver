<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateChallengesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('challenges', function (Blueprint $table) {
            $table->increments('id', true);
            $table->integer('owner_id')->unsigned();
            $table->integer('target_id')->unsigned();
            $table->string('image_mini')->nullable(true);
            $table->integer('owner_list_id')->unsigned();
            $table->integer('target_list_id')->unsigned()->nullable(true);            
            $table->integer('status')->default(1);
            $table->timestamp('start_date')->nullable(true);
            $table->timestamp('deadline_date');
            $table->integer('open_to')->default(1)->unsigned();
            $table->integer('winner_id')->unsigned()->nullable(true);
            $table->integer('winner_likes')->unsigned()->nullable(true);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('challenges');
    }
}
