<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSearchesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('searches', function (Blueprint $table) {
            $table->increments('id',true);
            $table->integer('user_id',false);
            $table->string('term',50);
            $table->integer('target')->comment("1: lists | 2: people | 3:topics | 4: locations");
            $table->timestamps();
            $table->index(["term"]);
            $table->index(["term","target"]);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('searches');
    }
}