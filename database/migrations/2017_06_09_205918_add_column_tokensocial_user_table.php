<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnTokensocialUserTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->longText("token_google");
            $table->longText("token_twitter");
            $table->longText("secrect_twitter");
            $table->longText("token_facebook");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn("token_google");
            $table->dropColumn("token_twitter");
            $table->dropColumn("secrect_twitter");
            $table->dropColumn("token_facebook");
        });
    }
}
