<?php

use Illuminate\Database\Seeder;

class AwardsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('awards')->insert([
            'name' => 'Alderman',
            'level' => 'alderman',
            'quantity' => 500,
            'type' => 1,
            'location' => 'city',
            'topic_quantity'=>500
        ]);

        DB::table('awards')->insert([
            'name' => 'Mayor',
            'level' => 'mayor',
            'quantity' => 2000,
            'type' => 1,
            'location' => 'city',
            'topic_quantity'=>2000
        ]);

        DB::table('awards')->insert([
            'name' => 'President',
            'level' => 'president',
            'quantity' => 25000,
            'type' => 1,
            'location' => 'country',
            'topic_quantity'=>25000
        ]);


        DB::table('awards')->insert([
            'name' => 'King',
            'level' => 'King',
            'quantity' => 50000,
            'type' => 1,
            'location' => 'country',
            'topic_quantity'=>50000
        ]);

        DB::table('awards')->insert([
            'name' => 'power badge',
            'level' => 'badge_for_lists',
            'quantity' => 20,
            'type' => 2,
            'location' => '',
            'topic_quantity'=>0
        ]);
    }
}
