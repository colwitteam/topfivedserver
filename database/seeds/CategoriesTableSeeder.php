<?php

use Illuminate\Database\Seeder;
use App\Category;

class CategoriesTableSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        $cat = new Category();
        $categories = [['name'=>'Default'],['name'=>'Food'],['name'=>'Entertainment']];

        foreach ($categories as $category):
            if ($cat->validate($category)):
                Category::create($category);
            endif;
        endforeach;
    }

}
