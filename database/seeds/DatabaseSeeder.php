<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(CategoriesTableSeeder::class);
        $this->call(LocationsTableSeeder::class);
        $this->call(TermsTableSeeder::class);
        $this->call(StatusChallengesTableSeeder::class);
        $this->call(AwardsTableSeeder::class);
        $this->call(UserAdminSeeder::class);
    }
}
