<?php

use Illuminate\Database\Seeder;
use App\Location;

class LocationsTableSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        $loc = new Location();
        $locations = [
            ['name'=>'miami','latitude'=>'25.7742700','longitude'=>'-80.1936600'],
            ['name'=>'new york','latitude'=>'40.6643','longitude'=>'-73.9385'],
            ['name'=>'orlando','latitude'=>'28.5383400','longitude'=>'-81.3792400'],
            ['name'=>'washington','latitude'=>'38.8951100','longitude'=>'-77.0363700'],
            ['name'=>'texas','latitude'=>'29.3838500','longitude'=>'-94.9027000'],
            ['name'=>'new mexico','latitude'=>'20.7565000','longitude'=>'-103.4429700'],
            ['name'=>'chicago','latitude'=>'25.7742700','longitude'=>'-80.1936600'],
            ['name'=>'san francisco','latitude'=>'41.8500300','longitude'=>'-87.6500500'],
            ['name'=>'boston','latitude'=>'42.3584300','longitude'=>'-71.0597700'],
            ['name'=>'las vegas','latitude'=>'36.1749700','longitude'=>'-115.1372200']
        ];

        foreach ($locations as $location):
            if ($loc->validate($location)):
                Location::create($location);
            endif;
        endforeach;
    }

}
