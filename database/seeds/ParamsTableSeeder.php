<?php

use Illuminate\Database\Seeder;

class ParamsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
         DB::table('params')->insert([
            'name' => 'sex',
            'value' => 'male'
        ]);
         DB::table('params')->insert([
             'name' => 'sex',
             'value' => 'female'
         ]);
    }
}
