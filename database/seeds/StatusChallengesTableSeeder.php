<?php

use Illuminate\Database\Seeder;

class StatusChallengesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('challenge_status')->insert([
            'name' => 'default'
        ]);
        DB::table('challenge_status')->insert([
            'name' => 'accepted'
        ]);
        DB::table('challenge_status')->insert([
            'name' => 'rejected'
        ]);
        DB::table('challenge_status')->insert([
            'name' => 'approved'
        ]);
        DB::table('challenge_status')->insert([
            'name' => 'disapproved'
        ]);
        DB::table('challenge_status')->insert([
            'name' => 'ended'
        ]);
    }
}
