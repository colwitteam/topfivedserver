<?php

use Illuminate\Database\Seeder;

class TermsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('terms')->insert([
            'kind' => 'term',
            'term' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris elementum, ligula ac gravida efficitur, erat leo commodo dolor, a egestas ex elit sed lectus. Cras dapibus ipsum vel lacus vulputate tempor quis semper arcu. Etiam congue ante ac enim posuere euismod. Ut commodo diam at pretium facilisis. Morbi lobortis a risus id pellentesque. Aliquam at elit eget quam interdum lobortis. Integer dolor nibh, eleifend quis diam eu, ultrices blandit felis. Donec convallis mattis lorem eu dignissim. Donec non quam ac eros lobortis blandit. Suspendisse metus lacus, lacinia non pharetra vel, ornare id risus. Etiam eget justo sed urna lobortis volutpat. Praesent pulvinar leo vitae maximus mollis. Cras volutpat nisl quis ipsum ullamcorper, eu pellentesque felis maximus.'
        ]);

        DB::table('terms')->insert([
            'kind' => 'conditions',
            'term' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris elementum, ligula ac gravida efficitur, erat leo commodo dolor, a egestas ex elit sed lectus. Cras dapibus ipsum vel lacus vulputate tempor quis semper arcu. Etiam congue ante ac enim posuere euismod. Ut commodo diam at pretium facilisis. Morbi lobortis a risus id pellentesque. Aliquam at elit eget quam interdum lobortis. Integer dolor nibh, eleifend quis diam eu, ultrices blandit felis. Donec convallis mattis lorem eu dignissim. Donec non quam ac eros lobortis blandit. Suspendisse metus lacus, lacinia non pharetra vel, ornare id risus. Etiam eget justo sed urna lobortis volutpat. Praesent pulvinar leo vitae maximus mollis. Cras volutpat nisl quis ipsum ullamcorper, eu pellentesque felis maximus.'
        ]);
    }
}
