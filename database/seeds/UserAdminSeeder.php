<?php

use Illuminate\Database\Seeder;

class UserAdminSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'email' => 'admin@inventiba.com',
            'username' => 'admin',
            'password'=>bcrypt('123456'),
            'firstname' => 'admin',
            'lastname' => 'inventiba',
            'profile_pic'=>null,
            'role'=>2,
            'banned' => 0,
            'private' => 0
        ]);
    }
}
