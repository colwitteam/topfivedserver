# General issue

Every request which require user interaction, must to include session token in the header request as in Authorization header.

## Important

All body messages must to be in json named data ie.:
```
{"param": "value"}
```
