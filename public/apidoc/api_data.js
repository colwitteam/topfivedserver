define({ "api": [
  {
    "type": "get",
    "url": "/user/:id",
    "title": "Fetch user data",
    "name": "GetUser",
    "group": "User",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": "<p>User id to retrieve</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": "<p>Reg Id.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "firstname",
            "description": "<p>User firstname.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "lastname",
            "description": "<p>User lastname.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "username",
            "description": "<p>User username.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "email",
            "description": "<p>User email.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "profile_pic",
            "description": "<p>URI to profile pic stored.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n  \"status\": \"ok\",\n  \"data\": [\n  \t{\n         \"id\": 1,\n         \"firstname\": \"John\",\n         \"lastname\": \"Doe\",\n         \"username\": \"JohnDoe\",\n         \"email\": \"joh@doe.com\"\n         \"profile_pic\": \"url/to/pic.jpeg\"\n     }\n  ]\n}",
          "type": "json"
        }
      ]
    },
    "description": "<p>Gets the user reg with providen id</p>",
    "version": "0.0.0",
    "filename": "app/Http/Controllers/UserController.php",
    "groupTitle": "User",
    "error": {
      "examples": [
        {
          "title": "Internal-Error:",
          "content": "HTTP/1.1 500 Server Error\n{\n  \"status\": \"fail\",\n  \"data\": []\n}",
          "type": "json"
        },
        {
          "title": "BadRequest-Error:",
          "content": "HTTP/1.1 400 Bad Request\n{\n  \"status\": \"fail\",\n  \"data\": []\n}",
          "type": "json"
        },
        {
          "title": "NotAuthorized-Error:",
          "content": "HTTP/1.1 403 Forbidden\n{\n  \"status\": \"fail\",\n  \"data\": []\n}",
          "type": "json"
        }
      ]
    }
  },
  {
    "type": "post",
    "url": "/signin",
    "title": "User login",
    "name": "PostSignin",
    "group": "User",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "json",
            "optional": false,
            "field": "credentials",
            "description": "<p>User data to login, email|username and password</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "strin",
            "optional": false,
            "field": "token",
            "description": "<p>Session token.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 201 OK\n{\n  \"status\": \"ok\",\n  \"data\": [\n  \t{\n         \"token\": \"q23iuriurb4virh4kqrhv.34thrjkh\"\n     }\n  ]\n}",
          "type": "json"
        }
      ]
    },
    "description": "<p>Creates a new entry in the users table. This endpoint does not handle the profle pic upload.</p>",
    "version": "0.0.0",
    "filename": "app/Http/Controllers/UserController.php",
    "groupTitle": "User",
    "error": {
      "examples": [
        {
          "title": "Internal-Error:",
          "content": "HTTP/1.1 500 Server Error\n{\n  \"status\": \"fail\",\n  \"data\": []\n}",
          "type": "json"
        },
        {
          "title": "BadRequest-Error:",
          "content": "HTTP/1.1 400 Bad Request\n{\n  \"status\": \"fail\",\n  \"data\": []\n}",
          "type": "json"
        }
      ]
    }
  },
  {
    "type": "post",
    "url": "/signup",
    "title": "User register",
    "name": "PostSignup",
    "group": "User",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "json",
            "optional": false,
            "field": "user",
            "description": "<p>User data to storage {firstname, lastname, email, birthdate, sex, username, password(not encrypted), profile_pic(optional base64 string)}</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "token",
            "description": "<p>Token for further interactions.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 201 OK\n{\n  \"status\": \"ok\",\n  \"data\": [\n  \t{\n         \"token\": \"sagsd1svsvsd.-msdfshfsfkasjdfhds\"\n     }\n  ]\n}",
          "type": "json"
        }
      ]
    },
    "description": "<p>Creates a new entry in the users table.</p>",
    "version": "0.0.0",
    "filename": "app/Http/Controllers/UserController.php",
    "groupTitle": "User",
    "error": {
      "examples": [
        {
          "title": "Internal-Error:",
          "content": "HTTP/1.1 500 Server Error\n{\n  \"status\": \"fail\",\n  \"data\": []\n}",
          "type": "json"
        },
        {
          "title": "BadRequest-Error:",
          "content": "HTTP/1.1 400 Bad Request\n{\n  \"status\": \"fail\",\n  \"data\": []\n}",
          "type": "json"
        }
      ]
    }
  },
  {
    "type": "put",
    "url": "/user/:id",
    "title": "Update user data",
    "name": "PutUser",
    "group": "User",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": "<p>User id to retrieve</p>"
          },
          {
            "group": "Parameter",
            "type": "User",
            "optional": false,
            "field": "user",
            "description": "<p>User data to storage [firstname, lastname, email, birthdate, sex, username, password(not encrypted), profile_pic(optional string)]</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": "<p>Reg Id.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "firstname",
            "description": "<p>User firstname.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "lastname",
            "description": "<p>User lastname.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "username",
            "description": "<p>User username.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "email",
            "description": "<p>User email.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n  \"status\": \"ok\",\n  \"data\": [\n  \t{\n         \"id\": 1,\n         \"firstname\": \"John\",\n         \"lastname\": \"Doe\",\n         \"username\": \"JohnDoe\",\n         \"email\": \"joh@doe.com\"\n         \"profile_pic\": \"url/to/pic.jpeg\"\n     }\n  ]\n}",
          "type": "json"
        }
      ]
    },
    "description": "<p>Updates data of an user. This endpoint does not handle the profle pic upload.</p>",
    "version": "0.0.0",
    "filename": "app/Http/Controllers/UserController.php",
    "groupTitle": "User",
    "error": {
      "examples": [
        {
          "title": "Internal-Error:",
          "content": "HTTP/1.1 500 Server Error\n{\n  \"status\": \"fail\",\n  \"data\": []\n}",
          "type": "json"
        },
        {
          "title": "BadRequest-Error:",
          "content": "HTTP/1.1 400 Bad Request\n{\n  \"status\": \"fail\",\n  \"data\": []\n}",
          "type": "json"
        },
        {
          "title": "NotAuthorized-Error:",
          "content": "HTTP/1.1 403 Forbidden\n{\n  \"status\": \"fail\",\n  \"data\": []\n}",
          "type": "json"
        }
      ]
    }
  }
] });
