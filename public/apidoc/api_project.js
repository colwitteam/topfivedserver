define({
  "name": "TOPFIVED",
  "version": "0.0.0",
  "description": "Top five lists.",
  "title": "Topfived.",
  "url": "topfived.com/api",
  "header": {
    "title": "Intro",
    "content": "<h1>General issue</h1>\n<p>Every request which require user interaction, must to include session token in the header request as in Authorization header.</p>\n<h2>Important</h2>\n<p>All body messages must to be in json named data ie.:</p>\n<pre><code>{&quot;param&quot;: &quot;value&quot;}\n</code></pre>\n"
  },
  "template": {
    "forceLanguage": "en",
    "withCompare": false,
    "withGenerator": false
  },
  "sampleUrl": false,
  "defaultVersion": "0.0.0",
  "apidoc": "0.3.0",
  "generator": {
    "name": "apidoc",
    "time": "2017-04-20T05:07:53.114Z",
    "url": "http://apidocjs.com",
    "version": "0.17.5"
  }
});
