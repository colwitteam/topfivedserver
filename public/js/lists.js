$(document).ready(function () {
    var show_modal = true;


    $("td.list-row").click(function () {
        if(show_modal){
            $("#myModal").modal("show");
            var list = {
                'id': $(this).attr('data-id')
            };
            $.ajax({
                url: URL_API + 'admin/list/' + list.id,
                method: 'get',
                dataType: 'JSON',
                async: false,
                type: 'get',
                success: function (response) {
                    var div_list = container_list(response.data);
                    $(".modal-body").html(div_list);
                },
                beforeSend: function (xhr) {
                    xhr.setRequestHeader("Authorization", 'Bearer ' + AUTH)
                }
            });
        }
    });

    $(".change-status").click(function () {
        var button = $(this);
        var status = $(this).attr('data-status');
        var id = $(this).attr('data-id');
        var data_list = {
            'id':id,
            'status':status
        };
        sendEnabled(data_list,button);
    });

    $('#button-status').click(function () {
        var button = $(this);
        var status = $(this).attr('data-status');
        var id = $(this).attr('data-id');
        var data_list = {
            'id':id,
            'status':status
        };
        sendEnabled(data_list,button);

    });



    var container_list = function (response) {

        var table = "<div class='row justify-content-center font-modal'>"+
            "<div class='col-5'>"+
            "<label class='mb-1 label-modal font-weight-bold'>Id:</label>"+
            "</div>"+
            "<div class='col-7'>"+
            response.id +
            "</div>"+
            "<div class='col-5'>"+
            "<label class='mb-1 label-modal font-weight-bold'>Name:</label>"+
            "</div>"+
            "<div class='col-7'>"+
            response.name +
            "</div>"+
            "<div class='col-5'>"+
            "<label class='mb-1 label-modal font-weight-bold'>Location:</label>"+
            "</div>"+
            "<div class='col-7'>"+
            response.city +' '+response.country +
            "</div>"+
            "<div class='col-5'>"+
            "<label class='mb-1 label-modal font-weight-bold'>Address:</label>"+
            "</div>"+
            "<div class='col-7'>"+
            response.address+
            "</div>"+
            "<div class='col-5'>"+
            "<label class='mb-1 label-modal font-weight-bold'>Private:</label>"+
            "</div>"+
            "<div class='col-7'>"+
            isPrivate(response.is_private)+
            "</div>"+
            "<div class='col-5'>"+
            "<label class='mb-1 label-modal font-weight-bold'>Banned:</label>"+
            "</div>"+
            "<div class='col-7'>"+
            (response.enable != 2 ? "Not" : "Yes" ) +
            "</div>"+
            "<div class='col-5'>"+
            "<label class='mb-1 label-modal font-weight-bold'>Name:</label>"+
            "</div>"+
            "<div class='col-7'>"+
            response.user_owner.firstname + " "+ response.user_owner.lastname +
            "</div>" +
            "<div class='col-5'>"+
            "<label class='mb-1 label-modal font-weight-bold'>Username:</label>"+
            "</div>"+
            "<div class='col-7'>"+ "@"+
            response.user_owner.username +
            "</div>" + 
            "<div class='col-5'>"+
            "<label class='mb-1 label-modal font-weight-bold'>Tops:</label>"+
            "</div>"+
            "<div class='col-7'>"+
            topslist(response.tops)+
            "</div>"+
            "<div class='col-5'>"+
            "<label class='mb-1 label-modal font-weight-bold'>Likes:</label>"+
            "</div>"+
            "<div class='col-7'>"+
            response.totalLikes+
            "</div>"+
            "<div class='col-5'>"+
            "<label class='mb-1 label-modal font-weight-bold'>Hashtags:</label>"+
            "</div>"+
            "<div class='col-7'>"+
            hashtagsList(response.hashtags)+
            "</div>"+
            "</div>"    ;
        $("h5.modal-title").text(response.name);
        disabledButton($("#button-status"),response.enable,response.id,0);
        
        return table;
    };

    var isPrivate = function (private) {
        if (private == 0) {
            return "Not";
        }
        else {
            return "Yes";
        }
    };

    var hashtagsList = function (hashtags) {
        var hashtag = "";
        $.each(hashtags,function (index,value) {            
            if(value != ""){
                hashtag+="#"+value+", ";
            }            
        });
        hashtag = hashtag.slice(0,-2);
        return hashtag;
    };

    var topslist = function (tops) {
        var top = "";
        $.each(tops,function (index,value) {
            top+=value.name+", ";
        });
        top = top.slice(0,-2);
        return top;
    };

    var disabledButton = function (button,status,id,ejecute){
        var button_parent = $("button[data-id='"+id+"'].change-status");

        setTimeout(function () {
            if(status==1){
                button.removeClass("btn-success");
                button.addClass("btn-danger");
                button.text("Disabled");
                button.attr('data-id',id);
                button.attr('data-status',2);

                button_parent.removeClass("btn-success");
                button_parent.addClass("btn-danger");
                button_parent.attr('data-status',2);
                button_parent.text("Disabled");
            }else{
                button.removeClass("btn-danger");
                button.addClass("btn-success");
                button.text("Enabled");
                button.attr('data-id',id);
                button.attr('data-status',1);

                button_parent.removeClass("btn-danger");
                button_parent.addClass("btn-success");
                button_parent.attr('data-status',1);
                button_parent.text("Enabled");
            }
            show_modal=true;
            button.prop("disabled",false);
        },ejecute);
    };

    var sendEnabled = function(data_list,button){
        button.prop("disabled",true);
        show_modal=false;
        $.ajax({
            url: URL_API + 'admin/list/' + data_list.id,
            method: 'put',
            dataType: 'JSON',
            async: false,
            type: 'put',
            data:data_list,
            success: function (response) {
                if(response) {
                    disabledButton(button, data_list.status,data_list.id,3000);
                }
            },
            beforeSend: function (xhr) {
                xhr.setRequestHeader("Authorization", 'Bearer ' + AUTH)
            }
        });
    };
});