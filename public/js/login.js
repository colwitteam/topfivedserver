/**
 * Created by chechorjuela on 18/07/17.
 */
$(document).ready(function () {

    $('.form-login').validate({
        rules: {
            email: {
                required: true,
                email: true,
            },
            password: {
                required: true
            }
        },
        messages: {
            email: {
                required: "Email is required",
                email: "Wrong email",
            },
            password: {
                required: "Password is required"
            }
        }
    });

    $(".form-login").submit(function (e) {
        e.preventDefault();
        if ($(".form-login").valid()) {
            $("button[type='submit']").prop('disabled', true);
            var data = $(this).serialize();
            $.ajax({
                url: $(this).attr('action'),
                data: data,
                method: 'post',
                dataType: 'JSON',
                async: false,
                headers: {'X-CSRF-TOKEN': $('meta[name=_token]').attr('content')},
                success: function (response) {
                    if (response.status == "ok") {
                        verifyUser(response.data);
                        setTimeout(function () {
                            $("button[type='submit']").prop('disabled', false);
                        }, 300);
                    } else {
                        $(".message-error-login").fadeIn();
                        setTimeout(function () {
                            $(".message-error-login").fadeOut(300);
                            $("button[type='submit']").prop('disabled', false);
                        }, 1500);
                    }
                },
                beforeSend: function () {

                }
            });
        }
    });

    function verifyUser(user) {
        var data_user = {
            id: user.user_id,
            token: user.token,
        };

        $.ajax({
            url: APP_URL + 'admin/verify_user_admin',
            data: data_user,
            dataType: 'JSON',
            headers: {'X-CSRF-TOKEN': $('meta[name=_token]').attr('content')},
            method: 'get',
            success: function (response) {
                if (response) {
                    window.location.reload();
                } else {
                    alert("It user not are is admin");
                }
            }
        });
    }
});