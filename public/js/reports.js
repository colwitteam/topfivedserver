$(document).ready(function () {
    var show_modal = true;

    $(".disabled_user").click(function () {
        var button = $(this);
        sendStatus(button);
    });
    $(".disabled_list").click(function () {
        var button = $(this);
        sendEnabledList(button);
    });
    $("#userModalReport").on('hidden.bs.modal', function () {
        setTimeout(function () {
            show_modal = true;
        }, 500)
    });
    $("tr.list-row.reports").click(function (e) {
        e.preventDefault();
        if (show_modal) {
            show_modal = false;
            $("#listModalReport").modal("show");
            var list = {
                'id': $(this).attr('data-id'),
                'type': 'list',
                'status':$(this).attr('data-status'),
                'name':$(this).attr('data-name')

            };
            $.ajax({
                url: URL_API + 'admin/reports/' + list.id + '/' + list.type,
                method: 'get',
                dataType: 'JSON',
                async: true,
                type: 'get',
                success: function (response) {
                    var content = user_report(response.data, list,list.name);
                    $("#listModalReport .modal-body").html(content);
                },
                beforeSend: function (xhr) {
                    xhr.setRequestHeader("Authorization", 'Bearer ' + AUTH)
                }
            });
        }
    });

    $("tr.user-row.reports").click(function (e) {
        e.preventDefault();
        if (show_modal) {
            show_modal = false;
            $("#userModalReport").modal("show");
            var user = {
                'id': $(this).attr('data-id'),
                'type': 'user',
                'status': $(this).attr('data-status')
            };
            $.ajax({
                url: URL_API + 'admin/reports/' + user.id + '/' + user.type,
                method: 'get',
                dataType: 'JSON',
                async: true,
                type: 'get',
                success: function (response) {
                    var content = user_report(response.data, user,'@'+ response.data[0].user_affect_username);
                    $("#userModalReport .modal-body").html(content);
                },
                beforeSend: function (xhr) {
                    xhr.setRequestHeader("Authorization", 'Bearer ' + AUTH)
                }
            });
        }
    });

    $(".change-status").click(function () {
        var button = $(this);
        sendStatus(button);
    });

    $('.change-status-list').click(function () {
        var button = $(this);
        sendEnabledList(button);
    });
    var user_report = function (data, object,title) {
        console.info(data);
        var content_reports = '';
        $.each(data, function (index, value) {
            var hr = '';

            if (index + 1 < data.length) {
                hr += '<hr>';
            }
            content_reports += '<div class="row row-bordered row justify-content-center font-modal">' +
                '<div class="col-12 ">' +
                '<div class="col-12 pl-0">' +
                '<div class="col-12 pl-0"><strong>Reporter :</strong> '+ value.user_origin_firstname +' '+ value.user_origin_lastname +'  @' + value.user_origin_username + '</div>' +
                '<div class="col-12 pl-0"><strong>Email :</strong> ' + value.user_origin_email + '</div>' +
                '</div>' +
                '<div>' +
                '<strong>Report description:</strong><br> ' + value.comment +
                '</div>' +
                '</div>' +
                '</div>' + hr;

        });

        if (object.type == 'user') {
            disableButton($("#button-status"), object.status, object.id, 0);
        } else {
            disableButtonList($("#button-status"), object.status, object.id, 0)
        }

        $("h5.modal-title").text("Reported: " +title);

        return content_reports;
    };

    function sendStatus(button) {
        var status = $(button).attr('data-status');
        var id = $(button).attr('data-id');
        var data = {
            'id': id,
            'status': status
        };
        changeReports(data,'user');
        button.prop("disabled", true);
        show_modal = false;
        $.ajax({
            url: URL_API + 'admin/user/' + data.id,
            method: 'put',
            dataType: 'JSON',
            async: false,
            type: 'put',
            data: data,
            beforeSend: function (xhr) {
                xhr.setRequestHeader("Authorization", 'Bearer ' + AUTH)
            },
            success: function (response) {
                if (response) {
                    disableButton(button, data.status, data.id, 3000);
                }
            }

        });
    }

    var sendEnabledList = function (button) {
        var status = $(button).attr('data-status');
        var id = $(button).attr('data-id');

        var data = {
            'id': id,
            'status': status
        };
        changeReports(data,'list');
        button.prop("disabled", true);
        show_modal = false;
        $.ajax({
            url: URL_API + 'admin/list/' + data.id,
            method: 'put',
            dataType: 'JSON',
            async: false,
            type: 'put',
            data: data,
            beforeSend: function (xhr) {
                xhr.setRequestHeader("Authorization", 'Bearer ' + AUTH)
            },
            success: function (response) {
                if (response) {
                    disableButtonList(button, data.status, data.id, 3000);
                }
            }

        });
    };

    var disableButton = function (button, status, id, timeExecute) {
        var button_parent = $("button[data-id='" + id + "'].change-status");
        var a_hrer = $("td[data-id='" + id + "'].reports");

        setTimeout(function () {
            if (status != 0) {
                button.removeClass("btn-danger");
                button.addClass("btn-success");
                button.text("Enable");
                button.attr('data-id', id);
                button.attr('data-status', 0);

                button_parent.removeClass("btn-danger");
                button_parent.addClass("btn-success");
                button_parent.text("Enable");
                button_parent.attr('data-id', id);
                button_parent.attr('data-status', 0);

                a_hrer.attr('data-status', 1);
                a_hrer.attr('data-id', id)

            } else {
                button.removeClass("btn-success");
                button.addClass("btn-danger");
                button.text("Disable");
                button.attr('data-id', id);
                button.attr('data-status', 1);

                button_parent.removeClass("btn-success");
                button_parent.addClass("btn-danger");
                button_parent.text("Disable");
                button_parent.attr('data-id', id);
                button_parent.attr('data-status', 1);

                a_hrer.attr('data-status', 0);
                a_hrer.attr('data-id', id)

            }
            show_modal = true;
            button.prop("disabled", false);
        }, timeExecute);
    }

    var disableButtonList = function (button, status, id, timeExecute) {

        var button_parent = $("button[data-id='"+id+"'].change-status-list");
        var a_hrer = $("td[data-id='" + id + "'].reports");

        setTimeout(function () {
            if (status == 1) {
                button.removeClass("btn-success");
                button.addClass("btn-danger");
                button.text("Disabled");
                button.attr('data-id', id);
                button.attr('data-status', 2);

                button_parent.removeClass("btn-success");
                button_parent.addClass("btn-danger");
                button_parent.attr('data-status', 2);
                button_parent.text("Disabled");

                a_hrer.attr('data-status', 1);
                a_hrer.attr('data-id', id)
            }else{

                button.removeClass("btn-danger");
                button.addClass("btn-success");
                button.text("Enabled");
                button.attr('data-id', id);
                button.attr('data-status', 1);

                button_parent.removeClass("btn-danger");
                button_parent.addClass("btn-success");
                button_parent.attr('data-status', 1);
                button_parent.text("Enabled");

                a_hrer.attr('data-status', 2);
                a_hrer.attr('data-id', id)
            }
            show_modal = true;
            button.prop("disabled", false);
        }, timeExecute);
    }

    var changeReports= function(object,type){
        $.ajax({
            url: URL_API + 'admin/reports/' + object.id + '/' + type,
            method: 'put',
            dataType: 'JSON',
            async: true,
            type: 'get',
            success: function (response) {

            },
            beforeSend: function (xhr) {
                xhr.setRequestHeader("Authorization", 'Bearer ' + AUTH)
            }
        });
    }
});