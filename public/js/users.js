$(document).ready(function () {

    var show_modal = true;

    $('td.user-row').click(function () {
        if (show_modal) {
            $('#userModal').modal('show');
            var id = $(this).attr('data-id');
            $.ajax({
                url: URL_API + 'admin/user/' + id,
                method: 'get',
                dataType: 'JSON',
                async: false,
                type: 'get',
                beforeSend: function (xhr) {
                    xhr.setRequestHeader("Authorization", 'Bearer ' + AUTH)
                },
                success: function (response) {                    
                    var content = containerUser(response.data);
                    $(".modal-body").html(content);
                }
            });
        }
    });

    var containerUser = function (response) {

        var table = "<div class ='row justify-content-center font-modal'>" +
                "<div class='col-12 align-center pb-3'>" + "<img src='" + response.profile_pic + "' class='avatar border-0 p-0'/></div>" +                
                "<div class='col-5'>" + "<label class='mb-1 label-modal font-weight-bold'>Id:</label>" + "</div>" +
                "<div class='col-7'>" + response.id + "</div>" +
                "<div class='col-5'>" + "<label class='mb-1 label-modal font-weight-bold'>Email: </label>" + "</div>" +
                "<div class='col-7'>" + response.email + "</div>" +
                "<div class='col-5'>" + "<label class='mb-1 label-modal font-weight-bold'>Firstname: </label>" + "</div>" +
                "<div class='col-7'>" + response.firstname + "</div>" +
                "<div class='col-5'>" + "<label class='mb-1 label-modal font-weight-bold'>Lastname: </label>" + "</div>" +
                "<div class='col-7'>" + response.lastname + "</div>" +
                "<div class='col-5'>" + "<label class='mb-1 label-modal font-weight-bold'>Username: </label>" + "</div>" +
                "<div class='col-7'>" + response.username + "</div>" +             
                "<div class='col-5'>" + "<label class='mb-1 label-modal font-weight-bold'>Location:</label>" + "</div>" +
                "<div class='col-7'>" + response.city + ' ' + response.country + "</div>" +
                "<div class='col-5'>" + "<label class='mb-1 label-modal font-weight-bold'>Address:</label>" + "</div>" +
                "<div class='col-7'>" + response.address + "</div>" +
                "<div class='col-5'>" + "<label class='mb-1 label-modal font-weight-bold'>Private:</label>" + "</div>" +
                "<div class='col-7'>" + (response.private != 1 ? "Not" : "Yes" ) + "</div>" +
                "<div class='col-5'>" + "<label class='mb-1 label-modal font-weight-bold'>Banned:</label>" + "</div>" +
                "<div class='col-7'>" + isBanned(response.banned) + "</div>" +                               
                "<div class='col-5'>" + "<label class='mb-1 label-modal font-weight-bold'>Followers:</label>" + "</div>" +
                "<div class='col-7'>" + response.followers + "</div>" +
                "<div class='col-5'>" + "<label class='mb-1 label-modal font-weight-bold'>Followings:</label>" + "</div>" +
                "<div class='col-7'>" + response.followings + "</div>" +
                "<div class='col-5'>" + "<label class='mb-1 label-modal font-weight-bold'>Lists:</label>" + "</div>" +
                "<div class='col-7'>" + getNameList(response.list_users) + "</div>" +
                "</div>";
        $("h5.modal-title").text('@'+response.username);
        disableButton($("#button-status"), response.banned, response.id);
        return table;
    };

    function getNameList(lists) {
        var list = "";
        $.each(lists, function (index, value) {
            if (index < 5) {
                list += value.name + ", ";
            }
        });
        list = list.slice(0, -2);
        return list;
    }
    ;

    $('.change-status').click(function () {
        var button = $(this);
        sendStatus(button);
    });

    $('#button-status').click(function () {
        var button = $(this);
        sendStatus(button);
    });

    function sendStatus(button) {
        var status = $(button).attr('data-status');
        var id = $(button).attr('data-id');
        var data = {
            'id': id,
            'status': status
        };
        button.prop("disabled", true);
        show_modal = false;
        $.ajax({
            url: URL_API + 'admin/user/' + data.id,
            method: 'put',
            dataType: 'JSON',
            async: false,
            type: 'put',
            data: data,
            beforeSend: function (xhr) {
                xhr.setRequestHeader("Authorization", 'Bearer ' + AUTH)
            },
            success: function (response) {
                if (response) {
                    disableButton(button, data.status, data.id, 3000);
                }
            }

        });
    }

    function disableButton(button, status, id, timeExecute) {
        var button_parent = $("button[data-id='" + id + "'].change-status");

        setTimeout(function () {
            if (status != 0) {
                button.removeClass("btn-danger");
                button.addClass("btn-success");
                button.text("Enable");
                button.attr('data-id', id);
                button.attr('data-status', 0);

                button_parent.removeClass("btn-danger");
                button_parent.addClass("btn-success");
                button_parent.text("Enable");
                button_parent.attr('data-id', id);
                button_parent.attr('data-status', 0);

            } else {
                button.removeClass("btn-success");
                button.addClass("btn-danger");
                button.text("Disable");
                button.attr('data-id', id);
                button.attr('data-status', 1);

                button_parent.removeClass("btn-success");
                button_parent.addClass("btn-danger");
                button_parent.text("Disable");
                button_parent.attr('data-id', id);
                button_parent.attr('data-status', 1);

            }
            show_modal = true;
            button.prop("disabled", false);
        }, timeExecute);
    }

    var isBanned = function (private) {
        if (private == 0) {
            return "Not";
        } else {
            return "Yes";
        }
    };

});