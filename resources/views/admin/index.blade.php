@extends('admin.layout.layout')
@section('content')
<div id="wrapper">
    <div class="row justify-content-center" id="content-login">
        <div class="col-xs-6 col-md-5">
            {!! Form::open(["route"=>"login.admin","method"=>"","class"=>'form-group center-block form-login']) !!}
            <h5 class="title-login pb-3">Please login</h5>

            <div class="form-group row">
                <label for="inputEmail3" class="sr-only">Email</label>
                <div class="col-sm-10 input-login">
                    {!! Form::text("email",null,["class"=>"form-control","placeholder"=>"Email"]) !!}
                </div>
            </div>
            <div class="form-group row">
                <label for="inputPassword3" class="sr-only">Password</label>
                <div class="col-sm-10 input-login">
                    {!! Form::password("password",["class"=>"form-control","placeholder"=>"Password"]) !!}
                </div>
            </div>
            <button type="submit" class="btn col-6 btn-primary">Login</button>
            {!! Form::close() !!}
            <div class="message-error-login" style="display: none;">
                <label class="error">Wrong email or password!</label>
            </div>
        </div>  
    </div>
</div>
@endsection
@section('scripts')
{!! Html::script('js/login.js') !!}
@endsection
