<nav class="navbar navbar-toggleable-md navbar-light bg-faded">
    <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarText"
            aria-controls="navbarText" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <a class="navbar-brand title-top" href="#">Topfived</a>
    @if(Session::get('access_token')!=null)
        <div class="collapse navbar-collapse" id="navbarText">
            <ul class="navbar-nav mr-auto">
                <li class="nav-item @if(str_replace(env('APP_URL').'admin/','',url()->current())=='lists') active @endif">
                    <a class="nav-link" href="{!! route('admin.lists') !!}">Lists</a>
                </li>
                <li class="nav-item @if(str_replace(env('APP_URL').'admin/','',url()->current())=='users') active @endif">
                    <a class="nav-link" href="{!! route('admin.users') !!}">Users</a>
                </li>
                <li class="nav-item dropdown">

                    <a class="dropdown-toggle nav-link" id="dropdownMenuButton" data-toggle="dropdown"
                       aria-haspopup="true" aria-expanded="false">
                        Reports
                    </a>
                    <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                        <a class="dropdown-item" href="{!! route('admin.reports.users') !!}">Users</a>
                        <a class="dropdown-item" href="{!! route('admin.reports.lists') !!}">Lists</a>
                    </div>
                </li>
                <li>
                     <span class="navbar-text hidden-lg-up">
                        <a href="{!! route('admin.logout') !!}">Logout</a>
                    </span>
                </li>
            </ul>
        </div>
        <span class="navbar-text hidden-md-down">
            <a href="{!! route('admin.logout') !!}">Logout</a>
        </span>
        </div>
    @endif
</nav>