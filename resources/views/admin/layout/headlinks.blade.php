{!! Html::style('css/global.css') !!}

{!! Html::style('css/bootstrap-4.0.0/css/bootstrap.min.css') !!}
{!! Html::style('css/movil.css',['media'=>"(min-width: 320px) and (max-width: 480px)"]) !!}
{!! Html::style('css/table.css',['media'=>"(min-width: 481px) and (max-width: 767px)"]) !!}
{!! Html::style('css/desktop.css',['media'=>"(min-width: 768px)"]) !!}