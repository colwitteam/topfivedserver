<html lang="en">
<head>
    <title>Topfived</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="_token" content="{!!csrf_token()!!}"/>
    @include('admin.layout.headlinks')
    @include('admin.layout.scripts')
    <script>
        var APP_URL = "{!! env('APP_URL') !!}";
        var URL_API = "{!! env('APP_URL_API') !!}";
        var AUTH = "{!! Session::get('access_token') !!}";
    </script>
</head>
<body>
    <header>
        @include('admin.layout.header')
    </header>
    <div class="container-fluid">

        @yield('content')
    </div>
    <footer>

    </footer>
    @yield('scripts')
</body>
</html>