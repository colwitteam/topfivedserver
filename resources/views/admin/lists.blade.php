@extends('admin.layout.layout')
@section('content')
    <div class="pl-md-5 pr-md-5" id="wrapper">
        <div class="col-12">
            <h3 class="display-5 pt-lg-5 pt-xl-5 pt-5 pb-4 pt-sm-5 pb-sm-5 pt-xl-4 pt-md-5 pb-md-5">Lists</h3>
            <div class="">
                <form class="full-width pb-4 form-check-inline">
                    <div class="row">
                        <div class="col-xs-12 col-md-2 col-sm-4">
                            <div class="form-group pl-0">
                                <label class="sr-only" for="inlineFormInput">Name</label>
                                <input type="text" name="name" value="{!! $request['name'] !!}"
                                       class="form-control mb-2 mr-sm-2 mb-sm-0" id="inlineFormInput"
                                       placeholder="List Name">
                            </div>
                        </div>
                        <div class="col-xs-12 col-md-2 pl-md-0 col-sm-4">
                            <div class="form-group pl-0">
                                <label class="sr-only" for="inlineFormInput">Top</label>
                                <input type="text" name="top" value="{!! $request['top'] !!}"
                                       class="form-control mb-2 mr-sm-2 mb-sm-0" id="inlineFormInput"
                                       placeholder="Top">
                            </div>
                        </div>
                        <div class="col-xs-12 col-md-2 pl-md-0 col-sm-4">
                            <div class="form-group pl-0">
                                <label class="sr-only" for="inlineFormInput">Hashtag</label>
                                <input type="text" name="hashtag" value="{!! $request['hashtag'] !!}"
                                       class="form-control mb-2 mr-sm-2 mb-sm-0" id="inlineFormInput"
                                       placeholder="Hashtag">
                            </div>
                        </div>
                        <div class="col-xs-12 col-md-4 pl-md-0 pr-md-0 col-sm-8">
                            <div class="form-group pl-0">
                                <label class="sr-only" for="inlineFormInput">Firstname, lastname or username</label>
                                <input type="text" name="term" value="{!! $request['term'] !!}"
                                       class="form-control mb-2 mr-sm-2 mb-sm-0" id="inlineFormInput"
                                       placeholder="Firstname, lastname or username">
                            </div>
                        </div>
                        <div class="col-xs-12 col-md-1 col-sm-4">
                            <button type="submit" class="btn btn-primary">Search</button>
                        </div>
                    </div>
                </form>
            </div>

            <table class="table table-hover table-responsive">
                <thead>
                <tr>
                    <th class="" style="width: 5%">Id</th>
                    <th style="width: 15%">Name</th>
                    <th style="width: 15%" class="">City</th>
                    <th style="width: 15%" class="hidden-xs-down">Country</th>
                    <th style="width: 40%" class="hidden-xs-down">Address</th>
                    <th style="width: 10%" class="text-center">Action</th>
                </tr>
                </thead>
                <tbody>
                @if(count($date.data.tops)>0 )
                    <td colspan="8"><h1 class="display-6 text-center">Bien</h1></td>
                @else
                    <tr>
                        <td colspan="8"><h1 class="display-6 text-center">No have items.</h1></td>
                    </tr>
                @endif
                </tbody>
            </table>

            @if($lists['total']>10)
                <div class="col-12 col-xs-12">
                <nav aria-label="Page navigation example">
                    <ul class="pagination  justify-content-center">
                        <li class="page-item @if($lists["prev_page_url"]==null)  disabled @endif">

                            <a class="page-link"
                               href="{!! route('admin.lists',["page"=>str_replace(env('APP_URL_API').'admin/lists?page=','',$lists["prev_page_url"]),'term'=>$request['term'],'top'=>$request['top'],'hashtag'=>$request['hashtag']]) !!}"
                               tabindex="-1">Previous</a>
                        </li>
                        <?php  $max = 3 ?>

                        @if($lists['current_page']<$max)
                            <?php $sp = 1 ?>
                        @elseif($lists['current_page'] >= (ceil($lists['total']/10) - floor($max / 2)))
                            <?php $sp = ceil($lists['total'] / 10) - $max + 1  ?>
                        @elseif($lists['current_page']>=$max)
                            <?php $sp = ceil($lists['total'] / 11) - floor($max / 1) - 1 ?>
                        @endif

                        @if($lists['current_page']>=$max)
                            <li class="page-item ">
                                <a class="page-link" href='{!! route('admin.lists',['page'=> 1,'term'=>$request['term'],'top'=>$request['top'],'hashtag'=>$request['hashtag']]) !!}' title='Page 1'>First</a></li>
                            @if ($lists['current_page'] > $max - 1)
                                <li class="page-item hidden-md-down"><a class="page-link"
                                                         href="{!! route('admin.lists',['page'=> $lists['current_page'] - $max ,'term'=>$request['term'],'top'=>$request['top'],'hashtag'=>$request['hashtag']]) !!}">...</a>
                                </li>
                            @endif

                        @endif
                        @for($i=$sp;$i<=($sp+$max);$i++)

                            @continue($i > $lists['last_page'])

                            <li class="page-item @if($lists['current_page']==$i) active @endif">
                                <a class="page-link"
                                   href="{!! route('admin.lists',["page"=>$i,'term'=>$request['term'],'top'=>$request['top'],'hashtag'=>$request['hashtag']]) !!}">
                                    {!! $i !!}
                                </a>
                            </li>
                        @endfor

                        @if ($lists['current_page'] + 3 <= $lists['last_page'])

                            @if ($lists['current_page']+ 1 <= ($lists['last_page'] - floor($max / 2)) && ($max + $lists['current_page']) - 2 <= (int) $lists['last_page'])
                                @if ($lists['current_page'] + 3 <= $lists['last_page'])
                                    <li class="page-item hidden-md-down">
                                        <a class="page-link"
                                           href="{!! route('admin.lists',['page'=> ($lists['last_page'] - floor($max / 2)),'term'=>$request['term'],'top'=>$request['top'],'hashtag'=>$request['hashtag']])  !!}">...</a>
                                    </li>
                                @endif
                                <li class="page-item"><a class="page-link" href="{!! route('admin.lists',['page'=> $lists['last_page'],'term'=>$request['term'],'top'=>$request['top'],'hashtag'=>$request['hashtag']]) !!}"
                                                         title='Page <?php $lists['last_page'] ?>'>Last</a>
                                </li>

                            @endif
                        @endif

                        <li class="page-item @if($lists["next_page_url"]==null) disabled @endif">
                            <a class="page-link"
                               href="{!! route('admin.lists',["page"=>str_replace(env('APP_URL_API').'admin/lists?page=','',$lists["next_page_url"]),'term'=>$request['term'],'top'=>$request['top'],'hashtag'=>$request['hashtag']]) !!}"
                            >Next</a>
                        </li>
                    </ul>
                </nav>
                </div>
            @endif

        </div>
        @include('admin.layout.modal.lists')
    </div>
@endsection
@section('scripts')
    {!! Html::script('js/lists.js') !!}
@endsection