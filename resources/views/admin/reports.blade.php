@extends('admin.layout.layout')
@section('content')
    <div id="wrapper">
        <h1 class="display-3 pt-4 pb-4">Reports</h1>
        <div class="container-fluid">
            <ul class="nav nav-tabs" role="tablist">
                <li class="nav-item">
                    <a class="nav-link active" href="#users" role="tab" data-toggle="tab">Users</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#lists" role="tab" data-toggle="tab">Lists</a>
                </li>
            </ul>
            <div class="col-12">
                <!-- Tab panes -->
                <div class="tab-content">
                    <div role="tabpanel" class="tab-pane fade in active show" id="users">
                        <table class="table table-hover table-responsive">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Firstname</th>
                                <th>Lastname</th>
                                <th>Username</th>
                                <th>Email</th>
                                <th>Latitude</th>
                                <th>Longitude</th>
                                <th class="text-center">Action</th>
                            </tr>
                            </thead>
                            <tbody class="list_user">

                            </tbody>
                        </table>
                    </div>

                    <div role="tabpanel" class="tab-pane fade" id="lists">
                        <table class="table table-hover table-responsive">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Name</th>
                                <th>City</th>
                                <th>Country</th>
                                <th>Address</th>
                                <th>Latitude</th>
                                <th>Longitude</th>
                                <th class="text-center">Action</th>
                            </tr>
                            </thead>
                            <tbody class="list_lists">

                            </tbody>
                        </table>
                    </div>
                </div>


            </div>
        </div>
    </div>
@endsection
@section('scripts')
    {!! Html::script('js/reports.js') !!}
@endsection