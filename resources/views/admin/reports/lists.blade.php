@extends('admin.layout.layout')
@section('content')
    <div class="pl-md-5 pr-md-5" id="wrapper">
        <div class="col-12">
            <h3 class="display-5 pt-lg-5 pt-xl-5 pt-5 pb-4 pt-sm-5 pb-sm-5 pt-xl-4 pt-md-5 pb-md-5 text-capitalize">reported Lists</h3>

            <div class="">
                <form class="full-width search-user pb-4 form-check-inline">
                    <div class="row">
                        <div class="col-xs-12 col-md-3 col-lg-2 col-sm-4">
                            <div class="form-group">
                                <select name="order" class="custom-select custom-list mr-sm-2 mb-sm-0"
                                        id="inlineFormCustomSelect">
                                    <option @if($request['order']==0) selected @endif value="0">Enabled Lists</option>
                                    <option @if($request['order']==1) selected @endif  value="1">Disabled Lists</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-xs-12 col-md-2 pl-md-0 col-sm-4">
                            <div class="form-group pl-0">
                                <label class="sr-only" for="inlineFormInput">Name</label>
                                <input type="text" name="name" value="{!! $request['name'] !!}"
                                       class="form-control mb-sm-0" id="inlineFormInput"
                                       placeholder="List Name">
                            </div>
                        </div>
                        <div class="col-xs-12 col-md-2 col-lg-1 pl-md-0 col-sm-4">
                            <div class="form-group pl-0">
                                <label class="sr-only" for="inlineFormInput">Top</label>
                                <input type="text" name="top" value="{!! $request['top'] !!}"
                                       class="form-control mr-sm-2 mb-sm-0" id="inlineFormInput"
                                       placeholder="Top">
                            </div>
                        </div>
                        <div class="col-xs-12 col-md-2 col-lg-1 pr-md-0 pl-lg-0 pr-lg-0 col-sm-4">
                            <div class="form-group pl-0">
                                <label class="sr-only" for="inlineFormInput">Hashtag</label>
                                <input type="text" name="hashtag" value="{!! $request['hashtag'] !!}"
                                       class="form-control mr-sm-2 mb-sm-0" id="inlineFormInput"
                                       placeholder="Hashtag">
                            </div>
                        </div>
                        <div class="col-xs-12 col-md-7 col-lg-4 col-sm-8">
                            <div class="form-group pl-0">
                                <label class="sr-only" for="inlineFormInput">Owner</label>
                                <input type="text" name="term" value="{!! $request['term'] !!}"
                                       class="form-control mr-sm-2 mb-sm-0" id="inlineFormInput"
                                       placeholder="Firstname, lastname or username">
                            </div>
                        </div>
                        <div class="col-xs-12 col-md-2 pr-md-0 col-sm-4">
                            <button type="submit" class="btn btn-primary">Search</button>
                        </div>
                    </div>
                </form>
            </div>
            <div class="row">
                <div class="col-md-12 col-lg-12 col-xs-12 col-sm-12">
                    <table class="table table-hover table-responsive">
                        <thead>
                        <tr>
                            <th style="width: 5%">Id</th>
                            <th style="width: 15%">Name</th>
                            <th style="width: 15%">City</th>
                            <th style="width: 15%" class="hidden-sm-down">Country</th>
                            <th style="width: 20%" class="hidden-sm-down">Address</th>
                            <th style="width: 10%">Quantity</th>
                            <th style="width: 10%" class="text-center">Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        @if(count($lists)>0)
                            @foreach($lists['data'] as $list)
                                <tr class="list-row reports" data-name="{!! $list['name'] !!}"
                                    data-status="{!! $list['enable'] !!}" data-id="{!! $list['id'] !!}">
                                    <th scope="row">{!! $list['id']!!}</th>
                                    <td>{!! $list['name'] !!}</td>
                                    <td>{!! $list['city'] !!}</td>
                                    <td class="hidden-sm-down">{!! $list['country'] !!}</td>
                                    <td class="hidden-sm-down">{!! $list['address'] !!}</td>
                                    <td class="list-row reports" data-name="{!! $list['name'] !!}"
                                        data-status="{!! $list['enable'] !!}" data-id="{!! $list['id'] !!}"><a href="">Reported
                                            ({!! $list['total_list'] !!})</a>
                                    </td>
                                    <td class="content-buttons" align="center">
                                        <button class="btn change-status-list text-capitalize @if($list['enable']!=1) btn-success @else btn-danger @endif"
                                                data-id="{!! $list['id']!!}"
                                                data-status="@if($list['enable']!=1) 1 @else 2 @endif"
                                                type="button">@if($list['enable']!=1) enabled @else
                                                disabled @endif</button>
                                    </td>
                                </tr>
                            @endforeach

                        @else
                            <tr>
                                <td colspan="8"><h1 class="display-6 text-center">No have items.</h1></td>
                            </tr>
                        @endif
                        </tbody>
                    </table>
                </div>
            </div>
            @if(count($lists)>0)
                @if($lists['total']>10)
                    <nav aria-label="Page navigation example">
                        <ul class="pagination justify-content-center">
                            <li class="page-item @if($lists["prev_page_url"]==null)  disabled @endif">

                                <a class="page-link"
                                   href="{!! route('admin.reports.lists',["page"=>str_replace(env('APP_URL_API').'admin/reports/lists?page=','',$lists["prev_page_url"]),
                               'order'=>$request['order'],'name'=>$request['name'],'firstname'=>$request['firstname'],'username'=>$request['username']]) !!}"
                                   tabindex="-1">Previous</a>
                            </li>
                            <?php  $max = 5 ?>

                            @if($lists['current_page']<$max)
                                <?php $sp = 1 ?>
                            @elseif($lists['current_page'] >= (ceil($lists['total']/10) - floor($max / 2)))
                                <?php $sp = ceil($lists['total'] / 10) - $max + 1  ?>
                            @elseif($lists['current_page']>=$max)
                                <?php $sp = ceil($lists['total'] / 11) - floor($max / 1) - 1 ?>
                            @endif

                            @if($lists['current_page']>=$max)
                                <li class="page-item"><a class="page-link"
                                                         href='{!! route('admin.reports.lists',['page'=> 1,'order'=>$request['order'],
                                                     'name'=>$request['name'],'firstname'=>$request['firstname'],'lastname'=>$request['lastname'],
                                                     'username'=>$request['username']]) !!}'
                                                         title='Page 1'>First</a></li>
                                @if ($lists['current_page'] > $max - 1)
                                    <li class="page-item hidden-md-down"><a class="page-link"
                                                             href="{!! route('admin.reports.lists',['page'=> $lists['current_page'] - $max ,
                                                         'order'=>$request['order'],'name'=>$request['name'],'firstname'=>$request['firstname'],
                                                         'lastname'=>$request['lastname'],'username'=>$request['username']]) !!}">...</a>
                                    </li>
                                @endif

                            @endif
                            @for($i=$sp;$i<=($sp+$max);$i++)
                                @continue($i > $lists['last_page'])

                                <li class="page-item @if($lists['current_page']==$i) active @endif">
                                    <a class="page-link"
                                       href="{!! route('admin.reports.lists',["page"=>$i,'order'=>$request['order'],
                                   'name'=>$request['name'],'firstname'=>$request['firstname'],'lastname'=>$request['lastname'],
                                   'username'=>$request['username']]) !!}">
                                        {!! $i !!}
                                    </a>
                                </li>
                            @endfor

                            @if ($lists['current_page'] + 3 <= $lists['last_page'])

                                @if ($lists['current_page']+ 1 <= ($lists['last_page'] - floor($max / 2)) && ($max + $lists['current_page']) - 2 <= (int) $lists['last_page'])
                                    @if ($lists['current_page'] + 3 <= $lists['last_page'])
                                        <li class="page-item hidden-md-down">
                                            <a class="page-link"
                                               href="{!! route('admin.reports.lists',['page'=> ($lists['last_page'] - floor($max / 2)),'order'=>$request['order'],'name'=>$request['name'],
                                           'firstname'=>$request['firstname'],'lastname'=>$request['lastname'],'username'=>$request['username']])  !!}">...</a>
                                        </li>
                                    @endif
                                    <li class="page-item"><a class="page-link"
                                                             href="{!! route('admin.reports.lists',['page'=> $lists['last_page'],'order'=>$request['order'],'name'=>$request['name'],
                                           'firstname'=>$request['firstname'],'lastname'=>$request['lastname'],'username'=>$request['username']]) !!}"
                                                             title='Page <?php $lists['last_page'] ?>'>Last</a>
                                    </li>

                                @endif
                            @endif

                            <li class="page-item @if($lists["next_page_url"]==null) disabled @endif">
                                <a class="page-link"
                                   href="{!! route('admin.reports.lists',["page"=>str_replace(env('APP_URL_API').'admin/reports/lists?page=','',
                               $lists["next_page_url"]),'order'=>$request['order'],'term'=>$request['term'],
                                           'top'=>$request['top'],'hashtag'=>$request['hashtag']]) !!}"
                                >Next</a>
                            </li>
                        </ul>
                    </nav>
                @endif
            @endif
        </div>
        @include('admin.reports.modal.lists')
    </div>
@endsection
@section('scripts')
    {!! Html::script('js/reports.js') !!}
@endsection