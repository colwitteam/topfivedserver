@extends('admin.layout.layout')
@section('content')
    <div class="pl-md-5 pr-md-5" id="wrapper">

        <div class="col-12">
            <h3 class="display-5 pt-lg-5 pt-xl-5 pt-5 pb-4 pt-sm-5 pb-sm-5 pt-xl-4 pt-md-5 pb-md-5">Users</h3>
            <div>
                <form class="form-check-inline pb-4 search-user">
                    <div class="row">
                        <div class="col-xs-12 col-md-3 col-sm-4">
                            <div class="form-group">
                                <label class="sr-only" for="inlineFormInput">username</label>
                                <input type="text" name="username" value="{!! $request['username'] !!}"
                                       class="form-control mb-2 mr-sm-2 mb-sm-0" id="inlineFormInput"
                                       placeholder="Username">
                            </div>
                        </div>
                        <div class="col-xs-12 col-md-2 pl-md-0 col-sm-4">
                            <div class="form-group">
                                <label class="sr-only" for="inlineFormInput">firstname</label>
                                <input type="text" name="firstname" value="{!! $request['firstname'] !!}"
                                       class="form-control mb-2 mr-sm-2 mb-sm-0" id="inlineFormInput"
                                       placeholder="Firstname">
                            </div>
                        </div>
                        <div class="col-xs-12 col-md-2 pl-md-0 col-sm-4">
                            <div class="form-group">
                                <label class="sr-only" for="inlineFormInput">lastname</label>
                                <input type="text" name="lastname" value="{!! $request['lastname'] !!}"
                                       class="form-control mb-2 mr-sm-2 mb-sm-0" id="inlineFormInput"
                                       placeholder="Lastname">
                            </div>
                        </div>
                        <div class="col-xs-12 col-md-3 pl-md-0 col-sm-4">
                            <div class="form-group">
                                <label class="sr-only" for="inlineFormInput">email</label>
                                <input type="text" name="email" value="{!! $request['email'] !!}"
                                       class="form-control mb-2 mr-sm-2 mb-sm-0" id="inlineFormInput"
                                       placeholder="Email">
                            </div>
                        </div>
                        <div class="col-xs-12 col-md-2 col-sm-4">
                            <button type="submit" class="btn btn-primary">Search</button>
                        </div>
                    </div>
                </form>
            </div>
            <div class="row">
            <div class="col-md-12">
            <table class="table table-hover table-responsive">
                <thead>
                <tr>
                    <th style="width: 5%">Id</th>
                    <th style="width: 15%" class="hidden-xs-down">Firstname</th>
                    <th style="width: 15%" class="hidden-xs-down">Lastname</th>
                    <th style="width: 15%">Username</th>
                    <th style="width: 15%">Email</th>
                    <th style="width: 10%" class="text-center">Action</th>
                </tr>
                </thead>
                <tbody>
                @if(count($users['data'])>0)
                    @foreach($users['data'] as $user)
                        <tr>
                            <th class="user-row" scope="row">{!! $user['id']!!}</th>
                            <td class="user-row hidden-xs-down"
                                data-id="{!! $user['id'] !!}">{!! $user['firstname'] !!}</td>
                            <td class="user-row hidden-xs-down"
                                data-id="{!! $user['id'] !!}">{!! $user['lastname'] !!}</td>
                            <td class="user-row"
                                data-id="{!! $user['id'] !!}">{!! '@'.$user['username'] !!}</td>
                            <td class="user-row" data-id="{!! $user['id'] !!}">{!! $user['email'] !!}</td>

                            <td class="content-buttons" align="center">
                                <button type="button" class="btn change-status text-capitalize
                                @if($user['banned']!=0) btn-success @else btn-danger @endif"
                                        data-id="{!! $user['id']!!}"
                                        data-status="@if($user['banned']!=0) 0 @else 1 @endif">
                                    @if($user['banned']!=0) Enable @else Disable @endif
                                </button>
                            </td>
                        </tr>
                    @endforeach

                @else
                    <tr>
                        <td colspan="8"><h4 class="display-6 text-center">No matches found.</h4></td>
                    </tr>
                @endif
                </tbody>
            </table>
            </div>
            </div>
            <div class="row">
                <div class="col-12">
                    @if($users['total']>10)
                        <nav aria-label="Page navigation example">
                            <ul class="pagination justify-content-center">
                                <li class="page-item @if($users["prev_page_url"]== null)  disabled @endif">
                                    <a class="page-link" href="{!! route('admin.users',["page"=>str_replace(env('APP_URL_API')
                   .'admin/users?page=','',$users["prev_page_url"]),'username'=>$request['username'],'firstname'=>$request['firstname'],'lastname'=>$request['lastname'],'email'=>$request['email']]) !!}">Previous</a>
                                </li>

                                <?php $max = 5 ?>

                                @if($users['current_page']<$max)
                                    <?php $sp = 1 ?>
                                @elseif($users['current_page'] >= (ceil($users['total']/10) - floor($max / 2)))
                                    <?php $sp = ceil($users['total'] / 10) - $max + 1 ?>
                                @elseif($users['current_page']>=$max)
                                    <?php $sp = ceil($users['total'] / 11) - floor($max / 1) - 1 ?>
                                @endif

                                @if($users['current_page']>=$max)
                                    <li class="page-item">
                                        <a class="page-link"
                                           href='{!! route('admin.users',['page'=> 1,'username'=>$request['username'],'firstname'=>$request['firstname'],'lastname'=>$request['lastname'],'email'=>$request['email']]) !!}'
                                           title='Page 1'>First</a>
                                    </li>
                                    @if ($users['current_page'] > $max - 1)
                                        <li class="page-item hidden-md-down">
                                            <a class="page-link"
                                                                 href="{!! route('admin.users',['page'=> $users['current_page'] - $max,'username'=>$request['username'],'firstname'=>$request['firstname'],'lastname'=>$request['lastname'],'email'=>$request['email']]) !!}">...</a>
                                        </li>
                                    @endif
                                @endif

                                @for($i=$sp;$i<=($sp+$max);$i++)

                                    @continue($i > $users['last_page'])

                                    <li class="page-item @if($users['current_page']==$i) active @endif">
                                        <a class="page-link"
                                           href="{!! route('admin.users',["page"=>$i,'username'=>$request['username'],'firstname'=>$request['firstname'],'lastname'=>$request['lastname'],'email'=>$request['email']]) !!}">
                                            {!! $i !!}</a>
                                    </li>
                                @endfor

                                @if ($users['current_page'] + 3 <= $users['last_page'])

                                    @if ($users['current_page']+ 1 <= ($users['last_page'] - floor($max / 2)) && ($max + $users['current_page']) - 2 <= (int) $users['last_page'])
                                        @if ($users['current_page'] + 3 <= $users['last_page'])
                                            <li class="page-item hidden-md-down">
                                                <a class="page-link"
                                                   href="{!! route('admin.users',['page'=> ($users['last_page'] - floor($max / 2)),'username'=>$request['username'],'firstname'=>$request['firstname'],'lastname'=>$request['lastname'],'email'=>$request['email']])  !!}">...</a>
                                            </li>
                                        @endif
                                        <li class="page-item">
                                            <a class="page-link"
                                               href="{!! route('admin.users',['page'=> $users['last_page'],'username'=>$request['username'],'firstname'=>$request['firstname'],'lastname'=>$request['lastname'],'email'=>$request['email']]) !!}"
                                               title='Page <?php $users['last_page'] ?>'>Last</a>
                                        </li>
                                    @endif
                                @endif

                                <li class="page-item @if($users["next_page_url"]== null) disabled @endif">
                                    <a class="page-link"
                                       href="{!! route('admin.users',["page"=>str_replace(env('APP_URL_API').'admin/users?page=','',$users["next_page_url"]),'username'=>$request['username'],'firstname'=>$request['firstname'],'lastname'=>$request['lastname'],'email'=>$request['email']]) !!}"
                                       tabindex="-1">Next</a>
                                </li>
                            </ul>
                        </nav>
                    @endif
                </div>
            </div>
        </div>
    </div>
    @include('admin.layout.modal.users')
@endsection
@section('scripts')
    {!! Html::script('js/users.js') !!}
@endsection