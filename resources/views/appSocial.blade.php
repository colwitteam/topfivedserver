<!DOCTYPE html>
<html>
<head>
  <title>Where lists become trends</title>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="{{asset('css/bootstrap-4.0.0/css/bootstrap.css') }}">
  <link rel="stylesheet" href="{{asset('css/appSocial.css') }}">

  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
  <script src="{{asset('js/convertImage/filesaver.js') }}"></script>
  <script src="{{asset('js/convertImage/html2canvas.js') }}"></script>

    <meta name="twitter:card" content="summary">
    <meta name="twitter:url" content="http://top5dapp.com/public/listShow/2">
    <meta name="twitter:domain" content="">
    @if ($date['status'] == 'ok')
        <meta name="twitter:title" content="{{$date['data']['name']}}">
        <meta name="twitter:text:title" content="{{$date['data']['name']}}">
        <meta name="og:title" content="{{$date['data']['name']}}">
        <meta name="twitter:image" content="https://topfived.com/wp-content/uploads/2017/09/top5d-thick-tm-150x150.png">
    @else        
        <meta name="twitter:title" content="Where lists become trends">
        <meta name="twitter:text:title" content="Where lists become trends">
        <meta name="og:title" content="Where lists become trends">        
    @endif
    
    <!--<meta name="twitter:image" content="https://topfived.com/wp-content/uploads/2017/09/top5d-thick-tm-150x150.png">-->
    <script type="text/javascript">
    /*$(function() { 
      $("#crearimagen").click(function() { 
        html2canvas($("#contenido"), {
          onrendered: function(canvas) {
            theCanvas = canvas;
            document.body.appendChild(canvas);
            /*
            canvas.toBlob(function(blob) {
              saveAs(blob, "Dashboard.png"); 
            });
            */
          /*}
        });
      });
    });*/
      $(document).ready(function() {
        $(window).load(function() {
          //$("#crearimagen").click(function() { 
          if ($("#idList").val() == null) {

            html2canvas($("#home"), {
              onrendered: function(canvas) {
                theCanvas = canvas;
                var img = canvas.toDataURL('image/png');
                //console.log(img);
                //$('head').append('<meta name="og:title" content="Juan David">');
                //document.head.innerHTML = document.head.innerHTML + '<meta name="og:title" content="Juan David">';
                var data = {
                  'id': $("#idList").val(),
                  'type': $("#type").val(),
                  'image': img
                };
                console.log(data);
                $.ajax({
                  url: 'http://top5dapp.com/public/api/update/image',
                  type: 'POST',   
                  data: { 
                        'id': $("#idList").val(),
                        'type': $("#type").val(),
                        'image': img
                  }
                })
                .done(function(rta) {
                  console.log(rta);
                })
                .fail(function(error) {
                  console.log("error");
                  console.log(error);
                })
                .always(function() {
                  console.log("complete");
                });
              }
            });
          }else{alert('df');}
          //});
          
        });
      });
    
    </script>
</head>
<body>
  <!-- First Parallax Image with Logo Text -->
  <div class="bgimg-1 w3-display-container" id="home">

    <div id="divContenedor">
      <div id="divHeaderTitle">
        <h3 id="textTitle">Where lists become trends</h3> 
      </div>

      <div id="divExtern">
        <div id="divHeader">
          <img src="{{asset('images/LogoOrange.png') }}" alt="Logo" id="imageLogo">
          <p id="textListThat">List That Connect</p>
        </div>
        @if ($date['status'] == 'ok')
        <input type="hidden" id="idList" name="idList" value="{{$date['data']['id']}}">
        
        <input type="hidden" id="type" name="type" value="list">
        <div id="divList">
            <h3 id="textList">My ranking for:</h3>  
            <h3 id="textList">{{$date['data']['name']}}</h3>  
            <br>
            @foreach($date['data']['tops'] as $row)
              <h3 id="textList">{{ $row['order']+1}}. {{ $row['name']}}</h3>                                  
            @endforeach            
        </div>  
        @else
        <div id="divList">
          <h3 id="textList">{{$date['status']}}</h3>  
        </div>
        @endif
        <div id="footerIntern">         
            <h3 id="textFooterIntern">oh, you don't agree?</h3>
            <h3 id="textFooterIntern">Well what's YOUR top five then?!?</h3>
        </div>  
      </div>

      <div id="divFooter">      
        <h3 id="textFooter">Listed by mike_b on Top5d: the simple way to rank, debate and discover, everything.</h3>  
        <h3 id="textFooterGray">Put your top picks out there with the hot new app. Download at (playstore link)
          <a href="https://www.top5dapp.com" id="linkHref">WWW.TOP5ADAPP.COM</a>      
        </h3> 
      </div>
    
    </div>

  </div>
</body>
</html>
