<!DOCTYPE html>
<html>
<head>
  <title>Where lists become trends</title>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="{{asset('css/bootstrap-4.0.0/css/bootstrap.css') }}">
  <link rel="stylesheet" href="{{asset('css/appSocial.css') }}">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
  <script src="{{asset('js/convertImage/filesaver.js') }}"></script>
  <script src="{{asset('js/convertImage/html2canvas.js') }}"></script>

  <meta name="twitter:card" content="summary">
  <meta name="twitter:url" content="http://top5dapp.com/public/listShow/2">
  <meta name="twitter:domain" content="">
  @if ($date['status'] == 'ok')
      <meta name="twitter:title" content="{{$date['data']['owner_user']['username']}}">
      <meta name="twitter:text:title" content="{{$date['data']['owner_user']['username']}}">
      <meta name="og:title" content="{{$date['data']['name']}}">
  @else        
      <meta name="twitter:title" content="Where lists become trends">
      <meta name="twitter:text:title" content="Where lists become trends">
      <meta name="og:title" content="Where lists become trends">
  @endif

  <meta name="twitter:image" content="https://topfived.com/wp-content/uploads/2017/09/top5d-thick-tm-150x150.png">
  <script type="text/javascript">
      $(document).ready(function() {
        $(window).load(function() {
          //$("#crearimagen").click(function() { 
            html2canvas($("#home"), {
              onrendered: function(canvas) {
                theCanvas = canvas;
                var img = canvas.toDataURL('image/png');
                console.log(img);
                //$('head').append('<meta name="twitter:image" content="http://bradsknutson.com/path/to/image/">');
                /*var data = {
                  'idList': $("#idList").val(),
                  'type': $("#type").val(),
                  'imageBase64': img
                };*/
                //console.log(data);
                $.ajax({
                  url: 'http://localhost:8000/usuario',
                  type: 'POST',   
                  data: { 
                        'idChallenge': $("#idChallenge").val(),
                        'type': $("#type").val(),
                        'imageBase64': img
                  }
                })
                .done(function(rta) {
                  console.log(rta);
                })
                .fail(function() {
                  console.log("error");
                })
                .always(function() {
                  console.log("complete");
                });
              }
            });
          //});
          
        });
      });
    
  </script>
</head>
<body>
  <!-- First Parallax Image with Logo Text -->
  <div class="bgimg-1 w3-display-container" id="home">
    <div id="divContenedor" >
      <div id="divHeaderTitle">
        <h3 id="textTitle">Where lists become trends</h3> 
      </div>

      <div id="divExternChallenge">
        <div id="divHeader">
          <img src="{{asset('images/LogoOrange.png') }}" alt="Logo" id="imageLogo">
          <p id="textListThat">List That Connect</p>
        </div>
        @if ($date['status'] == 'ok')
          @if ($date['data'] != [])
            @if ($date['data']['owner_user'] != null AND $date['data']['target_user'] != null AND $date['data']['owner_list'] AND $date['data']['target_list'])
              <div id="divListChallenge">
                  <h3 id="textListTitle">Best NFL Quaterback</h3>  
                  <input type="hidden" id="idChallenge" name="idChallenge" value="{{$date['data']['id']}}">
                  <input type="hidden" id="type" name="type" value="Challnge">
                  
                  @if ($date['status'] == 'ok')
                    <div id="divListUserChallenge">              
                      <h3 id="textRanking">Ranking by</h3> 
                      <h3 id="textNameRanking">{{$date['data']['owner_user']['firstname']}}</h3> 
                      <h3 id="textRankingUsername">{{$date['data']['owner_user']['username']}}</h3>                       
                      
                      @foreach($date['data']['owner_list']['tops'] as $row)
                        <h3 id="textListItem">{{ $row['order']+1}}. {{ $row['name']}}</h3>                        
                      @endforeach
                    </div>
                  @endif
                  
                  <div id="divImage">
                    <img src="{{asset('images/vs.png') }}" alt="Logo" id="imageVS">
                  </div>
                  @if ($date['status'] == 'ok')
                  <div id="divListUserChallengeRight">
                    <h3 id="textRanking">Ranking by</h3> 
                    <h3 id="textNameRanking">{{$date['data']['target_user']['firstname']}}</h3> 
                    <h3 id="textRankingUsername">{{$date['data']['target_user']['username']}}</h3>                     
                    
                    @foreach($date['data']['target_list']['tops'] as $row)
                      <h3 id="textListItem">{{ $row['order']+1}}. {{ $row['name']}}</h3>                    
                    @endforeach
                  </div>
                  @endif
              </div>          
            @else
              <div id="divListChallenge">
                <h3 id="textListTitle">Not Found</h3>  
              </div>
            @endif          
          @else
            <div id="divListChallenge">
              <h3 id="textListTitle">Not Found</h3>  
            </div>
          @endif          
        @else
          <div id="divListChallenge">
            <h3 id="textListTitle">Not Found</h3>  
          </div>
        @endif
        <div id="footerInternChallenge">         
            <h3 id="textFooterInternChallenge">Whose ranking are better?</h3>
            <h3 id="textClickVote">Click to vote</h3>
        </div>  
      </div>

      <div id="divFooter">      
        <h3 id="textFooter">Listed by mike_b on Top5d: the simple way to rank, debate and discover.</h3>  
        <h3 id="textFooterGray">Put your top picks out there with the hot new app. Download at (playstore link)
          <a href="https://www.top5dapp.com" id="linkHref">WWW.TOP5ADAPP.COM</a>      
        </h3> 
      </div>
    
    </div>

  </div>
</body>
</html>
