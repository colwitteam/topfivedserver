<?php

use App\Http\Middleware\CheckToken;

/*
| API Routes
|--------------------------------------------------------------------------
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
//User endpoint
Route::get('/', 'IndexController@index');
Route::get('/user/{id}', 'UserController@show')->middleware(CheckToken::class);
Route::get('/profile/', 'UserController@profileGet')->middleware(CheckToken::class);
Route::put('/profile/', 'UserController@profileUpdate')->middleware(CheckToken::class);
Route::put('/user/update', 'UserController@updateImageUsername')->middleware(CheckToken::class);
Route::post('/signup/', 'UserController@signup');
Route::post('/signin/', 'UserController@signin');
Route::post('/update/image', 'UserController@updateImageChallengeList');


Route::put('user/privacy', 'UserController@setAccount');
Route::get('user/awards/{id}', 'UserController@getAwards')->middleware(CheckToken::class);

Route::get('user/username/{username}', 'UserController@getByUsername')->middleware(CheckToken::class);

//social networks
Route::post('/fbsignin/', 'UserController@fbLogin');
Route::get('/fbfriend/{fbtoken}', 'UserController@getFbFriends')->middleware(CheckToken::class);

Route::name('googlesignin')->post('/googlesignin/', 'UserController@googleLogin');
Route::get('/googlecontact', 'UserController@getGoogleFriends')->middleware(CheckToken::class);

Route::post('/twittersignin/', 'UserController@twitterLogin');
Route::get('/twitterfriends/{token}/{secret}', 'UserController@twFollowers');
//*************

//Session endpoint
Route::get('/notsession', 'SessionController@notSession');

//user follows
Route::get('/follow/', 'UserController@getFollows')->middleware(CheckToken::class);
Route::get('/follower/', 'UserController@getFollowers')->middleware(CheckToken::class);
Route::post('/follow/', 'UserController@followUser')->middleware(CheckToken::class);
Route::post('/unfollow/', 'UserController@unfollowUser')->middleware(CheckToken::class);

//User notifications
Route::get('/notification/', 'UserController@getNotifications')->middleware(CheckToken::class);

//User Feed
Route::get('/feed/{uid}', 'UserController@getFeed')->middleware(CheckToken::class);

//User Hashtag
Route::get('hashtag/user/{name?}', 'UserController@getHashTags')->middleware(CheckToken::class);

//followers by id
Route::get('user/followers/{id?}', 'UserController@getFollowersById')->middleware(CheckToken::class);

//follows user by id
Route::get('user/following/{id?}', 'UserController@getUserFollowingsById')->middleware(CheckToken::class);

//Likes by lists
Route::get('list/likes/{id}', 'ListController@getUsersLikesByList')->middleware(CheckToken::class);

//Activities
Route::get('/activity/', 'UserController@getMyActivities')->middleware(CheckToken::class);
Route::get('/activities/', 'UserController@getFollowsActivities')->middleware(CheckToken::class);

//lists
Route::get('/list/', 'ListController@getAll')->middleware(CheckToken::class);
Route::post('/list/', 'ListController@create')->middleware(CheckToken::class);
Route::get('/list/user', 'ListController@getMyLists')->middleware(CheckToken::class);
Route::get('/lists/theme/{name}', 'ListController@getListsTheme')->middleware(CheckToken::class);
Route::get('/list/user/{id}', 'ListController@getAllByUser')->middleware(CheckToken::class);
Route::get('/list/{id}', 'ListController@getList')->middleware(CheckToken::class);
Route::put('/list/{id}', 'ListController@updateByUser')->middleware(CheckToken::class);
Route::delete('/list/{id}', 'ListController@deleteByUser')->middleware(CheckToken::class);
Route::get('list/locations/{id}/{radius}', 'ListController@getLocationsList')->middleware(CheckToken::class);
Route::get('lists/hashtag/{val}', 'ListController@getListByHashtag')->middleware(CheckToken::class);

//comments
Route::get('/comment/list/{id}', 'ListController@getListComments')->middleware(CheckToken::class);
Route::delete('/comment/list/{id}', 'ListController@deleteListComments')->middleware(CheckToken::class);
Route::post('/comment/list/', 'ListController@commentList')->middleware(CheckToken::class);

//Trending Topics
Route::get('/trending/', 'ListController@getTrendingTopics')->middleware(CheckToken::class);

//favorites
Route::get('/like/', 'ListController@getMyLikes')->middleware(CheckToken::class);
Route::post('/like/', 'ListController@setLike')->middleware(CheckToken::class);
Route::delete('/like/{id}', 'ListController@removeLikes')->middleware(CheckToken::class);

//list images
Route::post('/listpic/{id}', 'ListController@uploadListPic')->middleware(CheckToken::class);
Route::delete('/listpic/{id}', 'ListController@deleteImage')->middleware(CheckToken::class);

//list videos
Route::post('/listvideo/', 'ListController@uploadVideo')->middleware(CheckToken::class);
Route::delete('/listvideo/{id}', 'ListController@deleteVideo')->middleware(CheckToken::class);

//categoriesGeolocalizacion si o si se debe pedir en el registro
Route::get('/category/', 'CategoryController@all')->middleware(CheckToken::class);
Route::get('/category/{term}', 'CategoryController@show')->middleware(CheckToken::class);
Route::get('/categorylist/{term}', 'CategoryController@showLists')->middleware(CheckToken::class);

//locations
Route::get('/location/', 'LocationController@all')->middleware(CheckToken::class);
Route::get('/location/{term}', 'LocationController@show')->middleware(CheckToken::class);
Route::get('/locationlist/{term}', 'LocationController@showLists')->middleware(CheckToken::class);

/*Forgot password*/
Route::post('/user/forgotpassword', "ForgotController@forgotPassword");

/*Terms and condiontions*/
Route::get('terms/{kind}', 'TermsController@show');
Route::resource('list_saves', 'SavelistController', ['middleware' => 'checkToken']);

//Search
Route::group(['prefix' => 'search', 'middleware' => 'checkToken'], function () {
    Route::post('store', 'SearchController@store');
    Route::get('searches', 'SearchController@index');
    Route::get('recent_lists', 'SearchController@getRecentlySearchedLists');
    Route::get('lists', 'SearchController@getMoreSearchedLists');
    Route::get('users', 'SearchController@getMoreSearchedUsers');
    Route::get('topics', 'SearchController@getMoreSearchedTopics');
    Route::get('popular_lists', 'SearchController@getPopularLists');
    Route::get('popular_users', 'SearchController@getPopularUsers');
    Route::get('popular/locations', 'SearchController@getPopularLocations');
    Route::get('tops', 'SearchController@getSearchedTops');
});
Route::get('discovery/friend', 'UserController@getFriendsDiscover')->middleware(CheckToken::class);

//User hidden
Route::post('/user/hiddenuser', 'UserController@hiddenUser')->middleware(CheckToken::class);
Route::delete('/user/unhiddenuser', 'UserController@removeUsers')->middleware(CheckToken::class);
Route::get('/users/hidden', 'UserController@getHiddenUsers')->middleware(CheckToken::class);

//Challenges
Route::resource('challenge', 'ChallengeController', ['middleware' => 'checkToken']);
Route::get('challenge/{id}', 'ChallengeController@show')->middleware(CheckToken::class);
Route::get('challange_request', 'ChallengeController@challengeRequest')->middleware(CheckToken::class);
Route::put('challenge/response/{id}', 'ChallengeController@replyChallenge')->middleware(CheckToken::class);
Route::put('challenge/start/{id}', 'ChallengeController@startChallenge')->middleware(CheckToken::class);
Route::get('accept_challenges', 'ChallengeController@acceptsChallenge')->middleware(CheckToken::class);
Route::get('request_challenges', 'ChallengeController@pendingForApproval')->middleware(CheckToken::class);
Route::get('request_challenges/createdbyme', 'ChallengeController@createdByMe')->middleware(CheckToken::class);
Route::get('challenge/{id}', 'ChallengeController@show')->middleware(CheckToken::class);
Route::get('users/challenge/{id}', 'ChallengeController@showByUser')->middleware(CheckToken::class);

//Controllers Admin
Route::group(['prefix' => 'admin', 'middleware' => 'checkToken'], function () {
    Route::get('lists', 'ListController@getListAdmin');
    Route::get('users', 'UserController@getUsersAdmin');
    Route::get('user/{id}', 'UserController@getUserByIdAdmin');
    Route::get('list/{id}', 'ListController@getListByIdAdmin');
    Route::put('user/{id}', 'UserController@setBannedUser');
    Route::put('list/{id}', 'ListController@statusListEnabled');
    Route::get('reports/lists', 'ListController@getReportsList');
    Route::get('reports/users', 'UserController@getReportsUser');
    Route::get('reports/{id}/{type}', 'ReportController@show');
    Route::put('reports/{id}/{type}', 'ReportController@update');
});

//logout
Route::get('logout', 'UserController@logout')->middleware(CheckToken::class);

//messages
Route::resource('message', 'MessageController', ['middleware' => 'checkToken']);

//Reports
Route::resource('report', 'ReportController', ['middleware' => 'checkToken']);