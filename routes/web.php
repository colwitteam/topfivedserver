<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'IndexController@index');
Route::get('admin/login',function (){
    if(Session::get('access_token')):
        return redirect()->route('admin.lists');
    endif;
    return view('admin.index');
});
Route::post('login',[
    'as'=>'login.admin',
    'uses'=>'UserController@signin'
]);

Route::get('listShow/{numList}', 'ListPublicController@listShow');
Route::get('challengeShow/{numChallenge}', 'ListPublicController@challengeShow');

Route::get('admin',[
    'as'=>'admin.welcome',
    'uses'=>'AdminController@index'
]);
Route::get('admin/logout',[
    'as'=>'admin.logout',
    'uses'=>'AdminController@logout'
]);
Route::get('admin/verify_user_admin','AdminController@verifyUserAdmin');
Route::get('admin/welcome','AdminController@index');
Route::get('admin/lists',[
    'as'=>'admin.lists',
    'uses'=>'AdminController@getListAdmin'
]);
Route::get('admin/users',[
    'as'=>'admin.users',
    'uses'=>'AdminController@getUsersAdmin'
]);
Route::get('viewEmail',function(){
    return view("templates.forgotpassword");
});
Route::get('admin/reports/lists',[
    'as'=>'admin.reports.lists',
    'uses'=>'AdminController@getReportsList'
]);
Route::get('admin/reports/users',[
    'as'=>'admin.reports.users',
    'uses'=>'AdminController@getReportsUsers'
]);

Route::get('cache',function(){
    Artisan::call('config:cache');
    Artisan::call('config:clear');
    return "Ok";
});
